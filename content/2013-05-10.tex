% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 10.05.2013
% TeX: Jan

\renewcommand{\printfile}{2013-05-10}

\section{Elektro- und Magnetostatik}

Im Falle statischer Felder gilt:
%
\begin{align*}
	\partial_t\varrho &= 0 \, :\qquad \varrho = \varrho(\bm{r})\\
	\partial_t j &= 0 \, :\qquad \bm{j} = \bm{j}(\bm{r})
\end{align*}
%
Das heißt wir beschäftigen uns in diesem Kapitel mit \acct*{vorgegebenen} Verteilungen, was wiederum bedeutet, dass wir Rückwirkungen auf Ladungen und Ströme vernachlässigen werden.

Wir fordern, dass $\varrho(\bm{r})$ , $\bm{j}(\bm{r}) = 0$ ist außerhalb hinreichend großer Kugeln vom Radius $R$. Da wir statische Felder betrachten wollen, suchen wir auch die statischen Lösungen der Maxwellgleichungen:
%
\begin{align*}
	\partial_t\bm{E} &= 0 \, : \qquad \bm{E} = \bm{E}(\bm{r}) \\
	\partial_t\bm{B} &= 0 \, : \qquad \bm{B} = \bm{B}(\bm{r}).
\end{align*}
%
Die Maxwellgleichungen für den Fall statischer Wellen lauten wie folgt:

\begin{center}
	\begin{tabular}{cc}
		\toprule
		Elektrostatik & Magnetostatik \\
		\midrule
		$\div\bm{E} = 4\pi\varrho$ & $\div\bm{B} = 0$ \\
		$\rot\bm{E} = 0$           & $\rot\bm{B} = \frac{4\pi}{c}\bm{j}$ \\
		\bottomrule
	\end{tabular}
\end{center}

Es fällt auf, dass die Maxwellgleichungen für $\bm{E}$ und $\bm{B}$ nun nicht mehr gekoppelt sind. Die Kopplung zwischen den jeweiligen Feldern tritt also erst durch die Zeitabhängigkeit in Kraft. Es folgt außerdem, dass $\div\bm{j}= 0$. Dies ist allerdings mit der Kontinuitätsgleichung vereinbar, da $\partial_t\varrho = 0$ ist.

\begin{notice}[Aufgabe (für $\mathbb{R}^3$):]
	Gesucht sind Vektorfelder $\bm{E}$/$\bm{B}$, deren Quellen/Wirbel gegeben sind und deren Wirbel/Quellen verschwinden.
	
	Hierfür fordern wir als Randbedingungen, dass die Felder für $r\to\infty$ mindestens wie $r^{-2}$ verschwinden sollen. Diese Forderung gewährleistet, dass die dazugehörigen Potentiale wie $r^{-1}$ abfallen, da ja die Felder, Gradientenfelder eines Potentials sind. Würden wir hingegen fordern, dass das Feld nur mit $r^{-1}$ verschwinden würde, so würde das zugehörige Potential wie $\log(r)$ laufen, was im Unendlichen nicht verschwindet.
\end{notice}
%

\begin{theorem}[Fundamentalsatz der Vektoranalysis]
	Im $\mathbb{R}^3$ gilt:
	%
	\begin{align*}
		\bm{F}(\bm{r}) \rightleftarrows
		\begin{cases}
			\div\bm{F}(\bm{r}) \eqcolon Q(\bm{r}) & \text{Quellen}\\
			\rot\bm{F}(\bm{r}) \eqcolon \bm{W}(\bm{r})\text{ mit }\div\bm{W} = 0 & \text{Wirbel}
		\end{cases}
	\end{align*}
	%
	Ein Vektorfeld sei durch seine Quellen $Q(\bm{r})$ und seine Wirbel $\bm{W}(\bm{r})$ eindeutig bestimmt, wenn
	%
	\begin{itemize}
		\item $\bm{F}(\bm{r}) = \mathcal{O}\left(\frac{1}{r^2}\right)$ für $r\to\infty$
		\item $Q(\bm{r})$, $\bm{W}(\bm{r})$ im \acct*{ganzen} $\mathbb{R}^3$ bekannt sind und im Unendlichen hinreichend schnell abfallen (z.B.\ außerhalb einer Kugel mit Radius $R < \infty$)
	\end{itemize}

	\begin{proof}
		Hier soll nur die Eindeutigkeit bewiesen werden, die Existenz folgt später durch Konstruktion. Wir nehmen an: 

		$\exists\, \bm{F}_1, \bm{F}_2$ mit gleichen Quellen und Wirbeln; $\bm{F}_1 - \bm{F}_2 \eqcolon \bm{U}\neq 0$, $\bm{U} = \mathcal{O}(\frac{1}{r^2})$ für $r\to\infty$.
		%
		\begin{align*}
			\div\bm{F}_i &= Q  \qquad , \, i = 1, 2\\
			\rot\bm{F}_i &= \bm{W} \qquad ,\,  \div\bm{W} = 0
		\end{align*}
		%
		Daraus folgt, dass $\div\bm{U}= 0$ und $\rot\bm{U}= 0$ ist. $\bm{U}$ ist also wirbelfrei und lässt sich somit als ein Gradientenfeld schreiben $\bm{U}= \nabla\psi$. Dabei ist $\psi(r\to\infty)= \mathcal{O}(\frac{1}{r})$. Daraus wiederum folgt direkt:
		\[
		  \nabla^2\psi = 0,
		\]
		also die \acct{Laplace-Gleichung}. Die Lösungen davon sind die harmonischen Funktionen.
		%
		\begin{align*}
			\int_{V}\,(\nabla\psi)^2\,\mathrm{d}V &= \int_{V}\,\Bigl[\nabla\cdot(\psi\nabla\psi) - \psi\underbrace{\nabla^2\psi}_{= 0}\Bigr]\,\mathrm{d}V\\
			&\stackrel{\text{Gauß}}{=} \underbrace{\int_{\partial V}\,\underbrace{\psi\bm{n}\cdot\nabla\psi}_{\mathcal{O}\left(\frac{1}{r^3}\right)}\,\underbrace{\mathrm{d}f}_{\propto r^2}}_{\mathcal{O}\left(\frac{1}{R}\right)} \to 0
		\intertext{dabei ist $R$ der Radius des Volumens V}
			\implies \int_{\mathbb{R}^3}\,(\nabla\psi)^2\,\mathrm{d}V &= \int_{\mathbb{R}^3}\,\bm{U}^2\,\mathrm{d}V = 0\\
			\implies \bm{U} &= 0
		\end{align*}
		%
		Dies ist jedoch ein Widerspruch zur Annahme, dass $\bm{F}_1 - \bm{F}_2 = \bm{U}\neq 0$. Damit ist die Eindeutigkeit bewiesen.
	\end{proof}
\end{theorem}


\begin{notice}[Zusätzliches Resultat:]
	Die harmonischen Funktionen $\psi(\bm{r})$ mit $\nabla^2\psi= 0$ (im ganzen $\mathbb{R}^3$) und $\psi(r\to\infty) = \mathcal{O}(1/r)$ sind: $\psi\equiv 0$
\end{notice}

%
\subsection{Elektrostatik im \texorpdfstring{$\mathbb{R}^3$}{ℝ³}}
%
Wie oben bereits beschrieben gilt im Falle der Elektrostatik:
%
\begin{align*}
	\rot\bm{E} &= 0\\
	\div\bm{E} &= 4\pi\varrho.
\end{align*}
%
Das elektrische Feld $\bm{E}$ ist hier also wirbelfrei, weswegen ein Potential $\phi$ existiert und $\bm{E}$ sich als Gradientenfeld $\bm{E}= -\nabla\phi$ schreiben lässt. Damit folgt insbesondere auch:
%
\begin{align*}
	-\nabla\cdot\nabla\phi &= -\nabla^2\phi = 4\pi\varrho\\
	\implies \nabla^2\phi &= -4\pi\varrho, 
\end{align*}
%
mit $\phi(r\to\infty) = 1/r$ --- die sogenannte \acct{Poisson-Gleichung}. Im folgenden werden wir die Lösungen der Poissonschen Gleichung für das Potential $\phi$ suchen. Dabei setzt sich die allgemeine Lösung als Superposition der Lösung der homogenen Gleichung und der speziellen Lösung der inhomogenen Gleichung zusammen. Die homogene Gleichung entspricht aber dabei gerade wieder der Laplace-Gleichung, die oben schon gelöst wurde. Es gilt also:
%
\begin{align*}
	\nabla^2\phi_h &= 0\\
	\phi_h(r\to\infty) &= \mathcal{O}\left(\frac{1}{r}\right)\\
	\implies \phi_h &= 0
\end{align*}
%
Wir werden nun zwei verschiedene Wege kennenlernen, um das inhomogene Problem zu lösen.

\paragraph{1. Weg:}
%
Hierbei wollen wir das Coulombgesetz, sowie das Superpositionsprinzip ausnutzen. Damit gilt im kontinuierlichen Fall:
%
\begin{align*}
	\bm{E} &= \int_{V}\frac{\bm{r} - \bm{r}'}{|\bm{r} - \bm{r}'|^3}\varrho(\bm{r}')\,\mathrm{d}V'\\
	&= -\nabla_\bm{r} \int_{V}\frac{\varrho(\bm{r}')}{|\bm{r} - \bm{r}'|}\,\mathrm{d}V'\\
	&= -\nabla\phi
\intertext{weiterhin gilt:}
	\div\bm{E} &= -4\pi\varrho\\
	\implies \nabla^2\phi &= -4\pi\varrho
\end{align*}
%
Damit haben wir die Grundaufgabe der Elektrostatik (im $\mathbb{R}^3$) gelöst und
%
\begin{align*}
	\boxed{
		\phi(\bm{r}) = \int_{\mathbb{R}^3}\frac{\varrho(\bm{r}')}{|\bm{r} - \bm{r}'|}\,\mathrm{d}V'
	}
\end{align*}
%
als Potential erhalten.

\paragraph{2. Weg:}
%
Dieser Weg führt uns über die \acct{Greensche Funktion}. Diese wird als Potential $G(\bm{r} - \bm{r}')$ für eine Einheitspunktladung $\varrho(\bm{r})= \delta(\bm{r} - \bm{r}')$ mit der Randbedingung $\mathcal{O}(\frac{1}{r})$ angenommen. Es gilt also:
%
\begin{align*}
	\nabla^2G(\bm{r} - \bm{r}') &= -4\pi\delta(\bm{r} - \bm{r}').
\end{align*}
%
Dabei wirkt der Laplace- Operator nur auf $\bm{r}$. Es sei auch angemerkt, dass die Greensche Funktion im Allgemeinen nicht von obiger Form ist. Lediglich bei translationsinvarianten Problemen wird diese Form angenommen. Nutzen wir nun wieder das Superpositionsprinzip aus, so ergibt sich für eine beliebige Ladungsverteilung:
%
\begin{align*}
	\phi(\bm{r}) &= \int_{\mathbb{R}^3}G(\bm{r} - \bm{r}')\,\mathrm{d}V'\\
	\implies \nabla^2\phi(\bm{r}) &= \int_{\mathbb{R}^3}\underbrace{\nabla^2G(\bm{r} - \bm{r}')}_{= -4\pi\delta(\bm{r} - \bm{r}')}\varrho(\bm{r}')\,\mathrm{d}V' = -4\pi\varrho(\bm{r})
\end{align*}
%
Wir bestimmen $G(\bm{r})$ mittels \acct*{Fouriertransformation}\/:
%
\begin{align*}
	\nabla^2G(\bm{r}) &= -4\pi\delta(\bm{r}) \; , \quad G(\bm{r})= \int\,\frac{\mathrm{d}^3k}{(2\pi)^3}\hat{G}(\bm{k})\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}\\
	\nabla^2G(\bm{r}) &= \int\,\frac{\mathrm{d}^3k}{(2\pi)^3}(\mathrm{i\bm{k}})^2\hat{G}(\bm{k})\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}\\
	\delta(\bm{r}) &= \int\,\frac{\mathrm{d}^3k}{(2\pi)^3}1\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}\\
	\implies \int\,\frac{\mathrm{d}^3k}{(2\pi)^3}(\mathrm{i\bm{k}})^2\hat{G}(\bm{k})\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}} &= -4\pi\int\,\frac{\mathrm{d}^3k}{(2\pi)^3}1\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}\\
	\implies k^2\hat{G}(\bm{k}) &= 4\pi \; , \quad \text{d.h.\ } \hat{G}(\bm{k}) = \frac{4\pi}{k^2}\\
	\implies G(\bm{r}) &= \frac{1}{(2\pi)^3}\int_{0}^{\infty}\,\mathrm{d}k\,k^2\hat{G}(k^2)\int\,\mathrm{d}\Omega_k\,\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}
\end{align*}
%

\begin{notice}[Nebenrechnung:]
%
\begin{align*}
	I(\bm{r}) &\coloneq \int\,\mathrm{d}\Omega_k\,\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}\qquad , \qquad\text{$D$: Drehmatrix (Drehung)}\\
	\implies I(D\bm{r}) &= \int\,\mathrm{d}\Omega_k\,\mathrm{e}^{\mathrm{i}\bm{k}\cdot D\bm{r}} = \int\,\mathrm{d}\Omega_k\,\mathrm{e}^{\mathrm{i}(D^{-1}\bm{k})\cdot\bm{r}} = \int\,\mathrm{d}\Omega_{D^{-1}k}\,\mathrm{e}^{i(D^{-1}\bm{k})\cdot\bm{r}} = \int\,\mathrm{d}\Omega_k\,\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}} = I(\bm{r})
\end{align*}
%
Dabei wurde ausgenutzt, dass das Skalarprodukt invariant unter Drehungen ist, sodass letztendlich gezeigt wurde, dass das gesamte Integral invariant unter Drehungen ist. 
\end{notice}
%
Damit kann der Vektor $\bm{r}$ beliebig gedreht werden:
%
\begin{align*}
	I(\bm{r}) &= I(r = |\bm{r}|) = I\left(\begin{pmatrix}
		0\\
		0\\
		r
	\end{pmatrix}\right)
	\intertext{Wenn $\bm{r}$ also in $z$- Richtung zeigt, folgt für das Integral $I(\bm{r})$:}
	I(\bm{r}) &= I(r) = \int_{0}^{\pi}\,\mathrm{d}\vartheta_k\,\sin\vartheta_k\,\int_{0}^{2\pi}\,\mathrm{d}\varphi_k\,\mathrm{e}^{\mathrm{i}kr\cos\vartheta_k}\\
	&\stackrel{x= \cos\vartheta_k}{=} 2\pi\int_{-1}^{1}\,\mathrm{d}x\,\mathrm{e}^{\mathrm{i} k r x} = 4\pi\frac{\sin(k r)}{k r}\\
	\implies G(\bm{r}) &= \frac{1}{8\pi^3}\,4\pi\,\int_{0}^{\infty}\,\mathrm{d}k\,4\pi\frac{\sin(k r)}{k r} = \frac{2}{\pi}\frac{1}{r}\underbrace{\int_{0}^{\infty}\,\mathrm{d}x\frac{\sin x}{x}}_{=\frac{\pi}{2}}
\end{align*}
%
Damit haben wir also insgesamt gefunden:
%
\begin{align*}
	\boxed{G(\bm{r}) = \frac{1}{|\bm{r}|}}
\end{align*}
%
Für unsere Konstruktion mit $\nabla^2G(\bm{r} - \bm{r}')= -4\pi\delta(\bm{r} - \bm{r}')$ gilt:
%
\begin{align*}
	\boxed{
	\nabla^2\frac{1}{|\bm{r} - \bm{r}'|} = -4\pi\delta(\bm{r} - \bm{r}')
	}
\end{align*}
%
\subsection{Magnetostatik im \texorpdfstring{$\mathbb{R}^3$}{ℝ³}}
%
$\div\bm{B}= 0$ wird durch den Ansatz $\bm{B}= \rot\bm{A}$ gewährleistet.

\begin{notice}
	Das \acct*{Vektorpotential} $\bm{A}$ ist nicht eindeutig. Bilden wir die sogenannte \acct*{Eichtransformation}, d.h. $\bm{A}\to\bm{A}'$, so gilt:
	%
	\begin{align*}
		\bm{A}' &= \bm{A} + \nabla\xi\\
		\implies \underbrace{\rot\bm{A}'}_{= \bm{B}'} &= \underbrace{\rot\bm{A}}_{= \bm{B}} + \underbrace{\rot(\nabla\xi)}_{= 0}
	\end{align*}
\end{notice}

Dabei wird die >>Eichung<< so gewählt, dass
%
\begin{align*}
	\boxed{
		\div\bm{A} = 0\qquad\text{\acct{Coulombeichung}}
	}
\end{align*}
%
Dies ist stets möglich, denn wenn $\div\bm{A}= 4\pi\lambda\neq 0$; dann bilde man $\bm{A}' = \bm{A} + \nabla\xi$ so, dass $0= \div\bm{A}'= \div\bm{A} + \nabla^2\xi= 4\pi\lambda + \nabla^2\xi$.

Falls $\div\bm{A}= 4\pi\lambda\neq 0$, dann lässt sich stets eine Eichtransformation auf $\bm{A}'= \bm{A} + \nabla\xi$, mit $\xi$ als Lösung der Poissongleichung $\nabla^2\xi= -4\pi\lambda$, finden, so dass $\div\bm{A}'= 0$.

Damit lässt sich die zweite Maxwellgleichung schreiben als
%
\begin{align*}
	\frac{4\pi}{c}\bm{j} &= \rot\bm{B} = \nabla\times(\nabla\times\bm{A})\\
	&= \underbrace{\nabla(\nabla\cdot\bm{A})}_{\mathclap{= 0 \hateq \text{Coulombeichung}}} - \nabla^2\bm{A}
\end{align*}
%
denn
%
\begin{align*}
	\varepsilon_{\alpha\beta\gamma}\partial_{\beta}\varepsilon_{\gamma\mu\nu}\partial_{\mu}A_{\nu} &= \left(\delta_{\alpha\mu}\delta_{\beta\nu} - \delta_{\alpha\nu}\delta_{\beta\mu}\right)\partial_{\beta}\partial_{\mu}A_{\nu}\\
	&= \partial_{\beta}\partial_{\alpha}A_{\beta} - \partial_{\beta}\partial_{\beta}A_{\alpha}\\
	&= \left(\nabla(\div\bm{A}) - \nabla^2\bm{A}\right)_{\alpha})\\
	\implies \nabla^2\bm{A} &= -\frac{4\pi}{c}\;|\,\div\\
	\implies \nabla^2\div\bm{A} &= -\frac{4\pi}{c}\div\bm{j} = 0,
\end{align*}
%
wegen Magnetostatik. Damit folgt, dass $\div\bm{A}= 0$, falls $\div\bm{A}= \mathcal{O}(1/r)$, womit die Coulombeichung erfüllt wäre.

In kartesischen Koordinaten gilt:
%
\begin{align*}
	\nabla^2 A_{\alpha} &= -\frac{4\pi}{c}j_{\alpha} \; , \quad \alpha= x, y, z\; (\hateq \text{drei Poissongleichungen})
\end{align*}
%
Wir erhalten damit folgenden Ausdruck für das Vektorpotential $\bm{A}(\bm{r})$:
%
\begin{align*}
	\boxed{
	\bm{A}(\bm{r}) = \frac{1}{c}\int_{\mathbb{R}^3}\,\frac{\bm{j}(\bm{r}')}{|\bm{r} - \bm{r}'|}\,\mathrm{d}V'
	}
\end{align*}
%
Dies fällt wie $1/r$ ab. Für das Magnetfeld $\bm{B}(\bm{r})$ folgt damit:
%
\begin{align*}
	\bm{B}(\bm{r}) &= \nabla\times\bm{A}(\bm{r})= \frac{1}{c}\int_{\mathbb{R}^3}\,\nabla\times\frac{\bm{j}(\bm{r}')}{|\bm{r} - \bm{r}'|}\,\mathrm{d}V'\\
	&= -\frac{1}{c}\int_{\mathbb{R}^3}\,\mathrm{d}V'\bm{j}(\bm{r}')\times\nabla\frac{1}{|\bm{r} - \bm{r}'|}
\end{align*}
%
Denn:
%
\begin{align*}
	\varepsilon_{\alpha\beta\gamma}\partial_{\beta}\left[j_{\gamma}f\right] &= \varepsilon_{\alpha\beta\gamma}j_{\alpha}\partial_{\beta}f = -\varepsilon_{\alpha\gamma\beta}j_{\gamma}\partial_{\beta}f = -\left(\bm{j}\times\nabla f\right)_{\alpha}
\end{align*}
%
Damit folgt das \acct{verallgemeinerte Biot-Savart Gesetz}:
%
\begin{align*}
	\bm{B}(\bm{r}) &= \frac{1}{c}\int_{\mathbb{R}^3}\,\mathrm{d}V'\bm{j}(\bm{r}')\times\frac{\bm{r} - \bm{r}'}{|\bm{r} - \bm{r}'|}
\end{align*}

\begin{notice}[Check:]
	\begin{align*}
		\div\bm{B} &= -\frac{1}{c}\int_{\mathbb{R}^3}\,\mathrm{d}V'\nabla\cdot\left(\bm{j}(\bm{r}')\times\nabla\frac{1}{|\bm{r} - \bm{r}'|}\right)\\
		&= \frac{1}{c}\int_{\mathbb{R}^3}\,\mathrm{d}V'\,\bm{j}(\bm{r}')\cdot\underbrace{\rot\left(\nabla\frac{1}{|\bm{r} - \bm{r}'|}\right)}_{= 0} = 0
	\end{align*}
	%
	Denn:
	%
	\begin{align*}
		\div(\bm{j}\times\bm{g}) &= \partial_{\alpha}\varepsilon_{\alpha\beta\gamma}j_{\beta}g_{\gamma}= -j_{\beta}\varepsilon_{\beta\alpha\gamma}\partial_{\alpha}g_{\gamma} = -\bm{j}\rot\bm{g}
	\end{align*}
\end{notice}
