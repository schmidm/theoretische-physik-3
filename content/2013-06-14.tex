% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 14.06.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-06-14}

\begin{notice}[Zusammenfassung:]
	Elektromagnetische Wellen im Vakuum erfüllen die Wellengleichung
	%
	\begin{align*}
		\quabla \bm{E} &= 0 = \quabla \bm{B} \\
		\bm{E}(\bm{r},t) &= \Re \int \frac{\mathrm{d}^3 k}{(2\pi)^3} \tilde{\bm{E}}(\bm{k}) \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - \omega(k) t)} \\
		\bm{B}(\bm{r},t) &= \Re \int \frac{\mathrm{d}^3 k}{(2\pi)^3} \tilde{\bm{B}}(\bm{k}) \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - \omega(k) t)}
	\end{align*}
	%
	mit $\omega(k) = c |\bm{k}|$.

	$\tilde{\bm{E}}(\bm{k}), \tilde{\bm{B}}(\bm{k}) \in \mathbb{C}^3$ ist die komplexe Amplitude der Wellen. Phase und Polarisation stammen aus den Anfangsbedingungen.
	%
	\begin{align*}
		\nabla \cdot \bm{E} &= 0 = \nabla \cdot \bm{B} \implies \bm{k} \cdot \tilde{\bm{E}}(\bm{k}) = 0 = \bm{k} \cdot \tilde{\bm{B}}(\bm{k}) \\
		\nabla \times \bm{E} &= -\frac{1}{c} \partial_t \bm{B} \implies \tilde{\bm{B}}(\bm{k}) = \hat{\bm{k}} \times \tilde{\bm{E}}(\bm{k})
	\end{align*}
\end{notice}

\section[Die elektromagnetischen Potentiale]{Die elektromagnetischen Potentiale (Elektrodynamik als Eichfeldtheorie)}

Aus der Statik ist bekannt, dass
%
\begin{align*}
	\bm{E}(\bm{r}) &= - \nabla \phi(\bm{r}) \\
	\bm{B}(\bm{r}) &= \nabla \times \bm{A}(\bm{r})
\end{align*}

\begin{notice}[Ziele:]
	\begin{itemize}
		\item Verallgemeinerung auf zeitabhängige Felder
		\item Lösung der >>vollen<< Maxwellschen Gleichungen mit vorgegebenen $\varrho(\bm{r},t)$ und $\bm{j}(\bm{r},t)$.
	\end{itemize}
\end{notice}

Mit den homogenen Maxwellgleichungen erhalten wir
%
\begin{gather*}
	\begin{rcases}
		\div \bm{B} = 0 \implies \bm{B} = \rot \bm{A} \\
		\rot \bm{E} + \frac{1}{c} \partial_t \bm{B} = 0
	\end{rcases}
	\implies \rot \left( \bm{E} + \frac{1}{c} \partial_t \bm{A} \right) = 0
	\implies  \bm{E} + \frac{1}{c} \partial_t \bm{A} = - \nabla \phi
\end{gather*}

\begin{notice}[Fazit:]
	Die homogenen Maxwellschen Gleichungen werden mit folgenden Ansätzen identisch erfüllt:
	%
	\begin{align}
		\boxed{\bm{B} = \rot \bm{A} \; , \quad \bm{E} = - \nabla \phi - \frac{1}{c} \partial_t \bm{A}} \label{eq:2013-06-14-Pot}
	\end{align}
\end{notice}

Die -- hier noch relativ zueinander beliebigen -- Potentiale $\phi(\bm{r},t)$ und $\bm{A}(\bm{r},t)$ werden durch die inhomogenen Maxwellschen Gleichungen in Verbindung mit einer \acct{Eichtransformation} festgelegt.

\paragraph{Inhomogene Maxwellgleichungen}

\begin{align}
	\nabla \cdot \bm{E} = 4 \pi \varrho &= - \nabla^2 \phi - \frac{1}{c} \partial_t \nabla \cdot \bm{A} \label{eq:2013-06-14-I} \tag{I} \\
	\nabla \times \bm{B} - \frac{1}{c} \partial_t \bm{E} = \frac{4 \pi}{c} \bm{j} &= \nabla \times \left( \nabla \times \bm{A} \right) + \frac{1}{c} \partial_t \nabla \phi + \frac{1}{c^2} \partial_t^2 \bm{A} \label{eq:2013-06-14-IV} \tag{IV} \\
	&= - \left( \nabla^2 - \frac{1}{c^2} \partial_t^2 \right) \bm{A} + \nabla \left( \nabla \cdot \bm{A} - \frac{1}{c} \partial_t \phi \right) \notag
\end{align}

Nun stellt sich die Frage, was ist die Divergenz von $\bm{A}$? ($\nabla \cdot \bm{A}$)

\paragraph{Erste Wahl:} Lorenzeichung
%
\begin{align}
	\boxed{\nabla \cdot \bm{A} + \frac{1}{c} \partial_t \phi = 0} \label{eq:2013-06-14-LE}
\end{align}
%
Unter dieser Eichung transformieren sich die Gleichungen \eqref{eq:2013-06-14-I} und \eqref{eq:2013-06-14-IV} folgendermaßen:
%
\begin{align}
	\begin{gathered}
		\text{\eqref{eq:2013-06-14-I}} \\
		\text{\eqref{eq:2013-06-14-IV}}
	\end{gathered}
	 \implies
	\boxed{
	\begin{aligned}
		\left( \nabla^2 - \frac{1}{c^2} \partial_t^2 \right) \phi &= - 4 \pi \varrho \\
		\left( \nabla^2 - \frac{1}{c^2} \partial_t^2 \right) \bm{A} &= - \frac{4 \pi}{c} \bm{j}
	\end{aligned}
	}
	\label{eq:2013-06-14-IWG}
\end{align}

Diese inhomogenen Wellengleichungen sind mit \eqref{eq:2013-06-14-Pot} und mit \eqref{eq:2013-06-14-LE} äquivalent zu den vollen Maxwellschen Gleichungen, d.h.\ gesucht werden nur solche Lösungen der inhomogenen Wellengleichung \eqref{eq:2013-06-14-IWG}, welche  der \eqref{eq:2013-06-14-LE} genügen.

Sei $(\phi,\bm{A})$ eine auf diese Weise gefundene Lösung von \eqref{eq:2013-06-14-LE} und \eqref{eq:2013-06-14-IWG}.

\begin{notice}[Frage:]
	Wie viele andere Potentiale $(\phi',\bm{A}')$ gibt es, die im Allgemeinen zwar nicht \eqref{eq:2013-06-14-LE} und \eqref{eq:2013-06-14-IWG} lösen, aber die gleichen physikalischen Felder $\bm{E}$ und $\bm{B}$ proudzieren, wie $(\phi,\bm{A})$?
\end{notice}

\begin{notice}[Antwort:]
	Es gibt unendlich viele solche Potentiale $(\phi',\bm{A}')$ und sie gehen aus $(\phi,\bm{A})$ durch sogenannte \acct{Eichtransformation} hervor. 
\end{notice}
%
\begin{gather*}
	\boxed{
	\begin{aligned}
		\bm{A}' &= \bm{A} + \nabla \Lambda \\
		\phi' &= \phi - \frac{1}{c} \partial_t \Lambda
	\end{aligned}
	}
\end{gather*}
%
Dabei ist $\Lambda(\bm{r},t)$ ein beliebiges skalares Vektorfeld.
%
\begin{align*}
	\bm{B}'
	&= \nabla \times \bm{A}'
	= \nabla \times \left( \bm{A} + \nabla \Lambda \right)
	= \nabla \times \bm{A}
	= \bm{B}
	\\
	\bm{E}'
	&= - \nabla \phi' - \frac{1}{c} \partial_t \bm{A}'
	= - \nabla \left[ \phi - \frac{1}{c} \partial_t \Lambda \right] - \frac{1}{c} \partial_t \left[ \bm{A} + \nabla \Lambda \right] \notag \\
	&= - \nabla \phi - \frac{1}{c} \partial_t \bm{A}
	= \bm{E}
\end{align*}

\begin{notice}
	\begin{enumerate}
		\item \label{itm:2013-06-14-1} Zu jedem beliebigen Paar $(\phi',\bm{A}')$ lässt sich eine Eichtransformation finden, sodass das transformierte Paar $(\phi'',\bm{A}'')$ der Lorenzeichung \eqref{eq:2013-06-14-LE} genügt.
			\begin{proof}
				Seien $(\phi',\bm{A}')$ beliebig.
				%
				\begin{align*}
					\nabla \cdot \bm{A}' + \frac{1}{c} \partial_t \phi' = \Gamma \neq 0 \quad \text{i.a.}
				\end{align*}
				%
				Forderung
				%
				\begin{align*}
					& \nabla \times \bm{A}'' + \frac{1}{c} \partial_t \phi'' = 0 \\
					\iff& \nabla \left[ \bm{A}' + \nabla \Lambda \right] + \frac{1}{c} \partial_t \left[ \phi' \frac{1}{c} \partial_t \Lambda \right] = 0 \\
					\iff& \underbrace{\nabla \times \bm{A}' + \frac{1}{c} \partial_t \phi'}_{\Gamma} + \nabla^2 \Lambda - \frac{1}{c^2} \partial_t^2 \Lambda = 0 \\
					\iff& \quabla \Lambda = - \Gamma
				\end{align*}
				%
				Die Lösung dieser inhomogenen Wellengleichung liefert die gesuchte Eichtransformation.
			\end{proof}

		\item \label{itm:2013-06-14-2} Es gibt unendlich viele Paare $(\phi'',\bm{A}'')$, die der Lorenzeichung \eqref{eq:2013-06-14-LE} genügen. Sie gehen untereinander durch eingeschränkte Eichtransformation $\Lambda_0$ hervor, die der Wellengleichung $\quabla \Lambda_0 = 0$ genügen. Hierfür gibt es unendlich viele Lösungen.

			Die inhomogenen Maxwellgleichungen ohne Einchung lauten
			%
			\begin{subequations}
				\begin{gather}
					\nabla^2 \phi + \frac{1}{c} \partial_t (\nabla \cdot \bm{A}) = - 4 \pi \varrho \label{eq:2013-06-14-Stern1} \tag{*} \\
					\quabla \bm{A} - \nabla \left( \nabla \bm{A} + \frac{1}{c} \partial_t \phi \right) = - \frac{4 \pi}{c} \bm{j} \label{eq:2013-06-14-Stern2} \tag{**}
				\end{gather}
			\end{subequations}
			%
			Eine Entkopplung findet durch die Lorenzeichung ($\nabla \cdot \bm{A} + \frac{1}{c} \partial_t \phi) = 0$) oder durch die Coulombeichung ($\nabla \cdot \bm{A} = 0$) statt.

			Sei $(\phi,\bm{A})$ eine Lösung der gekoppelten Gleichungen, das der Lorenzeichung nicht genügt, d.h.
			%
			\begin{align*}
				\nabla \cdot \bm{A} + \frac{1}{c} \partial_t \phi = \Gamma \neq 0
			\end{align*}
			%
			Jetzt wird eine Eichtransformation $(\phi,\bm{A}) \to (\phi',\bm{A}')$, sodass $(\phi',\bm{A}')$ die Lorenzeichung erfüllt.
			$\quabla \Lambda = - \Gamma$ ist die Bestimmungsgleichung für diese Eichtransformation.

			Aus
			%
			\begin{align*}
				\phi' &= \phi - \frac{1}{c} \partial_t \Lambda \\
				\bm{A}' &= \bm{A} + \nabla \Lambda
			\end{align*}
			%
			folgt
			%
			\begin{align*}
				-4\pi \varrho
				&= \nabla^2 \phi + \frac{1}{c} \partial_t \nabla \cdot \bm{A} \\
				&= \nabla^2 \left( \phi' + \frac{1}{c} \partial_t \Lambda \right) + \frac{1}{c} \partial_t \nabla \left( \bm{A}' - \nabla \Lambda \right) \\
				&= \nabla^2 \phi' + \frac{1}{c} \partial_t \nabla \cdot \bm{A} + \frac{1}{c} \partial_t \nabla^2 \Lambda - \frac{1}{c} \partial_t \nabla^2 \Lambda \\
				&= \nabla^2 \phi' + \frac{1}{c} \partial_t \nabla \cdot \bm{A}'
			\end{align*}
			%
			und
			%
			\begin{align*}
				- \frac{4 \pi}{c} \bm{j}
				&= \quabla \bm{A} - \nabla \left( \nabla \cdot \bm{A} + \frac{1}{c} \partial_t \phi \right) \\
				&= \quabla \left[ \bm{A}' - \nabla \Lambda \right] - \nabla \left( \nabla \left[ \bm{A}' - \nabla \Lambda \right] + \frac{1}{c} \partial_t \left[ \phi' + \frac{1}{c} \partial_t \Lambda \right] \right) \\
				&= \quabla \bm{A}' - \nabla \quabla \Lambda - \nabla \left( \nabla \cdot \bm{A}' + \frac{1}{c} \partial_t \phi' \right) - \nabla \underbrace{\left( - \nabla^2 \Lambda + \frac{1}{c^2} \partial_t^2 \Lambda \right)}_{-\quabla \Lambda} \\
				&= \quabla \bm{A}' - \nabla \left( \nabla \cdot \bm{A}' + \frac{1}{c} \partial_t \phi \right)
			\end{align*}
			%
			Daraus folgt, dass \eqref{eq:2013-06-14-Stern1} und \eqref{eq:2013-06-14-Stern2} invariant sind unter Eichtransformation.

			Jede Lösung von \eqref{eq:2013-06-14-Stern1} und \eqref{eq:2013-06-14-Stern2} kann durch eine geeignete Eichtransformation $\Lambda$ ($\quabla \Lambda = - \nabla \cdot \bm{A} + \frac{1}{c} \partial_t \phi$ im Falle der Lorenzeichung) auf eine Form gebracht werden, sodass sie die inhomogene Wellengleichung im Fall der Lorenzeichung (bzw.\ die entsprechenden Gleichungen um Fall der Coulombeichung, s.u.) erfüllt.

		\item Findet man Lösungen $(\tilde{\phi},\tilde{\bm{A}})$ der inhomogenen Wellengleichung die der Lorenzeichung nicht genügen und unterwirft diese einer Eichtransformation $\Lambda$ gamäß Punkt~\ref{itm:2013-06-14-1} um $(\hat{\phi},\hat{\bm{A}})$ zu erhalten, so genügen letzere zwar der Lorenzeichung, sind aber keine Lösungen der inhomogenen Wellengleichung mehr, sondern entspricht einer Lösung zu
			%
			\begin{equation}
				\begin{aligned}
					\quabla \hat{\phi} &= \quabla \tilde{\phi} - \frac{1}{c} \partial_t \quabla \Lambda = -4 \pi \varrho - \frac{1}{c} \partial_t \quabla \Lambda \\
					\quabla \hat{\bm{A}} &= \quabla \tilde{\bm{A}} + \nabla \quabla \Lambda = - \frac{4 \pi}{c} \bm{j} + \nabla \quabla \Lambda
				\end{aligned}
				\label{eq:2013-06-14-Kreuz} \tag{\#}
			\end{equation}
			%
			Da $(\tilde{\phi},\tilde{\bm{A}})$ die Lorenzeichung nicht erfüllen und $\Lambda$ gemäß \eqref{eq:2013-06-14-Stern1}, so bestimmt wurde, dass $(\hat{\phi},\hat{\bm{A}})$ die Lorenzeichung erfüllen, muss $\quabla \Lambda = - \Gamma \neq 0$.

			Andererseits zeigt \eqref{eq:2013-06-14-Kreuz}, dass $(\tilde{\phi},\tilde{\bm{A}})$ der Lorenzeichung genügen und die inhomogene Wellengleichung erfüllen und nur einer eingeschränkten Eichtransformation $\Lambda_0$ mit $\quabla \Lambda_0 = 0$ unterworfen werden, sodass die Lorenzeichung erhalten bleibt (siehe~\ref{itm:2013-06-14-2}), dann bleiben die transformierten Paare nach wie vor Lösungen der inhomogenen Wellengleichung.

			Lösungen der inhomogenen Wellengleichung, die die Lorenzeichung nicht erfüllen (und damit keine Lösung von \eqref{eq:2013-06-14-Stern1} und \eqref{eq:2013-06-14-Stern2} sind) werden durch Umeichung (die zur Erfüllung der Lorenzeichung führt) noch keine Lösung von \eqref{eq:2013-06-14-Stern1} und \eqref{eq:2013-06-14-Stern2}.

		\item Ein Vorteil der Lorenzeichung ist, dass eine gleichartige Behandlung von $\phi$ und $\bm{A}$ stattfindet (Dies lässt sich auch lorenzinvariant formulieren).
	\end{enumerate}
\end{notice}

\paragraph{Zweite Wahl:} Coulombeichung
%
\begin{align*}
	\boxed{\nabla \cdot \bm{A} = 0}
\end{align*}
%
Auch in diesem zweiten Fall lässt sich jedes Paar $(\phi',\bm{A}')$ durch eine Eichtransformation in ein Paar $(\phi'',\bm{A}'')$ überführen, das der Coulombeichung genügt:
%
\begin{align*}
	0 = \nabla \cdot \bm{A}'' = \nabla ( \bm{A} + \nabla \Lambda ) \implies \nabla^2 \Lambda = - \div \bm{A}
\end{align*}
%
ist die Bestimmungsgleichung für die gesuchte Eichtransformation $\Lambda$.
%
\begin{align*}
	\begin{gathered}
		\text{\eqref{eq:2013-06-14-I}} \\
		\text{\eqref{eq:2013-06-14-IV}}
	\end{gathered}
	\implies
	\boxed{
	\begin{aligned}
		\nabla^2 \phi &= - 4 \pi \varrho \\
		\quabla \bm{A} &= - \frac{4 \pi}{c} \bm{j} + \frac{1}{c} \nabla \partial_t \bm{j} \eqcolon - \frac{4 \pi}{c} \bm{j}^\perp
	\end{aligned}
	}
\end{align*}
%
Bedeutung von $\bm{j}^\perp$:
%
\begin{align*}
	\bm{j}^\perp &= \bm{j} - \frac{1}{4 \pi} \nabla \partial_t \phi \\
	\div \bm{j}^\perp
	&= \div \bm{j} - \frac{1}{4 \pi} \partial_t \nabla^2 \phi \\
	&= \div \bm{j} - \frac{1}{4 \pi} \partial_t (-4 \pi \varrho) \\
	&= \div \bm{j} + \partial_t \varrho = 0
\end{align*}
%
\begin{align*}
	\bm{j}^\perp(\bm{r},t)
	&= \int \frac{\mathrm{d}^3 k}{(2 \pi)^3} \tilde{\bm{j}}^\perp(\bm{k},t) \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \\
	\div \bm{j}^\perp &= 0 \implies \tilde{\bm{j}}^\perp(\bm{k},t) \cdot \bm{k} = 0
\end{align*}
%
daher >>transversale<< Stromdichte. Vorteil der Coulombeichung ist, dass wenn $\varrho(\bm{r},t)$ im ganzen $\mathbb{R}^3$ bekannt ist, dann gilt:
%
\begin{align*}
	\phi(\bm{r},t) &= \int \frac{\varrho(\bm{r}',t)}{|\bm{r}- \bm{r}'|} \mathrm{d}V' \quad \text{(gilt nur für die Coulombeichung)}
\end{align*}
