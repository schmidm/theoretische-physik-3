% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 12.07.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-07-12}

$\mathrm{d}\tau(t)$ ist das Zeitdifferential einer mit dem Teilchen mitbewegten Uhr.
%
\begin{align*}
	\underbrace{
		\int_{t_A}^{t_B} \underbrace{
			\sqrt{1 - \frac{v^2(t)}{c^2}}
		}_{\leq 1} \, \mathrm{d}t
	}_{\leq t_B - t_A}
	&= \tau_B - \tau_A \\
	\leadsto \tau_B &- \tau_A < t_B - t_A
\end{align*}
%
$\tau_B - \tau_A$ ist die Zeit, die zwischen $A$ und $B$ für einen mitbewegten Beobachter versteicht. Die entspricht einer Zeitdilatation.
Dies kann wieder beobachtet werden bei der Zerfallszeit $T_0$ ruhender Pionen. Die Zerfallszeit in der Eigenzeit der in der kosmischen Höhenstrahlungen einfallenden Pionen ist größer als $T_\mathrm{Lab}$.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.5,-0.5)(2,2)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.5,-0.5)(2,2)[{\color{DimGray} $\bm{r}$},-90][{\color{DimGray} $c t$},0]
		\psline[linecolor=DarkOrange3,ArrowInside=->](0,0)(0,1.5)
		\psecurve[linecolor=MidnightBlue,arrows=->](0,-0.5)(0,0)(1,0.75)(0,1.5)
		\psecurve[linecolor=MidnightBlue](0,0)(1,0.75)(0,1.5)(0,2)
		\uput[-45](0,0){\color{DimGray} $c t_0$}
		\psyTick(1.5){\color{DimGray} c t_1}
		\uput[180](0,0.75){\color{DarkOrange3} $z_1$}
		\uput[0](1,0.75){\color{MidnightBlue} $z_2$}
	\end{pspicture}
	\caption{Zwillingsparadoxon}
	\label{fig:2013-07-12-1}
\end{figure}

In Abbildung~\ref{fig:2013-07-12-1} ist das Zwillingsparadoxon dargestellt. Das Alter verändert sich wie folgt
%
\begin{align*}
	z_1 &= t_1 - t_0 \\
	z_2 &= \tau_1 - \tau_0 < t_1 - t_0
\end{align*}
%
Der bewegte Zwilling bleibt scheinbar jünger als der ruhende Zwilling.

\begin{notice}
	Das Relativitätsprinzip scheint verletzt, aber das Ruhesystem von $z_2$ ist kein Interialsystem. Folglich ist $z_1 \leftrightarrow z_2$ keine Lorentztransformation. $z_2$ wird beschleunigt und damit gegenüber $z_1$ ausgezichnet.
\end{notice}

\section{Vierervektoren}

Im Euklidischen Raum erlaubt das Vektorkalkül physikalische Gesetze manifest basisunabhängig zu formulieren. Das Relativitätsprinzip legt nahe, eine analoge basisunabhängige Vektorschreibweise für die 4-dimensionale Raumzeit einzuführen.
%
\begin{align*}
	x^\mu = (ct,\underbrace{x,y,z}_{\bm{r}}) = (x^0,x^1,x^2,x^3) = (x^0,x^i)
\end{align*}
% 
Griechische Indizes laufen von $0$ ab, lateinische Indizes von $1$.

\paragraph{Poincar\'e-Transformation}

\begin{align*}
	x^\mu \to {x'}^\mu = \Lambda^\mu{}_\nu x^\nu + a^\mu
\end{align*}

Zunächst betrachten wir den Tensor $\Lambda^\mu{}_\nu$, der beliebig ist, bis auf die Eigenschaft $\det(\Lambda^\mu{}_{\nu}) \neq 0$.
%
\begin{align*}
	\mathrm{d}{x'}^\mu = \frac{\partial {x'}^\mu}{\partial x^\nu} \mathrm{d}x^\nu = \Lambda^\mu{}_\nu \mathrm{d}x^\nu
\end{align*}
%
Die spezielle Relativitätstheorie behandelt nur Fälle, in denen die Einträge $\Lambda^\mu{}_\nu$ und die $a^\mu$ konstant sind.
%
\begin{align*}
	\mathrm{d}x^\mu = \frac{\partial x^\mu}{\partial {x'}^\nu} \mathrm{d}{x'}^\nu = \bar{\Lambda}^\mu{}_\nu \mathrm{d}{x'}^\nu
\end{align*}
%
Die $\Lambda$ erfüllen folgende Eigenschaft
%
\begin{align*}
	\Lambda^\kappa{}_\mu \bar{\Lambda}^\mu{}_\lambda
	= \frac{\partial {x'}^\kappa}{\partial x^\mu} \frac{\partial x^\mu}{\partial {x'}^\lambda}
	= \frac{\partial {x'}^\kappa}{\partial {x'}^\lambda}
	= \delta^\kappa{}_\lambda
	= \begin{cases}
		1 & \kappa = \lambda \\
		0 & \mathrm{sonst}
	\end{cases}
\end{align*}

\begin{align*}
	\frac{\partial}{\partial {x'}^\mu} 
	&= \frac{\partial x^\nu}{\partial {x'}^\mu} \frac{\partial}{\partial x^\nu}
	= \bar{\Lambda}^\nu{}_\mu \frac{\partial}{\partial x^\nu} \\
	%
	\frac{\partial}{\partial x^\mu} 
	&= \frac{\partial {x'}^\nu}{\partial x^\mu} \frac{\partial}{\partial {x'}^\nu}
	= \Lambda^\nu{}_\mu \frac{\partial}{\partial {x'}^\nu}
\end{align*}

\paragraph{Tensoren im affinen Raum}

\begin{description}
	\item[Skalar] $\phi(x^\mu)$
		\begin{figure}
			\centering
			\begin{pspicture}(-0.8,-0.8)(4,3.5)
				\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(3,3)[{\color{DimGray} $x$},-90][{\color{DimGray} $c t$},180]
				\psline[linestyle=dotted,dotsep=1pt](0,2)(2,2)(2,0)
				\psxTick(2){\color{DimGray} x_P^\mu}
				\cnode*[linecolor=MidnightBlue](2,2){2pt}{P}\uput[45](P){\color{MidnightBlue} $P$}
				\rput(0.5,0.5){
					\psline[linecolor=MidnightBlue](0,0)(3;20)
					\psline[linecolor=MidnightBlue](0,0)(3;70)
					\uput[-90](3;20){\color{MidnightBlue} ${x'}^\mu$}
					\psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](1;20)(P)
					\psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](1;70)(P)
					\psdots*[linecolor=DarkOrange3](1;20)(1;70)
					\uput[-80](1;20){\color{DarkOrange3} ${x'}_P^\mu$}
				}
			\end{pspicture}
			\caption{Der skalare Fall $\phi(x^\mu)$}
		\end{figure}
		%
		\begin{gather*}
			\phi(x_P^\mu) \to \phi'({x'}_P^\mu) \\
			\phi({x'}_P^\mu(x_P^\nu)) = \phi(x_P^\nu)
		\end{gather*}

	\item[kontravarianter Vektor] $\mathbb{V}^1 \to A^\mu = (A^0,A^1,A^2,A^3)$
		\begin{gather*}
			A^\mu \to {A'}^\mu = \Lambda^\mu{}_\nu A^\nu
		\end{gather*}
		%
		$A^\mu$ transformiert sich wie $\mathrm{d}x^\nu$.

	\item[kovarianter Vektor] $\mathbb{V}_1 \to A_\mu = (A_0,A_1,A_2,A_3)$
		\begin{gather*}
			A_\mu \to {A'}_\mu = \bar{\Lambda}^\nu{}_\mu A_\nu
		\end{gather*}
		%
		$A_\mu$ transformiert sich wie $\mathrm{d}{x'}^\nu$
\end{description}

\begin{example} Gradient eines skalaren Feldes:
	\begin{gather*}
		\partial_\mu \phi(x^\nu) \to \partial'_\mu \phi'({x'}^\nu) = \partial'_\mu \phi(x^\kappa({x'}^\lambda)) \\
		= \partial_\nu \phi(x^\kappa) \frac{\partial x^\nu}{\partial {x'}^\mu} = \bar{\Lambda}^\nu{}_\mu \partial_\nu \phi
	\end{gather*}
\end{example}

Betrachte:
%
\begin{align*}
	A^\mu B_\mu \to {A'}^\mu {B'}_\mu
	&= \Lambda^\mu{}_\nu \bar{\Lambda}^\lambda{}_\mu A^\nu B_\lambda \\
	&= \underbrace{\bar{\Lambda}^\lambda{}_\mu \Lambda^\mu{}_\nu}_{\delta^\lambda{}_\nu} A^\nu B_\lambda \\
	&= A^\lambda B_\lambda
\end{align*}
%
Es zeigt sich, dass dies ein invarianter Skalar ist.

Denn:
%
\begin{align*}
	\bar{\Lambda}^\lambda{}_\mu \Lambda^\mu{}_\nu
	&= \frac{\partial x^\lambda}{\partial {x'}^\mu} \frac{\partial {x'}^\mu}{\partial x^\nu} \\
	&= \frac{\partial x^\lambda}{\partial x^\nu} \\
	&= \delta^\lambda{}_\nu
\end{align*}

\begin{notice}
	Auf der Stufe eines affinen Raumes gibt es keinen >>natürlichen<< Isomorphismus zwischen $\mathbb{V}^1$ und $\mathbb{V}_1$. Dies wird erst durch die Metrik geliefert.
\end{notice}

\paragraph{Tensor} $\tensor{T}^{\alpha_1,\dotsc,\alpha_n}{}_{\beta_1,\dotsc,\beta_n} \in \mathbb{V}^m{}_n$

\begin{example}
	\begin{align*}
		\tensor{T}^{\alpha\beta}{}_{\gamma} \to
		\boxed{
			{\tensor{T}'}^{\alpha\beta}{}_{\gamma} = \Lambda^\alpha{}_\kappa \Lambda^\beta{}_\lambda \bar{\Lambda}^\mu{}_\gamma \tensor{T}^{\kappa\lambda}{}_\mu
		}
	\end{align*}

	Transformiert sich wie $\mathrm{d}x^\alpha \, \mathrm{d}x^\beta \, \partial_\gamma$.
\end{example}

\begin{notice}
	Gleiches Symbol für den Tensor und seine Komponenten.
\end{notice}

\begin{example} Die Spur eines Tensors:
	\begin{align*}
		{\tensor{T}'}^\mu{}_\mu = \underbrace{\Lambda^\mu{}_\kappa \bar{\Lambda}^\nu{}_\mu}_{\delta^\nu{}_\kappa} \tensor{T}^\kappa{}_\nu = \tensor{T}^\kappa{}_\kappa
	\end{align*}
\end{example}

Die zusätzliche Struktur der Metrik macht einen affinen Raum zum metrischen Raum.
%
\begin{align*}
	\Aboxed{\mathrm{d}s^2 &= g_{\mu\nu} \mathrm{d}x^\mu \mathrm{d}x^\nu} \\
	\det(g_{\mu\nu}) &\neq 0 \\
	g_{\mu\nu} &= g_{\nu\mu}
\end{align*}
%
$\mathrm{d}s^2$ soll invariant sein, also
%
\begin{align*}
	\mathrm{d}s^2 &= \mathrm{d}{s'}^2 \\
	&= g'_{\alpha\beta} \mathrm{d}{x'}^\alpha \mathrm{d}{x'}^\beta \\
	&= g'_{\alpha\beta} \Lambda^\alpha{}_\mu \Lambda^\beta{}_\nu \mathrm{d}x^\mu \mathrm{d}x^\nu \\
	&= g_{\mu\nu} \mathrm{d}x^\mu \mathrm{d}x^\nu \\
	\implies g_{\mu\nu}
	&= g'_{\alpha\beta} \Lambda^\alpha{}_\mu \Lambda^\beta{}_\nu
\end{align*}
%
\begin{align*}
	\bar{\Lambda}^\mu{}_\lambda \bar{\Lambda}^\mu{}_\kappa g'_{\mu\nu}
	&= g'_{\alpha\beta} \underbrace{\Lambda^\alpha{}_\mu \bar{\Lambda}^\mu{}_\lambda}_{\delta^\alpha{}_\lambda} \underbrace{\Lambda^\beta{}_\nu \bar{\Lambda}^\nu{}_\kappa}_{\delta^\beta{}_\kappa} \\
	\implies g'_{\lambda\kappa} &= \bar{\Lambda}^\mu{}_\lambda \bar{\Lambda}^\nu{}_\kappa
\end{align*}

$g_{\mu\nu}$ ist ein $(0,2)$-Tensor (metrischer Tensor). Eine Metrik benötigt ein Skalarprodukt. Seien dazu $A^\mu, B^\nu \in \mathbb{V}^1$
%
\begin{align*}
	(A,B) = g_{\mu\nu} A^\mu B^\mu = g'_{\kappa\lambda} {A'}^\kappa {B'}^\lambda = (A',B')
\end{align*}
%
Denn:
%
\begin{align*}
	g'_{\kappa\lambda} {A'}^\kappa {B'}^\lambda
	&= \bar{\Lambda}^\mu{}_\lambda \bar{\Lambda}^\nu{}_\kappa \Lambda^\kappa{}_\varepsilon \Lambda^\lambda{}_\delta g_{\mu\nu} A^\varepsilon B^\delta \\
	&= g_{\mu\nu} A^\nu B^\mu \\
	&\overset{!}{{}={}} g_{\nu\mu} A^\nu B^\mu
\end{align*}
%
Die Metrik vermittelt einen basiunabhängigen Isomorphismus zwischen $\mathbb{V}^1$ und $\mathbb{V}_1$.
%
\begin{gather*}
	\boxed{A_\mu = g_{\mu\nu} A^\nu} \\
	\implies (A,B) = g_{\mu\nu} A^\mu B^\nu = A_\nu B^\nu = A^\mu B_\mu
\end{gather*}


