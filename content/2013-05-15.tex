% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 15.05.2013
% TeX: Henri

\renewcommand{\printfile}{2013-05-15}

\paragraph{Einschub zum Fundamentalsatz der Vektoranalysis:}
%
\begin{align*}
	\div \bm{U} &= 4 \pi Q \\
	\rot \bm{U} &= 4 \pi \bm{W}
\end{align*}
%
Wir wissen bereits, dass wenn die Quellen $Q$ und die Wirbel $\bm{W}$ von $\bm{U}$ im Endlichen liegen und $\bm{U}(r \to \infty) = \mathcal{O}\left( \frac{1}{r^2} \right)$ dann legen $Q$ und $\bm{W}$ das Vektorfeld $\bm{U}$ eindeutig fest.

Nun soll $\bm{U}$ explizit aus $Q$ und $\bm{W}$ konstruiert werden. Es wird also die Existenz gezeigt, was zuvor ausgelassen wurde. Betrachte dazu zunächst:
%
\begin{equation*}
	\begin{aligned}
		\div \bm{U}_\ell &= 4 \pi Q \\
                \rot \bm{U}_\ell &= 0
	\end{aligned}
	\quad
	\begin{aligned}
		\div \bm{U}_t &= 0 \\
                \rot \bm{U}_t &= 4 \pi \bm{W}
	\end{aligned}
\end{equation*}
%
wobei der Index $\ell$ für die longitudinale und der Index $t$ für die transversale Komponente steht. Damit:
%
\begin{align*}
	\bm{U}_\ell &= - \nabla \int_{\mathbb{R}^3} \frac{Q(\bm{r}')}{|\bm{r} - \bm{r}'|} \mathrm{d}^3 r' \\
	\bm{U}_t &= \nabla \times \int_{\mathbb{R}^3} \frac{\bm{W}(\bm{r}')}{|\bm{r} - \bm{r}'|} \mathrm{d}^3 r' \\
	\bm{U} &\coloneq \bm{U}_\ell + \bm{U}_t \implies
	\begin{aligned}[t]
		&\div \bm{U} = 4 \pi Q \\
		&\text{und } \rot \bm{U} = 4 \pi \bm{W} \\
		&\text{und } \bm{U}(r \to \infty) = \mathcal{O}\left( \frac{1}{r^2} \right)
	\end{aligned}
\end{align*}

\begin{notice}[Fazit:]
	Bei vorgegebener Ladungs- und Stromkonfiguration im $\mathbb{R}^3$ sind Elektro- und Magentostatik auf \acct*{Quadraturen} (Berechnung von Integralen $\to$ Computer) zurückgeführt.
\end{notice}

Nun stellt sich die Frage, ob damit aus Sicht der Physik alles bekannt ist. Die Antwort lautet \acct*{Nein}, denn
%
\begin{enumerate}
	\item Das Studium spezieller Verteilungen von $\varrho$ und $\bm{j}$ muss eine anschauliche Vorstellung über die elektromagnetischen Felder vermitteln.

	\item In den wenigsten Fällen der praktischen Anwendung sind $\varrho(\bm{r})$ und $\bm{j}(\bm{r})$ im ganzen $\mathbb{R}^3$ bekannt.
\end{enumerate}

Dies führt uns auf die elektro- und magnetostatischen \acct{Randwertprobleme}.

\begin{example}
	Betrachte einen Leiter $L$, wie in Abbildung~\ref{fig:2013-05-15-1} dargestellt.

	\begin{figure}[htpg]
		\centering
		\begin{pspicture}(-1,-1)(3.5,1)
			\pscircle[linecolor=MidnightBlue](0,0){1}
			\foreach \i in {120, 140, 160, 180, 200, 220, 240} {
				\rput(0.7;\i){\color{DimGray} $+$}
			}
			\foreach \i in {-60, -40, -20, 0, 20, 40, 60} {
				\rput(0.7;\i){\color{DimGray} $-$}
			}
			\rput(0,0){\color{DimGray} $L$}
			\pcarc(1;70)(1,1)
			\uput[0](1,1){\color{MidnightBlue} $\partial L$}

			\rput(2,0){
				\rput(0.8,0.3){\color{DimGray} $+$}
				\rput(1.0,0.4){\color{DimGray} $+$}
				\rput(0.9,0.6){\color{DimGray} $+$}
				\rput(0.6,0.4){\color{DimGray} $+$}
				\rput(0.9,0.3){\color{DimGray} $+$}
				\uput[-45](1,0.4){\color{DimGray} $\varrho(\bm{r})$}
			}
		\end{pspicture}
		\caption{\textbf{Links} ein Leiter $L$ mit positiven und negativen Ladungen und \textbf{rechts} eine zufällige Ladungsverteilung $\varrho(\bm{r})$.}
		\label{fig:2013-05-15-1}
	\end{figure}

	Auf $\partial L$ ist die Ladungsverteilung zunächst nicht bekannt, sondern lediglich dass $\phi|_{\partial L} = \mathrm{const}$.
	%
	\begin{align*}
		\phi(\bm{r}) = \int_{\mathbb{R}^3} \frac{\varrho(\bm{r}')}{|\bm{r} - \bm{r}'|} \mathrm{d}^3 r' = \phi(\bm{r},[\varrho(\bm{r})])
	\end{align*}
\end{example}

\section[Multipolentwicklung]{Multipolentwicklung (Statik)}

Zum besseren Verständnis ziehen wir die in Abbildung~\ref{fig:2013-05-15-2} eingezeichneten Größen hinzu.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.8,-1)(5,1.5)
		\psccurve[linecolor=MidnightBlue](0,0)(0.3,-1)(1,-0.9)(1.6,-1)(2,0)(1.8,1)(1,0.9)(0.6,1)
		\cnode*(1,-0.5){2pt}{O}
		\cnode*(4,1){2pt}{A}
		\pnode(1,0.5){B}
		\uput[-40](A){\color{DimGray} Aufpunkt}
		\ncline[arrows=->]{O}{B}\naput{\color{DimGray} $\bm{r}'$}
		\ncline[arrows=->]{O}{A}\nbput{\color{DimGray} $\bm{r}$}
		\ncline[arrows=->]{B}{A}\naput{\color{DimGray} $\bm{r} - \bm{r}'$}
		\rput(0.5,0.5){\color{MidnightBlue} $V$}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=180,
			nodesepA=-5pt
		](-0.2,1)(-0.2,-1){\color{DimGray} \clap{$\ell$}}
	\end{pspicture}
	\caption{Innerhalb des Volumens sind $\varrho, \bm{j} \neq 0$, also lokalisiert.}
	\label{fig:2013-05-15-2}
\end{figure}

Aus der Abbildung ist ersichtlich, dass
%
\begin{align*}
	\boxed{r \gg \ell \implies r \gg r'}
\end{align*}
%
Wir wählen $\bm{r}$ in kartesischen Koordinaten, sodass
%
\begin{align*}
	\bm{r} &= (x,y,z) = (x_1,x_2,x_3) \\
	|\bm{r}| &= r = (x_\alpha x_\alpha)^{1/2} \\
	\hat{\bm{r}} &= \frac{\bm{r}}{r} \\
	\hat{\bm{r}}^2 &= 1
\end{align*}

Sei $f(\bm{r} - \bm{r}')$ eine beliebige Funktion. Wir führen deren Taylorentwicklung in $\bm{r}'$ um $\bm{r}'_0 = 0$ bis zur zweiten Ordnung aus.
%
\begin{align*}
	f(\bm{r} - \bm{r}')
	&= f(\bm{r}) - x'_\alpha \partial_\alpha f(\bm{r}) + \frac{1}{2} x'_\alpha x'_\beta \partial_\alpha \partial_\beta f(\bm{r}) \\
	&= f(\bm{r}) - \bm{r}' \partial_{\bm{r}} f(\bm{r}) + \frac{1}{2} \left( \bm{r}' \partial_{\bm{r}} \right)^2 f(\bm{r}) \\
	&= f(\bm{r}) - \bm{r}' \cdot \nabla f(\bm{r}) + \frac{1}{2} \left( \bm{r}' \cdot \nabla \right)^2 f(\bm{r})
\end{align*}
%
Einen wichtigen Fall dieser Taylorentwicklung stellt die folgende Funktion dar
%
\begin{align*}
	\frac{1}{|\bm{r} - \bm{r}'|} &= \frac{1}{r} - x'_\alpha \partial_\alpha \frac{1}{r} + \frac{1}{2} x'_\alpha x'_\beta \partial_\alpha \partial_\beta \frac{1}{r} + \dotsb
\end{align*}
%
Dazu zwei kleine Nebenrechnungen
%
\begin{itemize}
	\item
		\begin{itemalign}
			\partial_\alpha \frac{1}{r}
			&= \partial_\alpha (x_\alpha x_\beta) \\
			&= - \frac{1}{2} (x_\beta x_\beta)^{-3/2} (x_\beta \partial_\alpha x_\beta + (\partial_\alpha x_\beta) x_\beta) \\
			&= - \frac{1}{2} (x_\beta x_\beta)^{-3/2} (x_\beta \delta_{\alpha\beta} + \delta_{\alpha\beta} x_\beta) \\
			&= - \frac{1}{r^2} \frac{x_\alpha}{r} \\
			\implies \nabla \frac{1}{r} &= - \frac{\bm{r}}{r^3} = - \frac{1}{r^2} \hat{\bm{r}}
		\end{itemalign}

	\item
		\begin{itemalign}
			\partial_\alpha \partial_\beta \frac{1}{r}
			&= - \partial_\alpha \left[ x_\beta (x_\gamma x_\gamma)^{-3/2} \right] \\
			&= - \left\{ \delta_{\alpha\beta} r^{-3} + x_\beta \left( - \frac{3}{2} \right) (x_\delta x_\delta)^{-5/2} \cdot 2 x_\alpha \right\} \\
			&= - \frac{\delta_{\alpha\beta}}{r^3} + 3 \frac{x_\alpha x_\beta}{r^5} \\
			&= \frac{1}{r^5} \left[ 3 x_\alpha x_\beta - r^2 \delta_{\alpha\beta} \right]
		\end{itemalign}
\end{itemize}

Damit folgt dann
%
\begin{align*}
	\frac{1}{|\bm{r} - \bm{r}'|} &= \frac{1}{r} + \frac{\bm{r} \cdot \bm{r}'}{r^3} + \frac{1}{2} \frac{1}{r^5} \left[ 3 (\bm{r} \cdot \bm{r}')^2 - r^2 {r'}^2 \right] + \dotsb
\end{align*}

Damit ergibt sich die \acct{Multipolentwicklung}:\index{Multipolentwicklung!elektrisches Feld}
%
\begin{alignat*}{3}
	\phi(\bm{r})
	&= \frac{1}{r} \int \varrho(\bm{r}') \, \mathrm{d}V' &&+ \frac{1}{r^3} \int (\bm{r} \cdot \bm{r}') \varrho(\bm{r}') \, \mathrm{d}V' &&+ \frac{1}{2} \frac{1}{r^5} \int \left[ 3 (\bm{r} \cdot \bm{r}')^2 - r^2 {r'}^2 \right] \varrho(\bm{r}') \, \mathrm{d}V' \\
	&\eqcolon \underbrace{\phi_1}_{\text{Monopol}} &&+ \underbrace{\phi_2}_{\text{Dipol}} &&+ \underbrace{\phi_3}_{\text{Quadrupol}}
\end{alignat*}

Analog\footnote{siehe Übungsaufgaben: Blatt 6, Aufgabe 2.\ (a)} erhält man die Multipolentwicklung für das magentische Feld:\index{Multipolentwicklung!magnetisches Feld}
%
\begin{alignat*}{3}
	\bm{A}(\bm{r})
	&= \frac{1}{c r} \int \bm{j}(\bm{r}') \, \mathrm{d}V' &&+ \frac{1}{c r^3} \int (\bm{r} \cdot \bm{r}') \bm{j}(\bm{r}') \, \mathrm{d}V' &&+ \frac{1}{2 c r^5} \int \left[ 3 (\bm{r} \cdot \bm{r}')^2 - r^2 {r'}^2 \right] \bm{j}(\bm{r}') \, \mathrm{d}V' \\
	&\eqcolon \underbrace{\bm{A}_1}_{\text{Monopol}} &&+ \underbrace{\bm{A}_2}_{\text{Dipol}} &&+ \underbrace{\bm{A}_3}_{\text{Quadrupol}}
\end{alignat*}

\subsection{Skalares Potential \texorpdfstring{$\phi$}{ϕ}: Elektrostatik}

\begin{enumerate}
	\item 
		\begin{itemalign}
			\boxed{
				\phi_1(\bm{r}) = \frac{q_\mathrm{tot}}{r}
				\; , \quad q_\mathrm{tot} = \int_V \varrho(\bm{r}') \, \mathrm{d}V'
			}
		\end{itemalign}
		%
		Ersatzladungsverteilung:
		%
		\begin{align*}
			\nabla^2 \phi_1
			&= - 4 \pi \varrho_1 = q_\mathrm{tot} \nabla^2 \frac{1}{r} \\
			&= q_\mathrm{tot} ( - 4 \pi \delta(\bm{r})) \\
			\implies \varrho_1(\bm{r}) &= q_\mathrm{tot} \delta(\bm{r})
		\end{align*}
		%
		Monopol: Gesamtladung im Ursprung.

	\item \fbox{
		\begin{itemalign}
			\bm{p} &\coloneq \int_V \varrho(\bm{r}') \bm{r}' \, \mathrm{d}V' \\
			\phi_2(\bm{r}) &= \frac{\bm{p} \cdot \bm{r}}{r^3} = - (\bm{p} \cdot \nabla) \frac{1}{r}
		\end{itemalign}}
		
		Dies sind elektrisches Dipolmoment und Dipolpotential. Beachte: $\bm{p}$ hängt im Allgemeinen von der Wahl des Koordinatenursprungs ab.
		%
		\begin{align*}
			\bm{r}' &= \bm{r}'' + \bm{a} \\
			\varrho(\bm{r}') &= \varrho(\bm{r}'' + \bm{a}) \eqcolon \varrho_a(\bm{r}'') \\
			\bm{p} &= \int_V \varrho_a(\bm{r}') (\bm{r}'' + \bm{a}) \, \mathrm{d}V'' = \bm{p}_a + \bm{a} q_\mathrm{tot}
		\end{align*}
		%
		$\bm{p}$ ist also unabhängig vom Ursprung genau dann, wenn gilt $q_\mathrm{tot} = 0$.
		%
		\begin{align*}
			\Aboxed{ \bm{E}_2(\bm{r}) &= - \nabla \phi_2(\bm{r}) = \frac{1}{r^3} [ 3 (\bm{p} \cdot \hat{\bm{r}}) \hat{\bm{r}} - \bm{p} ] } \\
			( \nabla \phi_2(\bm{r}) )_\alpha
			&= \partial_\alpha p_\beta x_\beta ( x_\gamma x_\gamma )^{-3/2} \\
			&= p_\beta \left[ \frac{\delta_{\alpha\beta}}{r^3} - x_\beta \frac{3}{2} ( x_\delta x_\delta )^{-5/2} 2 \delta_{\alpha\gamma} x_\gamma \right] \\
			&= p_\beta \left[ \frac{\delta_{\alpha\beta}}{r^3} - 3 x_\alpha x_\beta r^{-5} \right] \\
			&= \frac{1}{r^3} \left[ p_\alpha - 3 \frac{x_\alpha}{r} \frac{\bm{p} \cdot \bm{r}}{r} \right]
		\end{align*}

		Ein Dipol in $z$-Richtung hat die folgenden Eigenschaften:
		%
		\begin{align*}
			\bm{p} &= (0,0,p) \\
			\phi_2(\bm{r}) &= p \frac{\cos\vartheta}{r^2} \\
			r(\vartheta) &=
			\begin{dcases}
				\sqrt{\frac{p \cos\vartheta}{\phi_2}} & , 0 \leq \vartheta \leq \frac{\pi}{2} \\
				\sqrt{\frac{p (-\cos\vartheta)}{(-\phi_2)}} & , \frac{\pi}{2} \leq \vartheta \leq \pi \\
			\end{dcases}
		\end{align*}

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-0.8,-0.5)(1,2.1)
				\psline[arrows=->](0,-0.5)(0,2)
				\uput[0](0,2){\color{DimGray} $z$}
				\psline[arrows=->](0,0)(2;60)
				\psarcn[arrows=->](0,0){1}{90}{60}
				\uput{0.6}[75](0,0){\color{DimGray} $\vartheta$}
				\uput[-30](1;60){\color{DimGray} $\bm{r}$}
				\psdot(0,0)
				\pspolygon[linecolor=DarkOrange3](0.2,-0.3)(0.2,0.3)(0.3,0.3)(0,0.6)(-0.3,0.3)(-0.2,0.3)(-0.2,-0.3)
				\uput[180](-0.2,0){\color{DarkOrange3} $\bm{p}$}
			\end{pspicture}
			\caption{Ein Dipol in $z$-Richtung.}
		\end{figure}

		Ersatzladungsverteilung
		%
		\begin{align*}
			\nabla^2 \phi_2
			&= - 4 \pi \varrho_2 \\
			&= - (\bm{p} \cdot \nabla) \nabla^2 \frac{1}{r} \\
			&= 4 \pi (\bm{p} \cdot \nabla) \delta(\bm{r}) \\
			\Aboxed{
				\varrho_2 &= - (\bm{p} \cdot \nabla) \delta(\bm{r})
			}
		\intertext{mit $\bm{p} = (0,0,p)$}
			\varrho_2(\bm{r})
			&= - p \partial_z \delta(x) \delta(y) \delta(z) \\
			&= - p \delta(x) \delta(y) \lim\limits_{\varepsilon \to 0} \frac{1}{\varepsilon} \left[ \delta\left( z + \frac{\varepsilon}{2} \right) - \delta\left( z - \frac{\varepsilon}{2} \right) \right] \\
			&= \delta(x) \delta(y) \lim\limits_{\varepsilon \to 0} \left[ \frac{p}{\varepsilon} \delta\left( z - \frac{\varepsilon}{2} \right) + \frac{-p}{\varepsilon} \delta\left( z - \frac{-\varepsilon}{2} \right) \right]
		\end{align*}

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-1.5,-3.5)(2.3,3.5)
				\psline[arrows=->](0,-3.3)(0,3.3)
				\uput[0](0,3.3){\color{DimGray} $z$}
				\foreach \a/\b in {0.5/0.25 , 1/0.5 , 1.5/0.75} {
					\psellipse(0,\a)(\b,\a)
					\psellipse(0,-\a)(\b,\a)
					\psellipse[linestyle=dotted,dotsep=1pt](\b,0)(\b,\a)
					\psellipse[linestyle=dotted,dotsep=1pt](-\b,0)(\b,\a)
				}
				\psline[linecolor=DarkOrange3,linewidth=1.5\pslinewidth,arrows=->](0,-0.5)(0,0.5)
				\pnode(! 0.5 30 cos mul 0.5 add 1 30 sin mul){A}
				\rput(A){
					\psline[linecolor=MidnightBlue,arrows=->](0,0)(1;-80)
					\psdot*[linecolor=MidnightBlue](0,0)
					\uput[10](0.5;-80){\color{MidnightBlue} $\bm{E}_2$}
				}
				\uput[45](! 0.75 30 cos mul 1.5 30 sin mul 1.5 add){\color{DimGray} $\phi_2 = \mathrm{const}$}
				\uput[-45](! 0.75 -30 cos mul 1.5 -30 sin mul 1.5 sub){\color{DimGray} $\phi_2 = \mathrm{const}$}
			\end{pspicture}
			\caption{Fernfeldnäherung eines Dipols in $z$-Richtung.}
		\end{figure}

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-1.5,-1.5)(2,2)
				\pstThreeDCoor[
					coorType=2,
					linecolor=DimGray,
					xMax=1.5,yMax=1.5,zMax=1.5,
					xMin=-0.5,yMin=-0.5,zMin=-1.5,
					nameX={\color{DimGray} $x$},nameY={\color{DimGray} $y$},nameZ={\color{DimGray} $z$}
				]
				\pstThreeDNode(0,0,0){O}
				\pstThreeDNode(0,0,1){U}
				\pstThreeDNode(0,0,-1){D}
				\psbrace[
					fillcolor=DimGray,
					braceWidth=0.5pt,
					braceWidthInner=4pt,
					braceWidthOuter=2pt,
					ref=lC,
					rot=0,
					nodesepA=5pt
				](O)(U){\color{DimGray} \clap{$\frac{\varepsilon}{2}$}}
				\psbrace[
					fillcolor=DimGray,
					braceWidth=0.5pt,
					braceWidthInner=4pt,
					braceWidthOuter=2pt,
					ref=lC,
					rot=0,
					nodesepA=5pt
				](D)(O){\color{DimGray} \clap{$\frac{\varepsilon}{2}$}}
				\psdots*[linecolor=MidnightBlue](U)(D)
				\uput[45](U){\color{MidnightBlue} $\frac{p}{\varepsilon}$}
				\uput[-45](D){\color{MidnightBlue} $\frac{-p}{\varepsilon}$}
			\end{pspicture}
			\caption{Die Ersatzladungsdichte eines Dipols in $z$-Richtung.}
		\end{figure}

	\item
		\begin{itemalign}
			\phi_3(\bm{r})
			&= \frac{1}{2 r^5} \int \mathrm{d}V' \, \varrho(\bm{r}') \left[ 3 (\bm{r} \cdot \bm{r}')^2  - r^2 {r'}^2 \right] \\
			&= \frac{1}{6 r^5} \int \mathrm{d}V' \, \varrho(\bm{r}') \left[ 9 (\bm{r} \cdot \bm{r}')^2 - 3 {r'}^2 r^2 - 3 r^2 {r'}^2 + 3 r^2 {r'}^2 \right] \\
			&= \frac{1}{6 r^5} \int \mathrm{d}V' \, \varrho(\bm{r}') \left[ 9 x_\alpha x'_\alpha x_\beta x'_\beta - {r'}^2 3 x_\alpha x_\alpha - 3 r^2 x'_\alpha x'_\alpha + r^2 {r'}^2 \delta_{\alpha\alpha} \right] \\
			&= \frac{1}{6 r^5} \int \mathrm{d}V' \, \varrho(\bm{r}') \left[ 3 x_\alpha x_\beta - r^2 \delta_{\alpha\beta} ] [ 3 x'_\alpha x'_\beta - {r'}^2 \delta_{\delta\beta} \right]
		\end{itemalign}
		%
		Das elektrische Quadrupolpotential und der Tensor des Quadrupolmoments lauten also
		%
		\begin{equation*}
			\boxed{
				\begin{aligned}
					\phi_3(\bm{r})
					&= \frac{1}{6} Q_{\alpha\beta} \frac{3 x_\alpha x_\beta - r^2 \delta_{\alpha\beta}}{r^5} \\
					&= \frac{1}{6} Q_{\alpha\beta} \partial_\alpha \partial_\beta \frac{1}{r} \\
					&= \frac{1}{2} Q_{\alpha\beta} \frac{x_\alpha x_\beta}{r^5} \; , \quad \text{da $Q_{\alpha\alpha} = \tr Q = 0$} \\
					Q_{\alpha\beta}
					&\coloneq \int \mathrm{d}V' \, \varrho(\bm{r}') [ 3 x'_\alpha x'_\beta - {r'}^2 \delta_{\alpha\beta} ]
				\end{aligned}
			}
		\end{equation*}
\end{enumerate}
