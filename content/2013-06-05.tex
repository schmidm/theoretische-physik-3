% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 05.06.2013
% TeX: Henri

\renewcommand{\printfile}{2013-06-05}

\subsection{Greensche Funktion}

Es sind gegeben
%
\begin{enumerate}
	\item Eine Ladungsverteilung $\varrho$ in $V$
	
	\item Die Randbedingung auf $\partial V$ (außen und innen)
\end{enumerate}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,-0.5)(3.5,1.5)
		\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
		\psellipse[linecolor=DarkOrange3](2,0.5)(0.5,0.25)
		\psline[linecolor=DarkOrange3,arrows=->](2,0.75)(2,1.5)
		\uput[0](2,1.5){\color{DarkOrange3} $\bm{n}$}
		\rput(2,0.5){\color{DarkOrange3} $L$}
		\pcarc(1.5,0.5)(1,0.5)
		\uput[180](1,0.5){\color{DarkOrange3} $\partial L$}
		\rput(1,1.2){\color{MidnightBlue} $V$}
		\pcarc(1,0)(0.5,-0.3)
		\uput{0.1}[180](0.5,-0.3){\color{MidnightBlue} $\partial V$}
		\pscircle[fillstyle=hlines,hatchsep=2pt](1.9,-0.2){0.25}
		\pcarc(2.15,-0.2)(2.8,-0.3)
		\uput[0](2.8,-0.3){\color{DimGray} $\varrho(\bm{r})$}
	\end{pspicture}
	\caption{In das Volumen $V$ eingebetteter Leiter $L$.}
\end{figure}

Betrachte die Funktion $G(\bm{r},\bm{r}')$ mit
%
\begin{align*}
	\nabla^2 G(\bm{r},\bm{r}') &= - 4 \pi \delta(\bm{r} - \bm{r}')
\end{align*}
%
Lösung:
%
\begin{align*}
	G(\bm{r},\bm{r}') &= \frac{1}{|\bm{r} - \bm{r}'|} + F(\bm{r},\bm{r}')
\end{align*}
%
mit $\nabla^2 F(\bm{r},\bm{r}') = 0$ für alle $\bm{r},\bm{r}' \in \partial V \cup V$.

\begin{notice}
	\begin{enumerate}
		\item $L$ gehört nicht zu $V$.

		\item $F$ wird zur Befriedigung der Randbedingung benötigt.
	\end{enumerate}
\end{notice}

\begin{theorem}[Greenscher Satz]\index{Greenscher Satz}

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(0,-0.5)(3,1.5)
			\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
			\psellipse[linecolor=MidnightBlue,fillstyle=hlines](2,0.5)(0.5,0.25)
			\psline[linecolor=DarkOrange3,arrows=->](2,0.25)(2,-0.2)
			\uput[180](2,-0.2){\color{DarkOrange3} $\bm{n}'$}
			\psline[linecolor=DarkOrange3,arrows=->](1,0)(1,0.5)
			\uput[180](1,0.5){\color{DarkOrange3} $\bm{n}'$}
			\pcarc[arcangle=-20](1.5,0.5)(1.4,0.8)
			\uput{0.1}[180](1.4,0.8){\color{MidnightBlue} $\partial V$}
			\rput(1,1.2){\color{MidnightBlue} $V$}
			\pcarc(1,0)(0.5,-0.3)
			\uput{0.1}[180](0.5,-0.3){\color{MidnightBlue} $\partial V$}
		\end{pspicture}
		\caption{Illustration zum Greenschen Satz.}
		\label{fig:2013-06-05-1}
	\end{figure}

	Seien $\phi(\bm{r}), \psi(\bm{r})$ skalare Felder in $V$ und sei $\bm{n}'$ nach innen orientiert (vgl.\ Abb.\ \ref{fig:2013-06-05-1}).
	%
	\begin{equation*}
		\boxed{
			\begin{aligned}
				& \int_V \left[ \phi(\bm{r}') {\nabla'}^2 \psi(\bm{r}') - \psi(\bm{r}') {\nabla'}^2 \phi(\bm{r}') \right] \, \mathrm{d}V' \\
				={}& - \int_{\partial V} \left[ \phi(\bm{r}') {\nabla'} \psi(\bm{r}') - \psi(\bm{r}') {\nabla'} \phi(\bm{r}') \right] \cdot \bm{n}' \, \mathrm{d}f'
			\end{aligned}
		}
	\end{equation*}

	\begin{proof}
		Gaußscher Satz:
		%
		\begin{align*}
			\int_V \nabla' \cdot \bm{A}(\bm{r}') \, \mathrm{d}V' = - \int_{\partial V} \bm{A}(\bm{r}') \cdot \bm{n}' \, \mathrm{d}f'
		\end{align*}
		%
		Sei nun
		%
		\begin{align*}
			\bm{A} &\coloneq \phi \nabla' \psi \\
			\implies \nabla' \cdot \bm{A} &= (\nabla' \phi) \cdot (\nabla' \psi) + \phi {\nabla'}^2 \psi
		\end{align*}
		%
		Dann folgt
		%
		\begin{align}
			\int_V \left[ (\nabla' \phi) \cdot (\nabla' \psi) + \phi {\nabla'}^2 \psi \right] \, \mathrm{d}V'
			&= \int_{\partial V} \phi \nabla' \psi \cdot \bm{n}' \, \mathrm{d}f' \label{eq:2013-06-05-1} \\
			- \int_V \left[ (\nabla' \phi) \cdot (\nabla' \psi) + \phi {\nabla'}^2 \psi \right] \, \mathrm{d}V'
			&= \int_{\partial V} \psi \nabla' \phi \cdot \bm{n}' \, \mathrm{d}f' \label{eq:2013-06-05-2}
		\end{align}
		%
		Addieren wir die Gleichungen \eqref{eq:2013-06-05-1} und \eqref{eq:2013-06-05-2}, so erhalten wir den Greenschen Satz.
	\end{proof}
\end{theorem}

Wähle nun speziell:
%
\begin{itemize}
	\item $\phi$ soll $\nabla^2 \phi = - 4 \pi \varrho$ liefern.

	\item $\psi(\bm{r}')$ soll $\nabla^2 \psi(\bm{r}') = - 4 \pi \delta(\bm{r} - \bm{r}')$ liefern, d.h.\ $\psi(\bm{r}') = G(\bm{r},\bm{r}')$.
\end{itemize}
%
Einsetzen in den Greenschen Satz liefert:
%
\begin{align*}
	& \int_V \left[ \phi(\bm{r}') \left(- 4 \pi \delta(\bm{r}' - \bm{r}) - G(\bm{r},\bm{r}') ( - 4 \pi \varrho(\bm{r}') ) \right) \right] \, \mathrm{d}V' \\
	={}& - \int_{\partial V} \left[ \phi(\bm{r}') \nabla' G(\bm{r},\bm{r}') - G(\bm{r},\bm{r}') \nabla' \phi(\bm{r}') \right] \cdot \bm{n}' \, \mathrm{d}f' \\
	\Aboxed{
		\phi(\bm{r}') &= \int_V \varrho(\bm{r}') G(\bm{r},\bm{r}') \, \mathrm{d}V' + \frac{1}{4 \pi} \int_{\partial V} \left[ \phi(\bm{r}') \partial_{\bm{n}'} G(\bm{r}',\bm{r}) - G(\bm{r}',\bm{r}) \partial_{\bm{n}'} \phi(\bm{r}') \right] \, \mathrm{d}f'
	}
\end{align*}
%
Betrachten wir den letzten Ausdruck genauer:
%
\begin{align}
	\phi(\bm{r}') &=
	\underbrace{\int_V \varrho(\bm{r}') G(\bm{r},\bm{r}') \, \mathrm{d}V'}_{\hateq \text{Raumladung}}
	+ \frac{1}{4 \pi} \int_{\partial V} [
		\underbrace{\vphantom{\int_V} \phi(\bm{r}') \partial_{\bm{n}'} G(\bm{r}',\bm{r})}_{\hateq \text{Dipolschicht}}
		- \underbrace{\vphantom{\int_V} G(\bm{r}',\bm{r}) \partial_{\bm{n}'} \phi(\bm{r}')}_{\hateq \text{Oberflächenladung}}
	] \, \mathrm{d}f'
	\label{eq:2013-06-05-stern1}
\end{align}
%
Oberflächenladung: $\bm{n}'$ weist vom Leiter nach außen in $V$ hinein.
%
\begin{align*}
	- \partial_{\bm{n}'} \phi|_{\partial V} &= - 4 \pi \sigma \\
	\partial_{\bm{n}'} &= \bm{n}' \cdot \nabla'
\end{align*}

\paragraph{Spezialfall:} $\partial V \to \infty$ und $F \equiv 0$
%
\begin{align*}
	G(\bm{r},\bm{r}') &= \frac{1}{|\bm{r} - \bm{r}'|} \text{ und } \phi(\bm{r}) = \int_{\mathbb{R}^3} \frac{\varrho(\bm{r}')}{|\bm{r} - \bm{r}'|} \, \mathrm{d}V'
\end{align*}
%
falls $\bm{E}(r \to \infty) \to 0$ schneller als $1/r$.

Gleichung \eqref{eq:2013-06-05-stern1} gilt auch für ein unendliches $V$ mit $G(\bm{r}',\bm{r}) = \frac{1}{|\bm{r} - \bm{r}'|}$.
%
\begin{align*}
	\phi(\bm{r}) = \int_V \frac{\varrho(\bm{r}')}{|\bm{r} - \bm{r}'|} \, \mathrm{d}V'
	\begin{aligned}[t]
		&+ \int_{\partial V}
		\underbrace{\left[ \frac{\bm{n}'}{4 \pi} \phi(\bm{r}') \right]}_{\eqcolon \bm{\Pi}(\bm{r}')}
		\underbrace{\nabla' \frac{1}{|\bm{r} - \bm{r}'|}}_{- \nabla \frac{1}{|\bm{r} - \bm{r}'|}}
		\, \mathrm{d}f \\
		&+
		\int_{\partial V}
		\biggl[ \frac{1}{4 \pi} \bm{n}' \cdot
			\underbrace{\left( - \nabla' \phi(\bm{r}') \right)}_{\bm{E}(\bm{r}')}
		\biggr]
		\frac{1}{|\bm{r} - \bm{r}'|}
		\, \mathrm{d}f
	\end{aligned}
\end{align*}
%
\begin{description}
	\item[$\varrho(\bm{r}')$:] Raumladungsdichte

	\item[$\frac{1}{4 \pi} \bm{n}'(\bm{r}') \bm{E}(\bm{r}') = \sigma(\bm{r}')$:] Oberflächenladungsdichte

	\item[$\frac{\phi(\bm{r}')}{4 \pi} \bm{n}'(\bm{r}') = \bm{\Pi}(\bm{r}')$:] Oberflächendipoldichte

		Dipolpotential am Ort $\bm{r}$, wenn am Ort $\bm{r}'$ ein Dipolmoment $\bm{p}(\bm{r}')$ vorhanden ist:
		\[ \phi_\mathrm{Dipol} = - (\bm{p}(\bm{r}) \cdot \nabla_\bm{r}) \frac{1}{|\bm{r} - \bm{r}'|} \]
		hier $\bm{p}(\bm{r}') = \bm{\Pi}(\bm{r}')$.
\end{description}

Speziell $\varrho(\bm{r}') = 0$ in $V$
%
\begin{align*}
	\phi(\bm{r} \in V) &= \int_{\partial V} \left[ - \frac{\bm{n}'(\bm{r}')}{4 \pi} \phi(\bm{r}') \right] \nabla \frac{1}{|\bm{r} - \bm{r}'|} \, \mathrm{d}f'
	+ \int_{\partial V} \frac{\frac{\bm{n}'}{4 \pi} (-\nabla' \phi(\bm{r}'))}{|\bm{r} - \bm{r}'|} \, \mathrm{d}f'
\end{align*}
%
Dies ist eine Integralgleichung, die $\phi(\bm{r} \in V)$, sowie $\phi(\bm{r} \in \partial V)$ und $(\nabla \phi)(\bm{r} \in \partial V)$ verknüpft. Dies ist keine Lösung der Randwertaufgabe, da die gleichzeitige Vorgaben von $\phi$ und $\nabla \phi$ auf einer geschlossenen Oberfläche ($\hateq$ Cauchy-Randbedingung) eine Überspezifikation der Poisson-Differentialgleichung, eine elliptische Differentialgleichung, darstellt.

(Die Cauchy-Randbedingungen führen zu einer eindeutigen, stabilen Lösung der Wellengleichung, einer hyperbolischen Differentialgleichung)

Für geschlossenen Oberflächen lässt eine elliptische Differentialgleichung (Poisson-Dif\-fer\-en\-tial\-glei\-chung) zwei, sich ausschließende Randbedingungen zu:
%
\begin{enumerate}
	\item Dirichlet-Randbedingung: $\phi(\bm{r} \in \partial V)$ vorgegeben.

	\item Neumann-Randbedingung: $(\bm{n} \cdot \nabla \phi)(\bm{r} \in \partial V)$ vorgegeben.
\end{enumerate}
%
Beide führen zu eindeutigen und stabilen Lösungen. Stabil heißt hier, dass kleine Änderungen der Randbedingung zu kleinen Änderungen der Lösung führen.

Die Freiheit der Wahl von $F(\bm{r},\bm{r}')$ für $G$ wird benutzt, um in \eqref{eq:2013-06-05-stern1} den bekannten Anteil im Oberflächenintegral zu eliminieren und damit \eqref{eq:2013-06-05-stern1} tatsächlich zu einer Bestimmungsgleichung für $\phi(\bm{r})$ zu machen.

\paragraph{Dirichlet:} Hierbei sind $\phi(\bm{r}' \in \partial V)$ und $F$ so vorgegeben, dass
%
\begin{align*}
	\boxed{
		\begin{aligned}
			G_\mathrm{D}(\bm{r},\bm{r}') &= 0 \; , \quad \text{für alle $\bm{r}' \in \partial V$} \\
			\phi(\bm{r}) &= \int_V \mathrm{d}V' \, \varrho(\bm{r}') G_\mathrm{D}(\bm{r},\bm{r}') + \frac{1}{4 \pi} \int_{\partial V} \phi(\bm{r}') \partial_{\bm{n}'} G_\mathrm{D}(\bm{r},\bm{r}') \, \mathrm{d}f'
		\end{aligned}
	}
\end{align*}
%
wobei $\varrho(\bm{r}')$ bekannt, $\phi(\bm{r}')$ vorgegeben und $\bm{n}'$ ins Innere von $V$ zeigt.

\paragraph{Neumann:} Naheliegend wäre $\partial_{\bm{n}'} G_\mathrm{N}(\bm{r},\bm{r}') = 0$. Dies ist aber nicht zulässig, da
%
\begin{align*}
	\underbrace{\int_V \underbrace{\nabla' \cdot \nabla' G_\mathrm{N}(\bm{r},\bm{r}')}_{= - 4 \pi \delta(\bm{r} - \bm{r}')} \, \mathrm{d}V'}_{= - 4 \pi}
	&= - \int_{\partial V} (\bm{n}' \cdot \nabla') G_\mathrm{N}(\bm{r},\bm{r}') \, \mathrm{d}f' \\
	\implies&{} \int_{\partial V} (\bm{n}' \cdot \nabla') G_\mathrm{N}(\bm{r},\bm{r}') \, \mathrm{d}f' = 4 \pi
\end{align*}
%
Die einfachste mögliche Wahl von $G_\mathrm{N}(\bm{r},\bm{r}')$ ist somit:
%
\begin{align*}
	\boxed{
		\begin{aligned}
			(\bm{n}' \cdot \nabla) G_\mathrm{N}(\bm{r},\bm{r}') &= \frac{4 \pi}{|\partial V|} \; , \quad \text{für $\bm{r}' \in \partial V$} \\
			\phi(\bm{r}) &=
			\begin{aligned}[t]
				&\int_V \mathrm{d}V' \, \varrho(\bm{r}') G_\mathrm{N}(\bm{r},\bm{r}')
				+ \frac{1}{|\partial V|} \int_{\partial V} \phi(\bm{r}') \, \mathrm{d}f' \\
				&- \frac{1}{4 \pi} \int_{\partial V} G_\mathrm{N}(\bm{r},\bm{r}') (\bm{n}' \cdot \nabla') \phi(\bm{r}') \, \mathrm{d}f'
			\end{aligned}
		\end{aligned}
	}
\end{align*}
%
wobei $\varrho(\bm{r}')$ bekannt, $(\bm{n}' \cdot \nabla') \phi(\bm{r}')$ vorgegeben und $\bm{n}'$ ins Innere von $V$ zeigt. Außerdem ist $\langle \phi \rangle_{\partial V}$ eine Konstante bezüglich $\bm{r}$ im Potential mit
%
\begin{align*}
	\langle \phi \rangle_{\partial V} = \frac{1}{|\partial V|} \int_{\partial V} \phi(\bm{r}') \, \mathrm{d}f'
\end{align*}

\paragraph{Spezialfall: Äußeres Neumann-Problem}
%
\begin{align*}
	|\partial V| &= |\partial V_i| + |\partial V_a| \to \infty \; , \quad \text{für $\partial V_a \to \infty$}
\end{align*}
%
dann wird $\langle \phi \rangle_{\partial V} = 0$.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-1,-0.5)(3.5,2.3)
		\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
		\psellipse[linecolor=MidnightBlue,fillstyle=hlines](2,0.5)(0.5,0.25)
		\psline[linecolor=DarkOrange3,arrows=->](2,0.25)(2,-0.2)
		\uput[180](2,-0.2){\color{DarkOrange3} $\bm{n}'$}
		\psline[linecolor=DarkOrange3,arrows=->](1,0)(1,0.5)
		\uput[180](1,0.5){\color{DarkOrange3} $\bm{n}'$}
		\pcarc[arcangle=-20](1.5,0.5)(1.4,0.8)
		\uput{0.1}[180](1.4,0.8){\color{MidnightBlue} $\partial V_i$}
		\rput(1,1.2){\color{MidnightBlue} $V$}
		\pcarc(1,0)(0.5,-0.3)
		\uput{0.1}[180](0.5,-0.3){\color{MidnightBlue} $\partial V_a$}
		\rput(0,0){\psline[arrows=->](0,0)(0.5;200)\uput{0.1}[200](0.5;200){\color{DimGray} $\infty$}}
		\rput(3,1){\psline[arrows=->](0,0)(0.5;60)\uput{0.1}[60](0.5;60){\color{DimGray} $\infty$}}
		\rput(1,1.5){\psline[arrows=->](0,0)(0.5;100)\uput{0.1}[100](0.5;100){\color{DimGray} $\infty$}}
	\end{pspicture}
	\caption{Zum äußeren Neumann-Problem}
\end{figure}

\begin{notice}
	Zur physikalischen Bedeutung von $F(\bm{r},\bm{r}')$.
	\[ \nabla^2 F = 0 \text{ in } V \text{ und } G(\bm{r},\bm{r}') = \frac{1}{|\bm{r} - \bm{r}'|} + F(\bm{r},\bm{r}') \]
	$F(\bm{r},\bm{r}')$ ist das Potential am Ort $\bm{r}$ einer außerhalb von $V$ liegenden Ladungsverteilung, die so gewählt ist, dass deren Potential zusammen mit einer Punktladung der Ladung $1$ am Ort $\bm{r}'$ auf die Oberfläche $\partial V$ die auferlegte Randbedingung sicherstellt. (Prinzip der Spiegelladung)
\end{notice}

\begin{example}
	für eine Dirichlet-Greensche Funktion im Halbraum $V^+$.

	$G_\mathrm{D}(\bm{r},\bm{r}')$: Potential am Ort $\bm{r}$ herrührend von einer Punktladung $q = 1$ am Ort $\bm{r}' \in V^+$ unter der Bedingung, dass dieses Potential am Rand $\partial V^+$ null ist.
	\[ G_\mathrm{D}(\bm{r}',\bm{r}) = 0 \text{ für } x = 0 \]

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-1.5,-1.5)(3.2,1.5)
			\psframe[linestyle=none,fillstyle=hlines](-0.3,-1.3)(0,1.3)
			\psaxes[labels=none,ticks=none,arrows=->](0,0)(0,-1.5)(3,1.5)[{\color{DimGray} $x$},-90][,0]
			\pnode(0,0){O}
			\cnode*(1.5;-45){2pt}{q1}
			\cnode*[linecolor=DarkOrange3](1.5;-135){2pt}{q2}
			\pcline[arrows=->](0,0)(2;30)\naput{\color{DimGray} $\bm{r}$}
			\ncline[arrows=->]{O}{q1}\naput{\color{DimGray} $\bm{r}'$}
			\ncline[linecolor=DarkOrange3,arrows=->]{q1}{q2}
			\uput[0](2;30){\color{DimGray} Aufpunkt}
			\uput[0](q1){\color{DimGray} $q$}
			\uput[180](q2){\color{DarkOrange3} $-q$}
			\cput(-1,1){\color{DimGray} $L$}
		\end{pspicture}
		\caption{Lösung der Dirichlet-Greenschen Funktion.}
	\end{figure}

	\begin{align*}
		G_\mathrm{D}(\bm{r},\bm{r}') &=
		\frac{1}{|\bm{r} - \bm{r}'|} + \underbrace{\frac{-1}{|\bm{r} - (\bm{r}' - 2 \hat{\bm{x}} (\bm{r}' \cdot \hat{\bm{x}}))|}}_{F_\mathrm{D}(\bm{r}',\bm{r})}
	\\
		|\bm{r} - (\bm{r}' - 2 \hat{\bm{x}} (\bm{r}' \cdot \hat{\bm{x}}))|^2
		&= |\bm{r} - \bm{r}'|^2 + 4 ( (\bm{r} - \bm{r}') \cdot \hat{\bm{x}} ) (\bm{r}' \cdot \hat{\bm{x}}) + 4 (\bm{r}' \cdot \hat{\bm{x}})^2 \\
		&= |\bm{r} - \bm{r}'|^2 + 4 (\bm{r}' \cdot \hat{\bm{x}}) (\bm{r}' \cdot \hat{\bm{x}}) + 4 (\bm{r}' \cdot \hat{\bm{x}}) (\bm{r}' \cdot \hat{\bm{x}}) \\
		&= |\bm{r} - \bm{r}'|^2 + 4 (\bm{r}' \cdot \hat{\bm{x}})
	\\
		F_\mathrm{D}(\bm{r},\bm{r}')
		&= F_\mathrm{D}(\bm{r}',\bm{r}) \\
		&\neq F_\mathrm{D}(\bm{r} - \bm{r}')
	\\
		G_\mathrm{D}(\bm{r},\bm{r}')
		&= G_\mathrm{D}(\bm{r}',\bm{r})
	\\
		G_\mathrm{D}(\bm{r},\bm{r}')|_{x = 0} &= 0
	\\
		G_\mathrm{D}(\bm{r},\bm{r}')|_{x' = 0} &= 0
	\\
		\nabla^2 F_\mathrm{D}(\bm{r},\bm{r}') &= 0 \; , \quad \text{für $x \geq 0$}
	\end{align*}
\end{example}

\begin{notice}
	Greensche Funktionen sind sehr nützlich, da sie bei einer vorgegebenen Geometrie und Randbedingung für beliebige $\varrho(\bm{r}')$ anwendbar sind. Die Lösung beschränkt sich damit nur noch auf ein Integrationsproblem.
\end{notice}
