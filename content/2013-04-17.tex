% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 17.04.2013
% TeX: Michi

\renewcommand{\printfile}{2013-04-17}

\subsection{Elektrischer Strom}

Elektrischer Strom ist Ladungstransport. In diesem Zusammenhang ist der Begriff der \acct{Stromdichte $\bm{j}$} wichtig. Diese besitzt die folgenden Eigenschaften:
%
\begin{itemize}
	\item Stromrichtung: $\hat{\bm{j}}$ mit $\hat{\bm{j}}^2 = 1$
	\item $\bm{j} = \dfrac{\text{Ladung}}{\text{Zeit $\times$ Fläche}}$
\end{itemize}

\begin{figure}
	\centering
	\begin{pspicture}(-3,-3)(2,1)
		\pstThreeDLine(0,0,0)(4,0,0)(4,3,0)(0,3,0)(0,0,0)
		\pstThreeDCircle[
			linecolor=MidnightBlue,
			fillstyle=hlines,
			hatchcolor=MidnightBlue
		](2,1.5,0)(0.5,0,0)(0,0.5,0)
		\pstThreeDLine[arrows=->](2,1.5,0)(2,1.5,2)
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](2,1.5,0)(2,2.5,2)
		\pstThreeDNode(2,1.5,1){A}
		\pstThreeDNode(2,2,1){B}
		\pstThreeDNode(2,1.5,2){n}
		\pstThreeDNode(4,3,0){F}
		\pstThreeDNode(2,2.5,0){j}
		\pstThreeDNode(1.5,1.5,0){df}
		\ncarc[linecolor=Purple,arrows=->]{A}{B}
		\uput[180](n){\color{DimGray} $\bm{n}$}
		\uput[90](F){\color{DimGray} $F$}
		\uput[45](V){\color{DarkOrange3} $\bm{j}$}
		\uput[0](df){\color{MidnightBlue} $\mathrm{d}f$}
		\uput[90]([nodesep=0.1]{A}B){\color{Purple} $\vartheta$}
	\end{pspicture}
	\caption{Strom $\bm{j}$ durch eine Fläche $F$.}
\end{figure}

Der Strom durch eine Fläche $F$ ist gleich der Quotient von Ladung durch $F$ und der Zeit:
\[
	\text{Fläche $F$} = \frac{\text{Ladung durch $F$}}{\text{Zeit}}.
\]
\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-2,-1.0)(4.25,1.0)
		\psline(0,0.5)(4,0.5)
		\psline(0,-0.5)(4,-0.5)
		\psellipticarc(0,0)(0.25,0.5){90}{-90}
		\psellipse[fillstyle=hlines](4,0)(0.25,0.5)
		\psellipse[linestyle=dotted,dotsep=1pt](3,0)(0.25,0.5)
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=2pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=90,
			nodesepB=5pt
		](3,-0.5)(4,-0.5){\color{DimGray} \clap{$v \cdot \Delta t$}}
		\psline[linecolor=MidnightBlue,arrows=->](0.7,0)(1.5,0)
		\uput[90](1.5,0){\color{MidnightBlue} $\bm{v}$}
		\pcarc(0.8,-0.3)(0.5,-0.8)
		\uput[180](0.5,-0.8){\color{DarkOrange3} Ladungsträger}
		\pcarc(1.5,0.5)(2.0,0.8)
		\uput[0](2.0,0.8){\color{DimGray} Leiter}
		\psdots*[linecolor=DarkOrange3](0.5,-0.1)(1.5,-0.2)(1.1,-0.1)(0.9,-0.2)(0.8,-0.3)(2.2,-0.0)(2.7,-0.2)(1.8,-0.1)(0.6,0.1)(1.1,-0.1)(2.0,0.1)(3.0,-0.1)(0.7,-0.0)(2.7,0.0)(3.3,-0.2)(0.4,-0.2)(0.2,-0.1)(0.0,-0.1)(3.7,-0.1)(0.8,0.2)
	\end{pspicture}
	\caption{Ladungen fließen durch einen Leiter mit der Geschwindigkeit $v$.}
	\label{fig:2013-04-17-0}
\end{figure}

Der Gesamtstrom $I$, der durch ein endliches Flächenstück $F$ hindurchtritt, ist wie folgt definiert:
\[
	I = \int_{F} \bm{j} \cdot \bm{n} \; \mathrm{d}f.
\]
$\nu$ bezeichnet im Folgenden die Teilchenzahldichte geladener Teilchen und $q$ die Ladung des Teilchen. Betrachtet man nun die Ladung in einem Zeitabschnitt $\Delta t$ durch die Querschnittfläche $F$ des in Abbildung~\ref{fig:2013-04-17-0} skizzierten Leiters, gilt:
\[
	\underbrace{\nu q}_{\varrho} \cdot v \cdot \Delta t \cdot F = j \cdot  \Delta t  \cdot F \qquad \text{mit} \quad \hat{\bm{j}} = \frac{\bm{v}}{|\bm{v}|} \quad \text{und} \quad \bm{j} = \varrho \bm{v}.
\]
Allgemein gilt jedoch:
\[
	\bm{j}\left(\bm{r},t\right) = \varrho\left(\bm{r},t\right) \bm{v}\left(\bm{r},t\right).
\]
Dies beschreibt die Dichte des \acct*{Konvektionsstromes}\index{Konvektionsstrom}.

\begin{theorem}[Erfahrung]
Ladung kann weder entstehen noch verschwinden.
\end{theorem}

Zu diesem Zweck betrachten wir ein im Raum festes Volumen $V$ durch dessen Oberfläche $\partial V$ sich Ladungen nach $V$ hinein- oder aus $V$ herausbewegen können (siehe Abbildung~\ref{fig:2013-04-17.21}).

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,-0.5)(3,1.5)
		\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
		\psellipse[linecolor=DarkOrange3,fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](2,0.5)(0.25,0.125)
		\psline[linecolor=DarkOrange3,arrows=->](2,0.5)(2,1.5)
		\cnode*(0.5,0.5){2pt}{A}
		\cnode*(2.5,0.3){2pt}{B}
		\pnode(0,1){in}
		\pnode(3,0){out}
		\ncarc[arrows=->]{in}{A}
		\ncarc[arrows=->]{B}{out}
		\rput(0.8,0.3){
			\psdot(0,0)
			\pscoil[coilarm=0.2,coilwidth=0.2,arrows=->](0,0)(1;-10)
		}
		\psdots*(1,0.8)(1.3,0.6)(1.5,0.5)(2.1,-0.3)
		\rput(1,1.2){\color{MidnightBlue} $V$}
		\pcarc(1,0)(0.5,-0.3)
		\uput{0.1}[180](0.5,-0.3){\color{MidnightBlue} $\partial V$}
	\end{pspicture}
	\caption{Ein Volumen $V$ mit orientiertem Rand $\partial V$. Der Rand von $V$ ist dabei raumfest.}
\label{fig:2013-04-17.21}
\end{figure}

Für die Gesamtladung in $V$ gilt:
\[
	Q_V = \int_{V} \varrho(\bm{r},t) \; \mathrm{d}V.
\]
Hierbei gilt das Gesetz der Ladungserhaltung, d.h. $Q_V(t)$ ändert sich zeitlich nur durch Ein- und Ausströmen von Ladungen. 
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} Q_V(t) &= - \int_{\partial V} \bm{j}(\bm{r},t) \bm{n} \; \mathrm{d}f \\
	&= \int_{V} \partial_t \varrho(\bm{r},t) \; \mathrm{d}^3r
\end{align*}
% 
Das Minuszeichen rührt daher, dass das Volumen durch die Oberfläche Ladung verliert (also Minus) oder hinzukommt (dann Plus), also:
\[
	\dot{Q}_V < 0 \qquad \text{wenn} \qquad \bm{j}\cdot\bm{n} > 0.
\] 
Somit erhalten wir die \acct*{Kontinuitätsgleichung in integraler Form}\index{Kontinuitätsgleichung!integrale Form}:
\[
	\boxed{\frac{\mathrm{d}}{\mathrm{d}t} Q_V(t)	= \int_{V} \partial_t \varrho(\bm{r},t) \; \mathrm{d}^3r}
\]

\begin{theorem}[Satz der Ladungserhaltung]
	Der durch eine geschlossene Fläche hindurchtretende Gesamtstrom ist gleich dem zeitlichen Ladungsverlust des von der Fläche umschlossenen Gebiets. 
\end{theorem}

\begin{notice}[Paarerzeugung:]
	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(0,-1)(3,1)
			\psframe[fillstyle=hlines](0,-1)(2,1)
			\psframe[fillstyle=solid,fillcolor=white](0.2,-0.8)(1.8,0.8)
			\pcsin[linecolor=MidnightBlue,coilarm=0,amplitude=0.2,periods=5](3,0)(1.3,0)
			\uput{0.3}[90](2.5,0){\color{MidnightBlue} $\gamma$}
			\rput(1.3,0){
				\psline[linecolor=DarkOrange3,arrows=->](0,0)(0.5;135)
				\psline[linecolor=DarkOrange3,arrows=->](0,0)(0.5;225)
				\uput{0.1}[180](0.5;135){\color{DarkOrange3} $e^+$}
				\uput{0.1}[180](0.5;225){\color{DarkOrange3} $e^-$}
			}
		\end{pspicture}
		\caption{Paarerzeugung eines Elektron-Positron-Paares aus einem $\gamma$-Quant ausreichender Energie}
	\end{figure}
	\[
		Q_{\text{vorher}} = 0 = Q_{\text{nacher}} = e + (-e)  
	\]
	$e$ ist hierbei die Elementarladung. Ein Elektron hat die Elementarladung $-e$.
\end{notice}

\paragraph{Punktladungen:}
Für Punktladungen gilt für die Raumladungsdichte $\varrho(\bm{r},t)$ eines festen Volumens $V$:
\[
	\varrho(\bm{r},t) = \sum_{i=1}^{N} q_i \delta\left(\bm{r} - \bm{r}_i(t)\right).
\]

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,-1)(3,1.5)
		\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
		\psdots*[linecolor=MidnightBlue](0.4,-0.5)(0.3,1.4)(1.7,-0.2)(1.5,0.2)(0.9,0.7)(0.2,0.1)(1.1,1.4)(0.3,1.1)(1.5,1.2)(1.3,0.5)(0.4,0.5)(0.2,0.7)(1.7,0.9)(0.1,1.2)(1.2,0.6)(0.6,1.3)(0.3,0.8)
		\rput(2.5,0.6){\color{MidnightBlue} $V$}
		\pcline[linecolor=DarkOrange3,arrows=->](0,-1)(1.7,-0.2)
		\nbput{\color{DarkOrange3} $\bm{r}_i(t)$}
		\psdot(0,-1)
	\end{pspicture}
	\caption{Illustration für die Raumladungsdichte eines festen Volumen $V$.}
\end{figure}

\begin{align*}
	 Q_V(t) &= \int_{V} \sum_{i} q_i \delta\left(\bm{r} - \bm{r}_i(t)\right) \; \mathrm{d}^3r \\
	 &= \sum_{\left\{i|\bm{r}_i(t) \in V \right\}} q_i 
\end{align*}
%
Für die Stromdichte folgt:
\[
	\bm{j}(\bm{r},t) = \sum_{i} q_i \bm{v}_i(t) \delta \left(\bm{r} - \bm{r}_i(t)\right)
\]

\subsection{Diracs Delta-Distribution \texorpdfstring{$\delta$}{δ}}

\begin{enumerate}
	\item $\delta(x-x_0) = 0 \iff x \neq x_0$

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-0.3,-0.5)(2,2)
				\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(2,2)[{\color{DimGray} $x$}, -90][{\color{DimGray} $\infty$}, 0]
				\psxTick(0.8){\color{DimGray} x_0}
				\psline[linecolor=MidnightBlue](-0.2,0)(0.79,0)(0.8,2)(0.81,0)(1.8,0)
				\uput[0](0.8,1){\color{MidnightBlue} $\delta(x-x_0)$}
			\end{pspicture}
			\caption{Skizze der Delta-Distribution}
		\end{figure}

	\item 
		\begin{itemalign}
			\int_{a}^{b} f(x) \delta(x-x_0) \; \mathrm{d}x = \begin{cases} f(x_0) &, x_0 \in (a,b) \\ 0 &, x_0 \notin (a,b) \end{cases}
		\end{itemalign}
		%
		Einen Spezialfall stellt $f(x) = 1$ dar, denn:
		\[
			\int_{a}^{b} \delta(x-x_0) \; \mathrm{d}x = \begin{cases} 1 &, x_0 \in (a,b) \\ 0 &, x_0 \notin (a,b) \end{cases}
		\]
		Insbesondere gilt:
		\[
			\int_{-\infty}^{\infty} \delta(x-x_0) \; \mathrm{d}x = 1
		\]
		$\delta$ ist ein stetiges lineares Funktional auf den Raum der Testfunktion ($\to$ Distribution)

	\item Regularisierung:
		
		$\delta(x)$ als Folge gewöhnlicher Funktionen, z.b:
		\[
			\delta_n(x) = m e^{-\pi n^2 x^2}, \quad n \to \infty 
		\]

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-2,-0.8)(2,2)
				\psaxes[labels=none,ticks=none,arrows=->](0,0)(-2,-0.3)(2,2)[{\color{DimGray} $x$}, -90][,0]
				\psxTick(0.2659){\color{DimGray} \frac{1}{n \sqrt{2 \pi}}}
				\psyTick(1.5){\color{DimGray} n}
				\pscustom[linecolor=MidnightBlue,fillstyle=hlines,hatchcolor=DarkOrange3]{
					\psplot[plotpoints=200]{-1.8}{1.8}{1.5*EXP(-\psPi*1.5^2*x^2)}
				}
				% Line from (0, n/sqrt(e)) to (1/(n sqrt(2 pi), n/sqrt(e)) to (1/(n sqrt(2 pi), 0)
				\pnode(! 0 1.5 1 EXP sqrt div){A}
				\pnode(! 1 1.5 2 -1 ACOS mul sqrt mul div 1.5 1 EXP sqrt div){B}
				\pnode(! 1 1.5 2 -1 ACOS mul sqrt mul div 0){C}
				\psline(A)(B)(C)
				\uput[0](B){\color{DimGray} $\frac{n}{\sqrt{\mathrm{e}}}$}
				\rput(-1,0.5){\color{MidnightBlue} $\delta_n(x)$}
			\end{pspicture}
			\caption{Approximation der Delta-Distribution durch eine Funktionenfolge.}
		\end{figure}

		\begin{align*}
			\int_{-\infty}^{\infty} \delta_n(x) \; \mathrm{d}x &= 1 \\
			\lim\limits_{n \to \infty} \int_{-\infty}^{\infty} f(x) \delta_n(x) \; \mathrm{d}x &= f(0)
		\end{align*}
		%
		Weitere Möglichkeiten sind gegeben durch:
		\begin{itemize}
			\item 
				\begin{itemalign}
					\delta_\varepsilon(x) = \frac{1}{\pi} \Im\left(\frac{1}{x- \mathrm{i}\varepsilon}\right) = \lim\limits_{\varepsilon \to \infty} \frac{1}{\mathrm{i}} \frac{\varepsilon}{x^2 + \varepsilon^2} , \quad  \varepsilon \to 0
				\end{itemalign}
			\item 
				\begin{itemalign}
					\delta_m(x) = \frac{1}{\pi} \frac{\sin(mx)}{x} = \frac{1}{2 \pi} \int_{-m}^{m} e^{\mathrm{i} k x} \; \mathrm{d}k, \quad m \to \infty
				\end{itemalign}
		\end{itemize}

	\item Rechenregeln:
		\begin{itemize}
			\item 
				\begin{itemalign}
					\int_{a}^{b} f(x) \delta'(x) \; \mathrm{d}x
					&= f(x)\delta(x)|_a^b - \int_{a}^{b} f'(x) \delta(x) \; \mathrm{d}x \\
					&= \begin{cases}-f'(0) &, 0 \in (a,b) \\ 0 &, \text{sonst.}	\end{cases}
				\end{itemalign}
			\item 
				\begin{itemalign}
					\delta(g(x)) = \sum_{i}\frac{\delta(x-x_i)}{|g'(x_i)|} 
				\end{itemalign}
				$x_i$:  \quad $g(x_i)=0$ ist einfach Nullstelle
		\end{itemize}

	\item wichtige Anwendungen:
		\begin{itemize}
			\item
				\begin{itemalign}
					\delta(\bm{r}- \bm{r}_0) &= \delta(x-x_0) \delta(y-y_0)\delta(z-z_0) \\
					&= \int_{-\infty}^{\infty} \frac{\mathrm{d}^3k}{(2\pi)^3} e^{\mathrm{i} \bm{k} \left( \bm{r}- \bm{r}_0 \right)} \\
					&= \int_{-\infty}^{\infty} \frac{\mathrm{d}k_x}{2\pi} e^{\mathrm{i} k_x \left( x- x_0\right)} \int_{-\infty}^{\infty} \frac{\mathrm{d}k_y}{2\pi} e^{\mathrm{i} k_y \left( y- y_0\right)} \int_{-\infty}^{\infty} \frac{\mathrm{d}k_z}{2\pi} e^{\mathrm{i} k_z \left( z- z_0\right)}
				\end{itemalign}
			\item 
				\begin{itemalign}
					\nabla^2 \frac{1}{|\bm{r}-\bm{r}_0|} &= \left(\partial_x^2 + \partial_y^2 + \partial_z^2 \right) \frac{1}{|\bm{r}-\bm{r}_0|} \\
					&= -4 \pi \delta(\bm{r}-\bm{r}_0)
				\end{itemalign}
		\end{itemize}
\end{enumerate}

\paragraph{stationäre Ströme}
Ist die Stromdichte $\bm{j}$ zeitunabhängig und führt sie nirgends zu Ladungsanhäufungen, so handelt es sich um \acct{stationäre Ströme}. Es gilt:
\[
	\partial_t \bm{j} = 0 \qquad \text{und} \qquad \partial_t \varrho = 0.
\]
Wenn $\partial_t \varrho = 0$ so folgt aus unmittelbarer Konsequenz aus der Kontinuitätsgleichung:
\[
	\int_{\partial V} \bm{j}(\bm{r}) \cdot \bm{n} \, \mathrm{d}f = 0.
\]
Hieraus folgt, dass der Gesamtstrom $I$ durch jeden Querschnitt $\Delta F$ des Vektorfeldes $\bm{j}$ konstant ist. (Gilt für beliebig, geschlossene Oberflächen $\partial V$ ($\partial \partial V = 0$))

Betrachten wir das in Abbildung~\ref{fig:2013-04-17-1} abgebildete Kabel, wobei durch die Manteloberfläche kein Strom fließt.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-1,-1.5)(5,1.5)
		\psellipse[fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](0,0)(0.125,0.25)
		\psellipse[fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](4,1)(0.125,0.25)
		\psellipse[fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](4,-1)(0.125,0.25)
		\psbcurve(0,0.25)(2,0.75)L(2.5,1.25)(4,1.25)
		\psbcurve(0,-0.25)(2,-0.75)L(2.5,-1.25)(4,-1.25)
		\pscurve(4,0.75)(2.0,0)(4,-0.75)
		\psbcurve[linecolor=Purple,linestyle=dotted,dotsep=1pt](0,0)r(0.5,0)(1,0)(2,0.5)L(2.5,1)(4,1)
		\psbcurve[linecolor=Purple,linestyle=dotted,dotsep=1pt](0,0)r(0.5,0)(1,0)(2,-0.5)L(2.5,-1)(4,-1)
		\psline[linestyle=none,linecolor=Purple,arrows=->](0,0)(1,0)
		\psline[linecolor=MidnightBlue,arrows=->](0,0)(-1,0)
		\psline[linecolor=MidnightBlue,arrows=->](4,1)(5,1)
		\psline[linecolor=MidnightBlue,arrows=->](4,-1)(5,-1)
		\uput[-90](-1,0){\color{MidnightBlue} $\bm{n}_1$}
		\uput[-90](5,1){\color{MidnightBlue} $\bm{n}_2$}
		\uput[-90](5,-1){\color{MidnightBlue} $\bm{n}_3$}
		\uput[90](0,0.25){\color{DarkOrange3} $F_1$}
		\uput[-90](4,0.75){\color{DarkOrange3} $F_2$}
		\uput[90](4,-0.75){\color{DarkOrange3} $F_3$}
		\rput(1.8,0){\color{Purple} $\bm{j}$}
	\end{pspicture}
	\caption{Geteiltes Kabel, durch welches der Strom $\bm{j}$ fließt. Der Mantel ist stromlos.}
	\label{fig:2013-04-17-1}
\end{figure}

Für den Strom durch alle drei Querschnitssflächen $F_i$ gilt:
\[
	\int_{F_1 \cup F_2 \cup F_3 } \bm{j}\cdot \bm{n} \; \mathrm{d}f = I_1 + I_2 + I_3 = 0
\]
Dies veranschaulicht die \acct{1. Kirchhoffsche Regel}, nach der die Summe aller in einen Leiterzweig hinein- oder ausfließenden Ströme verschwindet. 

\section{Zirkulation eines Vektorfeldes, Strom und Magnetfeld}  
Wir betrachten die Fläche $F$ in Abbildung~\ref{fig:2013-04-17-2}.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.5,-1.5)(4.5,2)
		\pscurve[linecolor=DarkOrange3](0.5,0.5)(0,0.25)(0.5,0)(0.5,-0.25)(1,-0.5)
		\psecurve[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0.25)(0.5,0.5)(2,0)(1,-0.5)(0.5,-0.25)
		\psecurve(0,0.25)(0.5,0.5)
		(2,1.5)(2.5,1.4)(3,1.5)(4,1)(3.8,0)(4,-1)(2.5,-1.3)(2,-1.2)(1.5,-1.3)
		(1,-0.5)(0.5,-0.25)
		\psline[arrows=->](-0.5,-1.5)(0.5,-0.25)
		\rput(0.5,-0.25){
			\psline[linecolor=Purple,arrows=->](0,0)(0.6;10)
			\uput[90](0.6;10){\color{Purple} $\bm{V}(\bm{r},t)$}
		}
		\psellipse[linecolor=MidnightBlue,fillstyle=hlines,hatchcolor=MidnightBlue](3,1)(0.5,0.25)
		\psline[linecolor=MidnightBlue,arrows=->](3,1)(3.3,2)
		\psdot(-0.5,-1.5)
		\uput[180](3.3,2){\color{MidnightBlue} $\bm{n}$}
		\psline[arrows=->](2,1.1)(2,2)
		\psline[arrows=->](3.5,0)(4.5,-0.2)
		\uput[-90](4.5,-0.2){\color{DimGray} $\bm{V}$}
		\rput(3,-1){\color{DimGray} $F$}
		\uput[-45](0,-0.75){\color{DimGray} $\bm{r}$}
		\uput[20](2,0){\color{DarkOrange3} $\partial F = \mathcal{C}$}
	\end{pspicture}
	\caption{Der >>Kescher<< $F$, dessen Öffnung durch $\partial F = \mathcal C$ gegeben ist.}
\label{fig:2013-04-17-2}
\end{figure}

$\mathcal{C}$ sei hierbei ein geschlossener Weg ($0 = \partial \mathcal{C} = \partial\partial \mathcal{C}$). $\mathcal{C}$ ist so orientiert, dass $\mathcal{C}$ und die Flächennormale eine Rechtsschraube bilden. Wir betrachten die Zirkulation des Vektorfeldes $\bm{V}$ entlang $\mathcal{C}$:
\[
	\int_{\partial F} \bm{V}(\bm{r},t) \cdot \mathrm{d}\bm{r} = \Gamma_\mathcal{C}(\bm{V})
\]
$\Gamma_\mathcal{C}(\bm{V})$ bezeichnet hierbei den Weg.

Das Magnetfeld $\bm{B}$ wird ausgemessen mittels:
\begin{enumerate}
	\item Drehmoment auf (Standard-) Kompass
	\item Lorentzskraft: Ablenkung bewegter Ladungen

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(0,-0.5)(2.5,0.5)
			\psline[linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt](0,0)(2,0)
			\psarc[linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt](2,0.5){0.5}{-90}{0}
			\psline[linecolor=MidnightBlue,arrows=->](0,0)(1,0)
			\psdot(0,0)
			\rput(2,0.5){\color{DarkOrange3} $\otimes$}
			\rput(2,-0.5){\color{DarkOrange3} $\otimes$}
			\uput[-90](0.5,0){\color{MidnightBlue} $\bm{v}$}
			\uput[90](0,0){\color{DimGray} $q > 0$}
			\uput[45](2,-0.5){\color{DarkOrange3} $\bm{B}$}
		\end{pspicture}
		\caption{Eine Ladung $q$ wird durch ein Magnetfeld $\bm{B}$ abgelenkt.}
	\end{figure}

	\[
		\bm{F} = \frac{1}{c} q \bm{v}\times\bm{B}
	\]
	\begin{enumerate}
		\item Oersted, Ampère: Strom erzeugt Magnetfeld

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-0.25,-1)(3,1.5)
				\psellipse[fillstyle=vlines](2,0.25)(0.5,1)
				\rput(2,1.25){\psline[linecolor=DarkOrange3,arrows=->](0,0)(0.5,0)}
				\rput(2,-0.75){\psline[linecolor=DarkOrange3,arrows=->](0,0)(-0.5,0)}
				\rput(2.5,0.25){\psline[linecolor=DarkOrange3,arrows=->](0,0)(0,-1)}
				\rput(1.5,0.25){\psline[linecolor=DarkOrange3,arrows=->](0,0)(0,1)}
				\pscustom[fillstyle=solid,fillcolor=white]{
					\psline(2,0.5)(0,0.25)(0,-0.25)(2,0)
					\psellipticarc(2,0.25)(0.125,0.25){-90}{90}
				}
				\psellipse[fillstyle=solid,fillcolor=white](0,0)(0.125,0.25)
				\psline[linecolor=MidnightBlue,arrows=->](0.5,0.0625)(1.5,0.1875)
				\uput[0](2.5,1.25){\color{DarkOrange3} $\bm{B}$}
				\uput[-90](1,-0.1){\color{MidnightBlue} $\bm{I}$}
				\uput[0](2.5,0.4){\color{DimGray} $F$}
			\end{pspicture}
			\caption{Eine stromdurchflossener Leiter erzeugt ein Magnetfeld, das zirkular orientiert ist.}
		\end{figure}
		
		allgemein: $F$, $\partial F$
		stationäre Ströme:
		\[
			\underbrace{\frac{4 \pi }{c} \int_{F} \bm{j} \cdot \bm{n} \; \mathrm{d}f}_{=\frac{4 \pi }{c} I_F} = \int_{\partial F} \bm{B}(\bm{r}) \; \mathrm{d}\bm{r} = \Gamma_{\partial F}(\bm{B})
		\]
		Das Magnetfeld ist zeitunabhängig.
		\item weitere Erfahrungen:
		\begin{itemize}
			\item Es gibt keine magnetischen Ladungen (d.h. Monopole?)
			\item Die $\bm{B}$-Feldlinien sind geschlossen:
			\[
				\int_{\partial V} \bm{B} \cdot \bm{n} \; \mathrm{d}f = 0 \qquad \text{für beliebiges $V$, $\partial V$ geschlossen}
			\]
		\end{itemize}
	\end{enumerate}
\end{enumerate}

In Worten:
\begin{itemize}
	\item $\bm{B}$-Feld hat keine Quellen
	\item (stationäre) Ströme erzeugen $\bm{B}$-Feldwirbel
\end{itemize}

\begin{notice}[Gaußsches Gesetz:]
$\bm{E}$-Feld hat Quellen (Ladungen). Hat $\bm{E}$ auch Wirbel?
\end{notice}

Ein Vektorfeld ist erst durch seine Quellen und Wirbel festgelegt (später).
