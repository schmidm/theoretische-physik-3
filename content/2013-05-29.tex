% Henri Menke, Michael Schmid, Jan Schnabel , Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 29.05.2013
% TeX: Michi

\renewcommand{\printfile}{2013-05-29}

\section{Kräfte und Drehmomente auf lokalisierte Ladung- und Stromverteilungen}

Wir betrachten ein Gebiet $G$ in einem äußeren Magnetfeld $\bm{B}^{\mathrm{ext}}$ und einem elektrischen Feld $\bm{E}^{\mathrm{ext}}$. Die Vorgabe für die beiden Felder sei, dass sie nur langsam in $G$ variieren.  

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-1.5,0)(3,2)
		\psccurve[linecolor=MidnightBlue,fillstyle=solid,fillcolor=white](0.5,0.5)(1.5,1.7)(3,1.3)(2,0.2)
		\pscurve[arrows=->](1,2)(1.2,1.5)(1.2,0)
		\pscurve[arrows=->](2,2)(2.2,1.5)(2.2,0)
		\pscurve[arrows=->](0.5,1.6)(1.2,1.5)(2.2,1.5)(2.5,1.6)
		\pscurve[arrows=->](0.5,0.6)(1.2,0.5)(2.2,0.5)(2.5,0.6)
		\uput[0](2.5,0.6){\color{DimGray} $\bm{E}_\mathrm{ext}$}
		\uput[0](2.2,0){\color{DimGray} $\bm{B}_\mathrm{ext}$}
		\pnode(2.2,1.5){A}
		\cnode*(1.7,1){2pt}{B}
		\ncline[linecolor=DarkOrange3,arrows=->]{B}{A}
		\naput{\color{DarkOrange3} $\bm{r}$}
		\uput[-90](B){\color{DimGray} $O$}
		\uput[135](0.5,0.5){\color{MidnightBlue} $G: \varrho,\bm{j} \neq 0$}
	\end{pspicture}
	\caption{Das Gebiet $G$ mit den es durchsetzenden Feldern $\bm{E}_\mathrm{ext}$ und $\bm{B}_\mathrm{ext}$.}
\end{figure}

Zunächst Taylor-entwickeln wir die beiden Felder um $0$:
\[
	E_\alpha^{\mathrm{ext}} (\bm{r}) = E_\alpha^{\mathrm{ext}} (0) + x_\beta \partial_\beta E_\alpha^{\mathrm{ext}}(0) + \frac{1}{2} x_\beta x_\gamma \partial_\beta \partial_\gamma E_\alpha^{\mathrm{ext}} (0) + \ldots 
\]
Für das Magnetfeld gilt dementsprechend:
\[
	B_\alpha^{\mathrm{ext}} (\bm{r}) = B_\alpha^{\mathrm{ext}} (0) + x_\beta \partial_\beta B_\alpha^{\mathrm{ext}}(0) + \frac{1}{2} x_\beta x_\gamma \partial_\beta \partial_\gamma B_\alpha^{\mathrm{ext}} (0) + \ldots 
\]
Die beiden Felder sind statische äußere Felder. Mit Hilfe der Lorentzkraftdichte $\bm{k}$ lässt sich nun die Kraft $\bm{K}$ bestimmen:
%
\begin{align*}
	\bm{K} &= \int_{G} \bm{k}(\bm{r}) \, \mathrm{d}V \\
	&= \int_{G} \varrho(\bm{r}) \bm{E}^{\mathrm{ext}}(\bm{r}) \, \mathrm{d}V + \frac{1}{c} \int_{G} \bm{j}(\bm{r}) \times \bm{B}^{\mathrm{ext}}(\bm{r}) \, \mathrm{d}V \\
	&= \bm{K}_\mathrm{el} + \bm{K}_\mathrm{mag}.
\end{align*}
%
In $G$ gilt:
\[
	\nabla \times \bm{E}^{\mathrm{ext}} = 0 \quad \text{und} \quad \nabla \cdot \bm{E}^{\mathrm{ext}} = 0,
\]
da es sich um externe Felder und statische Felder handelt. Betrachten wir nun die Rotation des äußeren elektrischen Feldes:
%
\begin{align*}
	\nabla \times \bm{E}^{\mathrm{ext}} &= 0 \qquad \implies \qquad \partial_\gamma E_\alpha^{\mathrm{ext}} = \partial_\alpha E_\gamma^{\mathrm{ext}}
\end{align*}
%
Weiter folgt, dass
%
\begin{align*}
	\partial_\beta \partial_\gamma E_\alpha^{\mathrm{ext}} = \partial_\beta \partial_\gamma E_\gamma^{\mathrm{ext}}.
\end{align*}
%
Betrachten wir nun $\beta = \gamma$ so gilt für die obige Beziehung:
%
\begin{align*}
	\partial_\beta \partial_\beta  E_\alpha^{\mathrm{ext}} &= \nabla^2 E_\alpha^{\mathrm{ext}} \\
	&= \partial_\alpha \partial_\beta  E_\beta^{\mathrm{ext}} \\
	&= \partial_\alpha \nabla \cdot  \bm{E}^{\mathrm{ext}} = 0 \\
	\implies \nabla^2 E_\alpha^{\mathrm{ext}} &= 0
\end{align*}
%
Betrachten wir komponentenweise gilt:
%
\begin{align*}
	\delta_{\beta \gamma} \partial_\beta \partial_\gamma  E_\alpha^{\mathrm{ext}} = 0
\end{align*}
%
Damit folgt:
\[
	x_\beta x_\gamma \partial_\beta \partial_\gamma  E_\alpha^{\mathrm{ext}} = \frac{1}{3}(3 x_\beta x_\gamma - \underbrace{r^2 \delta_{\beta \gamma}) \partial_\beta \partial_\gamma  E_\alpha^{\mathrm{ext}}}_{= 0}
\]
Somit folgt für die $\alpha$-Komponente der Kraft $\bm{K}^{\mathrm{el}}$:
%
\begin{multline*}
	K_\alpha^{\mathrm{el}} =  E_\alpha^{\mathrm{ext}}(0) \int_{G} \varrho(\bm{r}) \, \mathrm{d}V + \partial_\beta  \alpha^{\mathrm{ext}}(0) \int_{G} x_\beta \varrho(\bm{r}) \, \mathrm{d}V \\ + \frac{1}{2} \left(\partial_\beta \partial_\gamma  E_\alpha^{\mathrm{ext}}(0)  \right) \int_{G} \frac{1}{3} \left(3 x_\beta x_\gamma - r^2 \delta_{\beta \gamma} \right)  \varrho(\bm{r}) \, \mathrm{d}V + \ldots
\end{multline*}
%
In Vektoren ergibt sich somit für die elektrische Kraft:
\[
	\boxed{ \bm{K} = q_{\mathrm{tot}} \bm{E}^{\mathrm{ext}}(0) + \left( \bm{p} \cdot \nabla \right) \bm{E}^{\mathrm{ext}}(0) +\frac{1}{6} \left(Q_{\alpha \beta } \partial_\alpha \partial_{\beta} \right) \bm{E}^{\mathrm{ext}}(0) + \ldots }
\]
$Q_{\alpha \beta}$ entspricht hierbei dem Quadrupoltensor. 

\begin{notice}[Speziell:]
	Kraft, die auf einen Dipol $\bm{p}$, der sich am Ort $\bm{r}_{\mathrm{Dipol}}$ befindet, durch ein äußeres Feld $\bm{E}^{\mathrm{ext}}$ ausgeübt wird:
	\[
		\boxed{ \bm{K}_{\mathrm{el}}^{\mathrm{Dipol}} = \left(\bm{p} \cdot \nabla\right)  \bm{E}^{\mathrm{ext}} |_{\bm{r}_{\mathrm{Dipol}}} }
	\]
\end{notice}
Im folgenden wollen wir nun die magnetische Kraft $\bm{K}_{\mathrm{mag}}$. $\bm{B}^{\mathrm{ext}}(0)$ gib keinen Beitrag, da:
\[
	\int_{G} \bm{j}(\bm{r}) \, \mathrm{d}V = 0.
\]
Wir betrachten somit das Kreuzprodukt von Stromdichte und externen Magnetfeld, wobei wir aber beachten, dass die Stromdichte nach $0$ verschoben wird, also mit $\bm{R} = \bm{r} + \bm{r}'$:

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,-1)(4.5,1.2)
		\psccurve[linecolor=MidnightBlue](0,0)(0.3,-1)(1,-0.9)(1.6,-1)(2,0)(1.8,1)(1,0.9)(0.6,1)
		\pnode(1,-0.5){O}
		\cnode*(4.5,0.8){2pt}{A}
		\pnode(1,0.5){B}
		\ncline[arrows=->]{O}{B}\naput{\color{DimGray} $\bm{r}'$}
		\ncline[arrows=->]{A}{O}\naput{\color{DimGray} $\bm{r}$}
		\ncline[arrows=->]{A}{B}\nbput{\color{DimGray} $\bm{R} = \bm{r} + \bm{r}'$}
		\rput(0.5,0.5){\color{MidnightBlue} $G_\bm{r}$}
	\end{pspicture}
	\caption{Gebiet $G_\bm{r}$ mit Koordinatensystem relativ zum Aufpunkt}
\end{figure}

\begin{align*}
	\bm{K}_{\mathrm{mag}} &= \frac{1}{c} \int_{G_{\bm{r}}} \bm{j}(\bm{R}) \times \bm{B}^{\mathrm{ext}}(\bm{R}) \, \mathrm{d}^3 R  \\
	&= \frac{1}{c} \int_{G_0} \bm{j}(\bm{r}-\bm{r}') \times \bm{B}^{\mathrm{ext}}(\bm{r} + \bm{r}') \, \mathrm{d}^3r'\\
\intertext{Mit einer Taylorentwicklung von $\bm{B}^{\mathrm{ext}}(\bm{r} + \bm{r}')$ um $\bm{r}$ folgt:}
	&\equiv \frac{1}{c} \int_{G_0} \tilde{\bm{j}}(\bm{r}') \times \left[ \bm{B}^{\mathrm{ext}}(\bm{r}) + \bm{r}' \nabla_{\bm{r}'} \bm{B}^{\mathrm{ext}} + \ldots \right] \, \mathrm{d}^3r'
\end{align*}
%
$\bm{j}$ ist die Stromdichte um $\bm{r}$, $\tilde{\bm{j}}$ ist die gleiche Stromdichte, aber nach $0$ verschoben. Die magnetische Kraft auf einen Dipol ergibt sich somit zu:
\[
	\bm{K}_{\mathrm{mag}}^{\mathrm{Dipol}}(\bm{r}) = \frac{1}{c} \int_{G} \bm{j}(\bm{r}') \times \left[ \left(\bm{r}'\cdot \nabla_{\bm{r}'} \right) \bm{B}^{\mathrm{ext}}(\bm{r}) \right] \, \mathrm{d}V'
\]

\begin{notice}[Nebenrechnung:]
Wir wollen den hinteren Term des Integrals etwas genauer analysieren und verwenden dabei die Eigenschaft
\[
	\rot \bm{B}^{\mathrm{ext}} = 0 \quad \implies \quad \partial_\beta B_\alpha^{\mathrm{ext}} = \partial_\alpha B_\beta^{\mathrm{ext}}.
\]
Für den Term des hinteren Integrals bedeutet dies:
%
\begin{align*}
	\left[ \left(\bm{r}'\cdot \nabla_{\bm{r}'} \right) \bm{B}^{\mathrm{ext}}(\bm{r}) \right]_\alpha &= x_\beta' \partial_\beta B_\alpha^{\mathrm{ext}} \\
	&= x_\beta' \partial_\alpha B_\beta^{\mathrm{ext}} \\
	&= \partial_\alpha \left(x_\beta' B_\beta^{\mathrm{ext}} \right) \\
	&= \left[ \grad_{\bm{r}} \left(\bm{r}' \cdot \bm{B}^{\mathrm{ext}}\right) \right]_\alpha
\end{align*}
%
\end{notice}

Damit ergibt sich für die magnetische Kraft auf einen Dipol:
%
\begin{align*}
	\bm{K}_{\mathrm{mag}}^{\mathrm{Dipol}}(\bm{r}) &= \frac{1}{c} \int_{G} \bm{j}(\bm{r}') \times \nabla_{\bm{r}} \left(\bm{r}' \cdot \bm{B}^{\mathrm{ext}}(\bm{r}) \right) \, \mathrm{d}V' \\
	&= -\frac{1}{c} \nabla_{\bm{r}} \times \int_{G} \bm{j}(\bm{r}') \cdot \left(\bm{r} \cdot \bm{B}^{\mathrm{ext}}\right) \, \mathrm{d}V' 
\end{align*}
%

\begin{notice}[Übung:]
Aus den Übungen sollte folgender integraler Zusammenhang bekannt sein:
%
\begin{align}
	\int_{G} \left(\bm{r} \cdot \bm{r}' \right) \bm{j}(\bm{r}') \, \mathrm{d}V' &= -\frac{1}{2} \bm{r} \times \int_{G} \left(\bm{r}' \times \bm{j}(\bm{r}') \right) \, \mathrm{d}V'  \nonumber \\
	&= c \bm{m} \times \bm{r} = - c \bm{r} \times \bm{m} \label{eq: Uebung_1}
\end{align}
%
In unserer jetzigen Betrachtung wird $\bm{r}$ mit $\bm{B}$ ersetzt.
\end{notice}

Mit Gleichung~\eqref{eq: Uebung_1} kann $\bm{K}_{\mathrm{mag}}^{\mathrm{Dipol}}$ umgeschrieben werden:
%
\begin{align*}
	\bm{K}_{\mathrm{mag}}^{\mathrm{Dipol}} &= \nabla_{\bm{r}} \times \left[ \bm{B}^\mathrm{ext}(\bm{r}) \times \bm{m} \right] 
\intertext{Mit $\bm{a} \times \left(\bm{b} \times \bm{c}\right) = \bm{b} (\bm{a} \cdot \bm{c}) - \bm{c} (\bm{a} \cdot \bm{b})$ folgt:}
	&= \left(\bm{m} \cdot \nabla_{\bm{r}}\right) \bm{B}^\mathrm{ext}(\bm{r}) - \bm{m} \underbrace{\left(\nabla_{\bm{r}} \cdot \bm{B}^\mathrm{ext}(\bm{r}) \right)}_{=0} 
\intertext{Mit der obigen Nebenrechnung, wobei $\bm{r}'$ mit $\bm{m}$ ersetzt wir folgt:}
	&= \grad_{\bm{r}} \left(\bm{m} \cdot \bm{B}^{\mathrm{ext}}(\bm{r})\right)
\end{align*}
%
Damit gilt für die magnetische Kraft auf einen Dipol:
\[
	\boxed{ \bm{K}_{\mathrm{mag}}^{\mathrm{Dipol}} = \left(\bm{m} \cdot \nabla_{\bm{r}} \right) \bm{B}^{\mathrm{ext}}(\bm{r})|_{\bm{r}_{\mathrm{Dipol}}} = \nabla_{\bm{r}}\left(\bm{m} \cdot \bm{B}^{\mathrm{ext}}(\bm{r}) \right)|_{\bm{r}_{\mathrm{Dipol}}} }
\]

\begin{notice}
Bekanntlich gilt für die Kraft folgende Beziehung:
\[
	\bm{K} = - \nabla U_{\mathrm{pot}},
\]
wobei $U_{\mathrm{pot}}$ das Potential ist. Die potentielle Energie eines permanenten magnetischen Moment (oder Dipol) in einen externen Magnetfeld kann entweder von einer Kraft, oder von einem Drehmoment erhalten werden. Interpretieren wir die Kraft als negativen Gradienten eines Potentials $U_{\mathrm{pot}}$, finden wir
\[
	U_{\mathrm{pot}} = - \bm{m} \cdot \bm{B}.
\]
Es zeigt sich, dass $\bm{m}$ eine parallele Orientierung zu $\bm{B}$ erreichen möchte, damit $U_{\mathrm{pot}}$ minimal ist.  
\end{notice}

\paragraph{Drehmomente:}
%
Für das Drehmoment $\bm{N}$ gilt
%
\begin{align*}
	\bm{N} &= \int_{G} \bm{p} \times \bm{k}(\bm{r}) \, \mathrm{d}V \\
	&= \bm{N}_{\mathrm{el}} + \bm{N}_{\mathrm{mag}}.
\end{align*}
%
Für ein elektrisches, bzw.\ magnetisches \acct{Drehmoment eines Dipols} gilt:
\[
	\boxed{  \bm{N}_{\mathrm{el}}^{\mathrm{Dipol}} = \bm{p} \times \bm{E}^{\mathrm{ext}}|_{\bm{r}_{\mathrm{Dipol}}}  \quad \text{und} \quad\bm{N}_{\mathrm{mag}}^{\mathrm{Dipol}} = \bm{m} \times \bm{B}^{\mathrm{ext}}|_{\bm{r}_{\mathrm{Dipol}} }  }
\]
Betrachten wir zuerst das elektrische Drehmoment:
%
\begin{align*}
	\bm{N}_{\mathrm{el}} &= \int_{G} \bm{r} \times \varrho(\bm{r}) \bm{E}^{\mathrm{ext}}(\bm{r}) \, \mathrm{d}V \\
	&= - \bm{E}^{\mathrm{ext}}(0) \times \underbrace{\int_{G} \varrho(\bm{r}) \bm{r} \, \mathrm{d}V}_{ = \bm{p}} \\
	&= - \bm{E}^{\mathrm{ext}} \times \bm{p} 
\end{align*}
%
Für das magnetische Drehmoment gilt:
%
\begin{align*}
	\bm{N}_{\mathrm{mag}} &= \frac{1}{c} \int_{G} \bm{r} \times \left(\bm{j}(\bm{r}) \times \bm{B}^{\mathrm{ext}}(0) \right) \, \mathrm{d}V \\
	&= \frac{1}{c} \int_{G} \left[ \left( \bm{r} \cdot \bm{B}^{\mathrm{ext}}(0) \right) \bm{j}(\bm{r}) - \left(\bm{r} \cdot \bm{j}(\bm{r}) \right) \bm{B}^{\mathrm{ext}}(0)  \right] \, \mathrm{d}V 
\intertext{Durch Verwendung von Gleichung~\eqref{eq: Uebung_1}, wobei $\bm{r} \to \bm{r}'$ und $\bm{B}^{\mathrm{ext}}(0) \to \bm{r}$, wird aus dem linken Term des Integrand:}
	&= \bm{m} \times \bm{B}^{\mathrm{ext}}(0) - \frac{1}{c} \bm{B}^{\mathrm{ext}}(0) \underbrace{\int_{G} \left(\bm{r}\cdot \bm{j}(\bm{r}) \right) \, \mathrm{d}V}_{= 0}
\end{align*}
%
Der zweite Term verschwindet, da
\[
	\int_{G} x_j j_\beta \, \mathrm{d}V = - \int_{G} x_\beta j_\alpha \, \mathrm{d}V,
\]
falls $j$ in $G$ lokalisiert ist. Für $\alpha = \beta$ (aber ohne Summenkonvention)
\[
	\int_{G} x_\alpha j_\alpha \, \mathrm{d}V = 0.
\]
Zur Erzeugung von Kräften brauch man also Feldgradienten, für Drehmomente nicht.

\begin{notice}[Fazit:]
Dipole erfahren im homogenen Feldern Drehmomente aber keine Kräfte, inhomogene Felder üben auf Dipole Kräfte aus.
\end{notice}

\section[Elektrostatik im begrenzten Raum]{Elektrostatik im begrenzten Raum (Randwertprobleme)}

Wir betrachten ein Volumen $V$ in dem ein Leiter $L$ mit dem Rand $\partial L$ ist, Ladungen $q_i$, sowie eine Ladungsverteilung $\varrho(\bm{r})$. Der Rand des Volumen $V$ ist $\partial V$. Bezüglich des Leiters ist $V$ außen. Der Normalenvektor  $\bm{n}$ des Leiters steht senkrecht auf dessen Rand und zeige ins Innere von $V$ (bezüglich des Leiters also nach außen).

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,-0.5)(3,1.5)
		\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
		\psellipse[linecolor=DarkOrange3](2,0.5)(0.5,0.25)
		\psline[linecolor=DarkOrange3,arrows=->](2,0.75)(2,1.5)
		\uput[0](2,1.5){\color{DarkOrange3} $\bm{n}$}
		\rput(2,0.5){\color{DarkOrange3} $L$}
		\psdots*(0.8,0.3)(0.5,0.5)(1,0.8)(1.3,0.6)(1.2,0.3)
		\rput(1,0.5){\color{DimGray} $q_i$}
		\rput(1,1.2){\color{MidnightBlue} $V$}
		\pcarc(1,0)(0.5,-0.3)
		\uput{0.1}[180](0.5,-0.3){\color{MidnightBlue} $\partial V$}
		\pscircle[fillstyle=hlines,hatchsep=2pt](1.9,-0.2){0.25}
		\pcarc(2.15,-0.2)(2.8,-0.3)
		\uput[0](2.8,-0.3){\color{DimGray} $\varrho(\bm{r})$}
	\end{pspicture}
	\caption{Ein in ein Volumen $V$ eingebetteter Leiter $L$.}
\end{figure} 

Es gilt:
\begin{itemize}
	\item $\bm{E} = 0$ im Leiter (Statik)
	\item $\bm{n} \times \bm{E}|_{\partial L}$ stetig (Stetigkeit der Tangentialkomponente)
	\item $\bm{E} \perp \partial L$ impliziert, das $\partial L$ eine Fläche konstanten Potentials ist (Äquipotentialfläche)
	\item $E_n \coloneq  \bm{E}^{\mathrm{außen}} \cdot \bm{n}|_{\partial L} = - \bm{n} \nabla \phi|_{\partial L} = 4 \pi \sigma$, wobei $\sigma$ auf $\partial L$ einen Strom in $E_n$ erzeugt.
\end{itemize}

\subsection{Randwertaufgaben der Potentialtheorie im begrenzten Raum $V$}
Wir betrachten die Poisson-Gleichung
\[
	\nabla^2 \phi = -4 \pi \varrho
\]
in $V$ mit zwei Arten von Randbedingungen, die \acct{Dirichlet-Randbedingung} und die \acct{Neumann-Randbedingung}.
\begin{description}
	\item[Dirichlet-Randbedingung (D):] Gesucht wird eine Lösung der Poisson-Gleichung, für die $\phi$ auf im Endlichen gelegenen vorgegebenen Flächen (hier $\partial L$) eine gegebene Funktion 
	\[
		\phi|_{\partial L} = f(\bm{r})
	\]
	ist und gegebenfalls für $|\bm{r}| \to \infty$ mindestens wie $1/r$ abfällt.
	\item[Neumann-Randbedingung (vNM):] Gesucht wird eine Lösung der Poisson-Gleichung, für die $\bm{n} \cdot \nabla \phi = \partial_n \phi$ auf im Endlichen gelegenen vorgegebenen Flächen (hier $\partial L$) gegebene Werte
	\[
		\bm{n} \cdot \nabla \phi|_{\partial L} = - E_n = g(\bm{r})
	\]
	annimmt und gegebenfalls  für $|\bm{r}| \to \infty$ mindestens wie $1/r^2$ abfällt.
\end{description}
%
\begin{proof} Eindeutigkeit der Lösung.

	Seien $\phi_1$ und $\phi_2$ zwei Lösungen zum gleichen $f(\mathrm{D})$ bzw. $f(\mathrm{vNM})$. Wie definieren zunächst eine Funktion $\psi$ mit
	\[
		\psi \coloneq \phi_1 - \phi_2,
	\] 
	die die Eigenschaft 
	\[
		\nabla \psi = 0 \quad \text{und} \quad 
		\begin{cases}
			\psi|_{\partial V} = 0 &, \text{bzw.}\\
			\partial_n \psi|_{\partial V} = 0 
		\end{cases}
	\]
	erfüllt. Wir betrachten nun das Integral 
	%
	\begin{align*}
		\int_{V} \left(\nabla \psi\right)^2 \, \mathrm{d}V &= \int_{V} \left(\nabla \psi\right) \left(\nabla \psi\right)\, \mathrm{d}V \\
		&= \int_{V} \biggl[ \div\left(\psi \grad\psi\right) - \psi \underbrace{\Delta \psi}_{=0} \biggr] \, \mathrm{d}V \\
		&= \int_{\partial V} \psi \left(\grad \psi\right) \, \mathrm{d}\bm{f} \\
		&= \int_{\partial V} \psi \partial_n \psi \, \mathrm{d}f 
	\end{align*}
	%
	Unabhängig davon welche Randbedingung nun verwendet wir, bzw.\ gegeben ist --- das Integral verschwindet. Damit folgt, dass $\nabla \psi = 0$ in $V \cup \partial V$ ist und $\psi = \mathrm{const}$ in $V \cup \partial V$ ist. Je nach Randbedingung gilt also:
	\[
		\psi = 
		\begin{cases}
			0 &,\text{in V für (D)} \\
			\mathrm{const} &,\text{in V für (vNM)}
		\end{cases}
	\]
	Damit ist der Beweis der Eindeutigkeit von $\phi$ für beide Randbedingungen erbracht.
\end{proof}

Für das Lösen der Poisson-Gleichung wurden verschiedene Lösungsmethoden entwickelt, die wichtigsten sind
\begin{itemize}
	\item Spiegelungsmethode
	\item Greensche Funktion
	\item Methode der Inversion
	\item Separation der Variablen
\end{itemize}

\begin{notice}
	Randwertprobleme treten in der Physik in vielen Gebieten auf:
	\begin{itemize}
		\item Hydrodynamik
		\item Elastizitätstheorie
		\item Quantenmechanik
	\end{itemize}
\end{notice}

\subsection{Beispiel zu Spiegelungsmethode}

\begin{example} Der leitende Halbraum, ein Dirichlet-Problem: Wir betrachten eine Punktladung $q$ die sich auf der $x$-Achse bei $\bm{r} = \bm{a}$ eines Koordinatensystems befindet und ein Leiter, dessen Oberfläche durch die $y$-Achse und $z$-Achse aufgespannt wird. Die Ladung befinde sich  vor diesem Leiter. 

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-3,-2)(3,2)
		% Ursprung als Node
		\pnode(0,0){O}
		% Achsen
		\psframe[linestyle=none,fillstyle=hlines](-0.4,-1.8)(0,1.8)
		\psaxes[labels=none, ticks=none]{->}(0,0)(-2.7,-2)(2.7,2)
		% Ladung und Spigelladung und Vektoren
		\cnode*(2,0){2pt}{q1}
		\cnode*[linecolor=DarkOrange3](-2,0){2pt}{q2}
		\ncline[arrows=->]{O}{q1}
		\ncline[linecolor=DarkOrange3,arrows=->]{O}{q2}
		\psline[arrows=->](0,-1)(0.5,-1)
		% Labels
		\uput[-90](-1,0){\color{DarkOrange3} $-\bm{a}$}
		\uput[135](q2){\color{DarkOrange3} $-q$}
		\uput[-90](1,0){\color{DimGray} $\bm{a}$}
		\uput[45](q1){\color{DimGray} $q$}
		\uput[-90](2.7,0){\color{DimGray} $x$}
		\uput[0](0.5,-1){\color{DimGray} $\bm{n}$}
		\uput[180](2,-2){\color{DimGray} $\phi|_{\partial L} = 0$}
		\pcarc[arcangle=20,arrows=<-](0.1,-1.5)(0.5,-1.8)
		\cput(-1,1.5){\color{DimGray} $L$}
	\end{pspicture}
	\caption{Der leitende Halbraum.}
\end{figure} 

Im folgenden Abschnitt wollen wir das Potential $\phi$ mit einer gegebenen Dirichlet-Randbedingung lösen. Es gilt die Poisson-Gleichung:
\[
	\Delta \phi = - 4 \pi q \delta(\bm{r}-\bm{a}),
\]
und die Randbedingung
\[
	\phi |_{\partial L} = 0.
\]
Gesucht ist also das Potential $\phi(\bm{r})$ für $x > 0$. Dies erreichen wir durch Einführung einer zusätzlichen Ladung $-q$ bei $\bm{r} = - \bm{a}$. Der Leiter sei geerdet, d.h.\ er besitzt das gleiche Potential auf der Oberfläche wie im unendlichen, d.h.\ $0$.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-3,-1.5)(3,1.5)
		\psellipse(-2,0)(1,0.5)
		\psellipse(2,0)(1,0.5)
		\psellipticarc(-3,0)(2.5,1.5){-90}{90}
		\psellipticarc(3,0)(2.5,1.5){90}{270}
		\psline(0,-1.5)(0,1.5)
		\cnode(-1.5,0){2pt}{q2}
		\cnode*(1.5,0){2pt}{q1}
		\uput[0](q1){\color{DimGray} $q$}
		\uput[180](q2){\color{DimGray} $-q$}
	\end{pspicture}
	\caption{Äquipotentialflächen des leitenden Halbraumes.}
\end{figure} 

Die Lösung der Possion-Gleichung sei gegeben durch 
%
\begin{align}
	\phi(\bm{r}) &= \frac{q}{|\bm{r}-\bm{a}|}- \frac{q}{|\bm{r}+\bm{a}|} \label{eq: leitender Halbraum}
\end{align}
%
Für $x>0$  bedeutet dies für die Poisson-Gleichung:
%
\begin{align*}
	\nabla^2 \phi(\bm{r}) &= - 4 \pi q \left\{  \delta(\bm{r}-\bm{a}) - \delta(\bm{r}+\bm{a}) \right\} \stackrel{!}{=} 4 \pi q \delta(\bm{r}-\bm{a}).
\end{align*}
%
Somit lautet das gesuchte Potential:
\[
	\phi(\bm{r}) = q \left[ \left((x-a)^2 +y^2 +z^2\right)^{-1/2} + \left((x+a)^2 +y^2 +z^2\right)^{-1/2} \right].
\]
Für $x=0$ bedeutet dies, dass das Potential verschwindet:
\[
	\phi(\bm{r})|_{x=0} = 0. 
\]
\begin{notice}[Fazit:]
	Gleichung~\eqref{eq: leitender Halbraum} ist die eindeutige Lösung der Randwertproblems.
\end{notice}
Für die Influenzladung $\sigma(y,z)$ auf der Leiteroberfläche gilt
%
\begin{align*}
	4 \pi \sigma = \bm{E} \cdot \bm{n} &= - \bm{n} \cdot \nabla \phi = \begin{pmatrix} 1 \\ 0 \\ 0\end{pmatrix} \cdot \nabla \phi \\
	&= - \partial_x \phi |_{x=0} \\
	&= \frac{-2 q a }{\bigl(a^2 + \underbrace{y^2 +z^2}_{=r^2} \bigr)}^{3/2}
\end{align*}
%
Die gesamte induzierte Ladung berechnet sich aus
%
\begin{align*}
	\int_{\partial L} \sigma(y,z) \, \mathrm{d}f &= -\frac{2 q a }{4 \pi} 2 \pi \int_{0}^{\infty} r \left(a^2 +r^2\right)^{-3/2} \, \mathrm{d}r \\
	&= -qa \int_{0}^{\infty} \frac{\mathrm{d}}{\mathrm{d}r} \left((-1) \left(a^2+r^2\right)^{-1/2}\right) \, \mathrm{d}r \\
	&= \left. qa \left(a^2 +r^2\right)^{-1/2} \right|_{0}^{\infty} \\
	&= -qa (a^2)^{-1/2} \\
	&= -q
\end{align*}
%
D.h.\ die gesamte Influenzladung ist entgegengesetzt gleich groß zur verursachenden Punkladung.
\end{example}
