% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 03.07.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-07-03}

\paragraph{Alternative zum Postulat $V_0 = c$}
$V_0$ kann aus dem Experiment (z.B.\ aus dem Zerfall von Pionen) erschlossen werden. Die Transformationsformel zusammen mit $V=V_0$ prognostiziert eine Zeitdilatation (siehe später), die von $V_0$ anhängt. Die Messung dieser Dilatation liefert umgekehrt $V_0$ mit dem Wert $\approx \SI{300000}{\km\per\s}$, innerhalb der Messgenauigkeit.

\begin{notice}
	\begin{align*}
		V_0 = c \iff m_\mathrm{Photon} = 0
	\end{align*}
	%
	Dass $V_0 = c$ hat zur Folge, dass die Ruhemasse des Photons verschwindet.

	Aber auch wenn $m_\mathrm{Photon} > 0$ wäre, würde das dem Einsteinschen Relativitätsprinzip nicht schaden. Allerdings wäre dann die Maxwellsche Elektrodynamik nicht mehr streng lorentzinvariant und müsste -- wie die Mechanik -- korrigiert werden.
\end{notice}

\section{Raumzeitgeometrie}

\begin{figure}
	\centering
	\begin{pspicture}(-0.7,-0.7)(3,3)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(3,3)[{\color{DimGray} $x$},-90][{\color{DimGray} $ct$},180]
		\psline[linestyle=dotted,dotsep=1pt](0,1)(1,1)(1,0)
		\psline[linestyle=dotted,dotsep=1pt](0,2)(2,2)(2,0)
		\psline[linestyle=dotted,dotsep=1pt](1,1)(1,2)
		\psdots*[linecolor=MidnightBlue](1,1)(1,2)(2,2)
		\uput[-225](1,1){\color{MidnightBlue} $C$}
		\uput[45](2,2){\color{MidnightBlue} $B$}
		\uput[135](1,2){\color{MidnightBlue} $A$}
		\psline[linecolor=DarkOrange3,arrows=|<->|](-0.2,1)(-0.2,2)
		\uput[180](-0.2,1.5){\color{DarkOrange3} $\Delta t_{AB}$}
		\psline[linecolor=DarkOrange3,arrows=|<->|](1,-0.2)(2,-0.2)
		\uput[-90](1.5,-0.2){\color{DarkOrange3} $\Delta x_{AB}$}
		\pscurve[linecolor=MidnightBlue,arrows=->](2.3,0)(2.5,0.5)(2.3,1)(2.5,1.5)(2.8,2)
		\uput[-45](2.8,2){\color{MidnightBlue} $\bm{r}(t)$}
		\cput(1.5,2.5){\color{DimGray} $\Sigma$}
	\end{pspicture}
	\caption{$A,B,C$ sind Ereignisse. Die Weltlinie $\bm{r}(t)$ hat stets eine Steigung größer als $1$, d.h. $|v| < c$}
	\label{fig:2013-07-03-1}
\end{figure}

In Abbildung~\ref{fig:2013-07-03-1} ist sichtbar, dass
%
\begin{align*}
	x = vt \iff ct = \frac{c}{v} x
\end{align*}
%
Wählen wir die Ereignisse $A$ und $B$, dann
%
\begin{align*}
	\Delta x_{AB} \neq 0 \; , \quad \Delta t_{AB} = 0
\end{align*}
%
bedeutet Gleichzeitigkeit in $\Sigma$. In einem System $\Sigma'$ sieht das anders aus
%
\begin{align*}
	\Delta t_{AB}' = \gamma \left( \Delta t_{AB} - \frac{v}{c} \Delta x_{AB} \right) \neq 0
\end{align*}
%
Das heißt: Die Gleichzeitigkeit zweier \emph{räumlich getrennter} Ereignisse ist kein bewegungsinvarianter Begriff -- \acct{Relativität der Gleichzeitigkeit}.

Wie sieht das Bild der Bewegung unter der Transformation aus? Für die $t'$-Achse (= Weltlinie von $O'$) gilt
%
\begin{align*}
	x' = 0 \implies c t_{O'} = \frac{c}{v} x_{O'}
\end{align*}
%
Dies ist die Form einer Gerade mit Steigung $c/v > 1$. Für die $x'$-Achse folgt:
%
\begin{align*}
	t' = 0 \implies c t_{x'} = \frac{v}{c} x_{x'}
\end{align*}
%
Hier erhalten wir eine Gerade der Steigung $v/c < 1$.
%
\begin{figure}
	\centering
	\begin{pspicture}(-0.3,-1.8)(3,3)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-1.8)(3,3)[{\color{DimGray} $x$},-90][{\color{DimGray} $ct$},180]
		\psline[linestyle=dotted,dotsep=1pt](0,2)(2,2)(2,0)
		\psxTick(2){\color{DimGray} x_A}
		\psyTick(2){\color{DimGray} c t_A}
		\psline[linecolor=DarkOrange3](-0.5;20)(3;20)
		\psline[linecolor=DarkOrange3](-0.5;70)(3;70)
		\uput[0](3;70){\color{DarkOrange3} $c t'$}
		\uput[-90](3;20){\color{DarkOrange3} $x'$}
		\psarcn[linecolor=MidnightBlue,arrows=->](0,0){2.5}{90}{70}
		\psarc[linecolor=MidnightBlue,arrows=->](0,0){2.5}{0}{20}
		\psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](2;20)(2,2)
		\uput{2.2}[80](0,0){\color{MidnightBlue} $\alpha$}
		\psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](2;70)(2,2)
		\uput{2.2}[10](0,0){\color{MidnightBlue} $\alpha$}
		\psdots*[linecolor=DarkOrange3](2;20)(2;70)
		\uput[135](2;20){\color{DarkOrange3} $x_{A}'$}
		\uput[-45](2;70){\color{DarkOrange3} $t_{A}'$}
		\psdots*[linecolor=MidnightBlue](2,2)\uput[45](2,2){\color{MidnightBlue} $A$}
		\rput(0,-1.5){
			\psline[linestyle=dashed](-0.3,0)(3,0)
			\psline[linecolor=DarkOrange3](-0.5;20)(3;20)
			\uput[0](3;20){\color{DimGray} in $\Sigma$}
			\uput[0](3,0){\color{DimGray} in $\Sigma'$}
			\rput(3,0.5){
				\rput[l](0,0){\color{DimGray} gleichzeitig}
				\psline[arrows=<->](-0.5,-0.3)(-0.1,0)(-0.5,0.3)
			}
		}
	\end{pspicture}
	\caption{$c t'$ sind die Punkte $(x,c,t)$ in $\Sigma$, die $x' = 0$ entsprechen; das sind diejenigen mit $ct = c/v \, x$. $x'$ sind die Punkte $(x,c,t)$ in $\Sigma$, die $t' = 0$ entsprechen; das sind diejenigen mit $ct = v/c \; x$.}
\end{figure}
%
\begin{align*}
	c t_{A}' &= \gamma \left( c t_A - \frac{v}{c} x_A \right) \\
	x_{A}' &= \gamma \left( x_A - \frac{v}{c} c t_A \right)
\end{align*}

Nun sind die Gleichzeitigkeit und die Gleichortigkeit nicht mehr nur einfach die Projektionen auf die $x$- oder $t$-Achse, sondern hängt vom Bezugssystem ab.
%
\begin{align*}
	\tan \frac{v}{c} \; , \quad \tan(\SI{90}{\degree} - \alpha) = \cot \alpha = \frac{1}{\tan \alpha} = \frac{c}{v}
\end{align*}
%
Im Grenzfall $v \to c$ wird $\alpha = \SI{45}{\degree}$.

Die Koordinatendifferenzen zweier Ereignisse $A$ und $B$ sind
%
\begin{align*}
	\text{in $\Sigma$} &: \Delta x, \Delta y, \Delta z, \Delta t \\
	\text{in $\Sigma'$} &: \Delta x', \Delta y', \Delta z', \Delta t'
\end{align*}
%
Damit ergibt sich der Zusammenhang
%
\begin{equation*}
	\begin{aligned}
		\Delta x' &= \gamma [ \Delta x - v \Delta t ] \\
		\Delta y' &= y \\
		\Delta z' &= z \\
		\Delta t' &= \gamma \left[ \Delta t - \frac{v}{c^2} \Delta x \right]
	\end{aligned}
\end{equation*}
%
Mit dieser Lorentztransformation erhält man
%
\begin{align*}
	S_{AB}^2
	&= (\Delta x)^2 + (\Delta y)^2 + (\Delta z)^2 - c^2 (\Delta t)^2 \\
	&= (\Delta x')^2 + (\Delta y')^2 + (\Delta z')^2 - c^2 (\Delta t')^2
\end{align*}
%
dies gilt insbesondere für den Lichtkegel $S^2 = 0$. Das heißt: Die Größe $S_{AB}^2$ ist eine lorentzinvariante Größe. $S_{AB}^2$ wird als Raum-Zeit-Abstand oder Intervall von $AB$ bezeichnet.

\paragraph{Lichtkegel}
%
\begin{figure}
	\centering
	\begin{pspicture}(-2,-2)(2,2)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-2,-2)(2,2)[{\color{DimGray} $x$},-90][{\color{DimGray} $ct$},180]
		\psline[linecolor=MidnightBlue](-2;45)(2;45)
		\psline[linecolor=MidnightBlue](-2;135)(2;135)
		\psline[linestyle=dotted,dotsep=1pt](-2;20)(2;20)
		\uput[0](2;20){\color{DimGray} $x'$}
		\psline[linestyle=dotted,dotsep=1pt](-2;70)(2;70)
		\uput[0](2;70){\color{DimGray} $ct'$}
		\psellipse[linecolor=MidnightBlue](0,1)(1,0.25)
		\psarcn[linecolor=DarkOrange3,arrows=->](0,0){1.8}{90}{70}
		\psarc[linecolor=DarkOrange3,arrows=->](0,0){1.8}{0}{20}
		\uput{1.5}[80](0,0){\color{DarkOrange3} $\alpha$}
		\uput{1.5}[10](0,0){\color{DarkOrange3} $\alpha$}
	\end{pspicture}
	\caption{Die den Lichtkegel aufspannenden Geraden sind gegeben durch $ct =x \equiv ct' = x'$ und $ct = -x$.}
\end{figure}
%
\begin{align*}
	S^2 = 0 = (\Delta \bm{r})^2 - c^2 (\Delta t)^2
\end{align*}
%
Der invariante Lichtkegel definiert eine Raum-Zeit-Metrik (Minkowskimetrik).

\paragraph{Vergleich}

\begin{description}
	\item[Euklidischer Raum:] (Abbildung \ref{fig:2013-07-03-4})
		\[ (\Delta x)^2 + (\Delta y)^2 + (\Delta z)^2 = r_p^2 = |\bm{r}_p|^2 \]
		invariant bei rämlichen Drehungen $D^{-1} = D^\top$.
		%
		\begin{figure}[!htb]
			\centering
			\begin{pspicture}(-2,-2)(2.5,2.5)
				\psaxes[labels=none,ticks=none,arrows=->](0,0)(-2,-2)(2,2)[{\color{DimGray} $x$},0][{\color{DimGray} $y$},90]
				\rput{10}{\psaxes[labels=none,ticks=none,arrows=->](0,0)(0,0)(2,2)[{\color{DimGray} $x'$},0][{\color{DimGray} $y'$},90]}
				\pscircle[linecolor=DarkOrange3!100](0,0){0.5}
				\pscircle[linecolor=DarkOrange3!80](0,0){1.0}
				\pscircle[linecolor=DarkOrange3!60](0,0){1.5}
				\psline[linecolor=MidnightBlue,arrows=->](0,0)(1.5;45)
				\uput[45](1.5;45){\color{MidnightBlue} $\bm{r}_p$}
				\uput{1.7}[70](0,0){\color{MidnightBlue} $P$}
			\end{pspicture}
			\caption{Euklidische Metrik}
			\label{fig:2013-07-03-4}
		\end{figure}

	\item[Minkowskimetrik:] (Abbildung \ref{fig:2013-07-03-5}) Flächen konstanten Abstandes aus $S^2 = \mathrm{const}$ sind hyperbolische Flächen.
		\[ x^2 + y^2 + z^2 - c^2 t^2 = S = \mathrm{const} \implies \frac{x^2}{S^2} + \frac{y^2}{S^2} + \frac{z^2}{S^2} - \frac{c^2 t^2}{S^2} = 1 \]
		%
		\begin{figure}[!htb]
			\centering
			\begin{pspicture}(-2,-2)(2,2)
				\psaxes[labels=none,ticks=none,arrows=->](0,0)(-2,-2)(2,2)[{\color{DimGray} $x$},-90][{\color{DimGray} $ct$},180]
				\psline[linestyle=dotted,dotsep=1pt](-2.5;45)(2.5;45)
				\psline[linestyle=dotted,dotsep=1pt](-2.5;135)(2.5;135)
				\rput(0,0){
					\psplot[linecolor=MidnightBlue]{-1.414}{1.414}{sqrt(x*x + 0.5)}
					\psplot[linecolor=DarkOrange3]{-1.414}{1.414}{-sqrt(x*x + 0.5)}
					\psplot[linecolor=MidnightBlue]{-1.414}{1.414}{sqrt(x*x + 1.5)}
					\psplot[linecolor=DarkOrange3]{-1.414}{1.414}{-sqrt(x*x + 1.5)}
				}
				\rput{90}(0,0){
					\psplot[linecolor=DarkRed]{-1.414}{1.414}{sqrt(x*x + 0.5)}
					\psplot[linecolor=DarkGreen]{-1.414}{1.414}{-sqrt(x*x + 0.5)}
					\psplot[linecolor=DarkRed]{-1.414}{1.414}{sqrt(x*x + 1.5)}
					\psplot[linecolor=DarkGreen]{-1.414}{1.414}{-sqrt(x*x + 1.5)}
				}
				\psdot*[linecolor=DarkRed](-0.707,0)
				\psdot*[linecolor=DarkGreen](0.707,0)
				\psdot*[linecolor=MidnightBlue](0,0.707)
				\psdot*[linecolor=DarkOrange3](0,-0.707)
				\pcarc(-0.8,0.1)(-2,0.5)
				\uput[180](-2,0.5){\color{DarkRed} $-\sqrt{S^2}$}
				\pcarc(-0.2,0.7)(-2,1)
				\uput[180](-2,1){\color{MidnightBlue} $+\sqrt{-S^2}$}
				\pcarc(0.8,-0.1)(2,-0.5)
				\uput[0](2,-0.5){\color{DarkGreen} $+\sqrt{S^2}$}
				\pcarc(0.2,-0.7)(2,-1)
				\uput[0](2,-1){\color{DarkOrange3} $+\sqrt{-S^2}$}
			\end{pspicture}
			\caption{Minkowskimetrik}
			\label{fig:2013-07-03-5}
		\end{figure}
		%
		$\overline{OP}$, $\overline{OQ}$, $\overline{OR}$ sind gleichlang im Sinne der Minkoskimetrik ($\neq$ euklidischer Metrik)

		Für die Abstände gilt: $S^2 > 0$ raumartig, $S^2 = 0$ lichtartig, $S^2 < 0$ zeitartig.
\end{description}

$c$ ist die Grenzgeschwindigkeit, also hat die Weltlinie stets eine Steigung größer $1$.
%
\begin{figure}
	\centering
	\begin{pspicture}(-0.3,-0.3)(2,1)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(2.3,1.3)[{\color{DimGray} $x$},-90][{\color{DimGray} $ct$},180]
		\psline[linestyle=dashed](-0.3,-0.3)(1.3,1.3)
		\psline[linestyle=dotted,dotsep=1pt](-0.3,-0.15)(2.3,1.15)
		\psdots*[linecolor=MidnightBlue](0,0)(1,1)(2,1)
		\uput[-45](0,0){\color{MidnightBlue} $A$}
		\uput[160](1,1){\color{MidnightBlue} $B$}
		\uput[-45](2,1){\color{MidnightBlue} $C$}
	\end{pspicture}
	\caption{Illustration raumartiger Ereignisse.}
\end{figure}
%
Haben zwei Ereignisse $A$ und $B$ einen zeitartigen Abstand, so lässt sich stets eine Weltlinie durchlegen, was bedeutet, dass die Ereignisse für ein und denselben materiellen Punkt zeitlich nacheinander eintreten können, d.h.\ es gibt ein Bezugssystem, in welchem sie am gleichen Ort nacheinander eintreten.

Sind zwei Ereignisse $A$ und $C$ raumartig, so gibt es stets ein Bezugssystem, in dem sie gleichzeitig an verschiedenen Orten stattfinden.

\begin{notice}
	In der Galilei-Newton-Welt hat der Begriff des Raum-Zeit-Abstandes keine physikalische Bedeutung. Es gibt dort nur einen absoluten, d.h. galileiinvarianten, zeitlichen Abstand.
\end{notice}

\begin{figure}[htb]
	\centering
	\begin{pspicture}(-0.3,-0.3)(3,3)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(3,3)[{\color{DimGray} $x$},-90][{\color{DimGray} $ct$},180]
		\rput(1,1.5){
			\psline[linecolor=MidnightBlue](-2;45)(2;45)
			\psline[linecolor=MidnightBlue](-2;135)(2;135)
			\psellipse[linecolor=MidnightBlue](0,0.5)(0.5,0.125)
			\psellipse[linecolor=MidnightBlue](0,-0.5)(0.5,0.125)
			\psdot*[linecolor=DarkOrange3](0,0)
			\uput{0.1}[-90](0,0){\color{DarkOrange3} $A$}
		}
		\rput(2.5,1.2){
			\psline[linecolor=MidnightBlue](-2;45)(2;45)
			\psline[linecolor=MidnightBlue](-2;135)(2;135)
			\psellipse[linecolor=MidnightBlue](0,0.5)(0.5,0.125)
			\psellipse[linecolor=MidnightBlue](0,-0.5)(0.5,0.125)
			\psdot*[linecolor=DarkOrange3](0,0)
			\uput{0.1}[-90](0,0){\color{DarkOrange3} $B$}
		}
		\uput[0](2,2.3){\color{DimGray} Zukunft}
		\uput[0](3.5,0.5){\color{DimGray} Vergangenheit}
	\end{pspicture}
	\caption{Kausale Struktur der Welt}
\end{figure}

$A$ und $B$ haben nur im Überlappungbereich ihrer Lichtkegel eine mögliche gemeinsame Vergangenheit bzw.\ Zukunft.

\paragraph{Zeitdilatation}

Vergleich von relativ zueinander bewegten gleichen Uhren.
%
\begin{figure}[htb]
	\centering
	\begin{pspicture}(-1,-0.5)(3,2.5)
		\rput(0,1.5){
			\cput(3,0.5){\color{DimGray} $\Sigma'$}
			\psaxes[yAxis=false,labels=none,ticks=none,arrows=->](0,0)(0,0)(3,0)[{\color{DimGray} $x'$},0][,0]
			\uput[180](0,0){\color{DimGray} $t'=0$}
			\uput[-90](0,0){\color{MidnightBlue} $O'$}
			\psdot*[linecolor=MidnightBlue](0,0)
			\rput(0,0.4){\psclock{0.2}{0}{0}}
			\psline[linecolor=DarkOrange3,arrows=->](0.3,0.4)(1,0.4)
			\uput[0](1,0.4){\color{DarkOrange3} $v$}
		}
		\rput(0,0){
			\cput(3,0.5){\color{DimGray} $\Sigma$}
			\psaxes[yAxis=false,labels=none,ticks=none,arrows=->](0,0)(0,0)(3,0)[{\color{DimGray} $x$},0][,0]
			\uput[180](0,0){\color{DimGray} $t=0$}
			\uput[-90](0,0){\color{MidnightBlue} $O$}
			\psdot*[linecolor=MidnightBlue](0,0)
			\rput(0,0.4){\psclock{0.2}{0}{0}}
			\psxTick(1.5){\color{DimGray} x_1}
			\rput(1.5,0.4){\psclock{0.2}{0}{0}}
		}
	\end{pspicture}
	\caption{Zeitdilatation}
\end{figure}
%
Die Uhr bei $x_1$ muss mit der in $O$ dadurch synchonisiert werden, dass sie von $O$ nach $x_1$ gerbracht wurde, dass von $O$ ein Lichtstrahl nach $x_1$ gesendet wurde und bei dessen Eintreffen auf die Zeit $x_1/c$ gestellt wurde. (Nicht zulässig: Uhren bei $O$ synchronisieren und dann nach $x_1$ bringen, da die Uhren beim Transport anders gehen könnten.
