% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 12.04.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-04-12}

\subsection{Zweiter Erfahrungssatz}

Es existieren zwei Ladungsverteilung $Q^{(1)}$ und $Q^{(2)}$. Beide Ladungsverteilungen erzeugen ein elektrisches und magnetisches Feld, also
%
\begin{equation*}
	\begin{aligned}
		Q^{(1)} &\to \bm{E}^{(1)}, \bm{B}^{(1)} \\
		Q^{(2)} &\to \bm{E}^{(1)}, \bm{B}^{(1)}
	\end{aligned}
\end{equation*}
%
Superponiert man die beiden Ladungsverteilungen, so erhält man auch eine Superposition der Felder.
%
\begin{equation*}
	Q^{(1)} \cup Q^{(2)} \xrightarrow{\text{Erfahrung}}
	\begin{cases}
		\bm{E}^{(1)}(\bm{r},t) + \bm{E}^{(2)}(\bm{r},t) \\
		\bm{B}^{(1)}(\bm{r},t) + \bm{B}^{(2)}(\bm{r},t)
	\end{cases}
\end{equation*}
%
Dieses Phänomen wird als lineare Superposition oder auch \acct{Superpositionsprinzip} bezeichnet.

\begin{notice}[Zusammenfassung der Feldbeschreibung:]
	Ladungen erzeugen im Raum einen >>Erregungszustand<<, der durch die zwei Vektorfelder $\bm{E}$ und $\bm{B}$ an jedem Punkt und zu jeder Zeit beschrieben werden kann.
	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(0,0)(5,2)
			\psccurve[linecolor=MidnightBlue,fillstyle=solid,fillcolor=white](0.5,0.5)(1.5,1)(2,1.3)(1.5,1.5)(1,1.4)(1,1.5)(0.5,1.2)(0,1.1)
			\rput(1,1){\color{MidnightBlue} $Q$}
			\pscurve(3,2)(3.2,1.5)(3.2,0)
			\pscurve(4,2)(4.2,1.5)(4.2,0)
			\pscurve(2.5,1.6)(3.2,1.5)(4.2,1.5)(4.5,1.6)
			\pscurve(2.5,0.6)(3.2,0.5)(4.2,0.5)(4.5,0.6)
			\rput(4.2,1.5){
				\psline[linecolor=MidnightBlue,arrows=->](0,0)(0.8;-80)
				\psline[linecolor=DarkOrange3,arrows=->](0,0)(0.8;10)
				\uput[10](0.8;-80){\color{MidnightBlue} $\bm{B}$}
				\uput[100](0.8;10){\color{DarkOrange3} $\bm{E}$}
				\psdot(0,0)
			}
		\end{pspicture}
		\caption{Die von der Ladungsverteilung $Q$ erzeugten Felder $\bm{E}$ und $\bm{B}$ bewirken eine Kraft $\bm{F}_\ell$ auf eine Testladung.}
	\end{figure}
\end{notice}

\begin{notice}[Vorraussetzung:]
	Eine Störung der felderzeugenden Ladung durch die Testladung muss vernachlässigbar sein.
\end{notice}

$\bm{E}$ und $\bm{B}$ sind auch vorhanden, wenn keine Testladung vorhanden ist, z.B.
%
\begin{equation*}
	\lim\limits_{q \to 0} \frac{\bm{F}}{q} \quad \text{für } \bm{v} = 0
\end{equation*}

\begin{example}
	Eine felderzeugende, ruhende Ladung $q_1$ erzeugt das elektrische Feld
	%
	\begin{align*}
		\bm{E}(\bm{r}) &= \frac{q_1}{|\bm{r} - \bm{r}_1|^3} (\bm{r} - \bm{r}_1)
	\end{align*}
	%
	Man sieht, dass dies proportional ist zu $1/r^2$ für $r \to \infty$. Da die Ladung ruht ist das Magnetfeld $\bm{B} = 0$. Eine Skizze ist in Abbildung~\ref{fig:1.4} zu sehen.

	\begin{figure}
		\centering
		\begin{pspicture}(-1,0)(2,1.5)
			\cnode*(0,1){2pt}{Q}
			\pnode(1,0){O}
			\pnode(2,1){E}
			
			\ncline[arrows=->]{O}{Q}\naput{\color{DimGray} $\bm{r}_1$}
			\ncline[arrows=->]{Q}{E}\naput{\color{DimGray} $\bm{r} - \bm{r}_1$}
			\ncline[arrows=->]{O}{E}\nbput{\color{DimGray} $\bm{r}$}
			\uput[180](Q){\color{DimGray} $q_1$}
		\end{pspicture}
		\caption{Eine ruhende Ladung $q_1$ bei $\bm{r}_1$ und eine Testladung am Punkt $\bm{r}$.}
		\label{fig:1.3}
	\end{figure}
\end{example}

\begin{example}
	Das \acct{Keplerproblem} für eine Testladung im elektrischen Feld kann formuliert werden mit
	%
	\begin{align*}
		m \ddot{\bm{r}} = q \bm{E}(\bm{r})
	\end{align*}
	%
	Siehe dazu auch Skizze~\ref{fig:1.3}.
	
	\begin{figure}
		\centering
		\begin{pspicture}(-1,-1)(1,1)
			\psdot(0,0)
			\uput[180](0,0){\color{DimGray} $q_1 > 0$}
			\foreach \i in {45, 135, 225, 315} {
				\psline[arrows=->](0.5;\i)(1;\i)
			}
		\end{pspicture}
		\caption{Das elektrische Feld einer Testladung.}
		\label{fig:1.4}
	\end{figure}
\end{example}

Bewegen wir eine Testladung $q$ mit der Geschwindigkeit $\bm{v}$ durch ein konstantes Magnetfeld $\bm{B} = (0,0,B)^\top$, so gilt für die Bewegung von $q$
%
\begin{align*}
	m \dot{\bm{v}} &= \frac{q}{c} \bm{v} \times \bm{B} && \left| \cdot \bm{v} \right. \\
	m \dot{\bm{v}} \cdot \bm{v} &= 0 \\
	\implies \frac{\mathrm{d}}{\mathrm{d}t} \left( \frac{m}{2} \bm{v}^2 \right) &= 0
\end{align*}
%
das bedeutet, dass das Magnetfeld keine Arbeit an der Ladung verrichtet. Weiterhin
%
\begin{align*}
	\dot{\bm{v}} = \omega_B \bm{v} \times \bm{e}_z \; , \quad \omega_B = \frac{q B}{m c} \;\text{ist die \acct{Zyklotronfrequenz}}
\end{align*}

\begin{figure}
	\centering
	\begin{pspicture}[Alpha=70,Beta=30](-1,-1)(2.3,2.3)
		\pstThreeDCoor[linecolor=DimGray,linewidth=1pt,xMax=2,yMax=2,zMax=2,xMin=-0.3,yMin=-0.3,zMin=-0.3,nameX=\color{DimGray}$x$,nameY=\color{DimGray}$y$,nameZ=\color{DimGray}$z$]
		\parametricplotThreeD[xPlotpoints=200,linecolor=MidnightBlue,plotstyle=curve,algebraic,arrows=->](2,20){0.5*cos(t) + 1 | 0.5*sin(t) + 1 | t/7}
		\pstThreeDCircle(1,1,0)(0.5,0,0)(0,0.5,0)
	\end{pspicture}
	\caption{Eine bewegte Ladung in einem Magnetfeld in $z$-Richtung beschreibt eine Schraubenlinie. Die Projektion dieser Schraubenlinie auf die $x,y$-Ebene ergibt einen Kreis.}
\end{figure}

\section{Elektrischer Fluss, Ladungsverteilung}

Es seien gegeben: Eine orientierbare Fläche $F$ (d.h.\ kein Möbiusband) und mit $\bm{V}(\bm{r},t)$ ein beliebiges Vektorfeld (sihe Abbildung~\ref{fig:1.4.5}).

Der \acct{Fluss $\Phi$} von $\bm{V}$ durch $F$ ist definiert als
%
\begin{align*}
	\Phi_F(\bm{V})
	&= \iint\limits_{F} \bm{V} \cdot \bm{n} \, \mathrm{d}f \\
	&= \int_{F} \bm{V} \cdot \mathrm{d}\bm{f}
\end{align*}

\begin{figure}
	\centering
	\begin{pspicture}(-3,-3)(2,1)
		\pstThreeDLine(0,0,0)(4,0,0)(4,3,0)(0,3,0)(0,0,0)
		\pstThreeDLine[
			linecolor=MidnightBlue,
			fillstyle=hlines,
			hatchcolor=MidnightBlue
		](1.5,2,0)(2.5,2,0)(2.5,1,0)(1.5,1,0)(1.5,2,0)
		\pstThreeDLine[
			linecolor=DarkOrange3,
			arrows=->
		](2,1.5,0)(2,1.5,2)
		\pstThreeDLine[arrows=->](2,1.5,0)(2,2.5,2)
		\pstThreeDLine[arrows=->](1,2.5,0)(1.4,2.8,2)
		\pstThreeDLine[arrows=->](3,1,0)(3.4,1.5,2)
		\pstThreeDLine[arrows=->](3.5,1.8,0)(3.5,2,2)
		\pstThreeDNode(2,1.5,1){A}
		\pstThreeDNode(2,2,1){B}
		\pstThreeDNode(2,1.5,2){n}
		\pstThreeDNode(4,3,0){F}
		\pstThreeDNode(2,2.5,2){V}
		\ncarc[linecolor=Purple,arrows=->]{A}{B}
		\uput[180](n){\color{DarkOrange3} $\bm{n}$}
		\uput[90](F){\color{DimGray} $F$}
		\uput[45](V){\color{DimGray} $\bm{V}$}
		\uput[90]([nodesep=0.1]{A}B){\color{Purple} $\vartheta$}
	\end{pspicture}
	\caption{Fluss eines Vektorfeldes $\bm{V}$ durch eine durch den Normalenvektor $\bm{n}$ orientierte Fläche $F$.}
\label{fig:1.4.5}
\end{figure}

\begin{notice}
	Mit elektrischem Fluss ist nicht elektrischer Strom gemeint.
\end{notice}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-2.5,-2)(6,2)
		\psccurve(-1.5,0)(0,-1.5)(2,-1.3)(5,-1.5)(5.5,0)(5,1.5)(2,1.3)(0,1.5)
		\pnode(0,0){O}
		\psdot(O)
		\pscircle(O){1}
		\psline(O)(1;-135)
		\rput{-60}(4,0){
			\psellipse[fillstyle=vlines](0,0)(0.8,0.5)
			\psline[linecolor=DarkOrange3,arrows=->](0,0)(1.3;90)
			\psline[arrows=->](0,0)(2;45)
			\psarcn[linecolor=Purple,arrows=->](0,0){1}{90}{45}
			\pnode(-0.8,0){A1}
			\pnode(0.8,0){A2}
			\pnode(1.3;90){n}
			\pnode(2;45){E}
			\pnode(0.8;67.5){theta}
		}
		\psline[linestyle=dotted,dotsep=1pt](O)(A1)
		\psline[linestyle=dotted,dotsep=1pt](O)(A2)
		\psline[linecolor=MidnightBlue]([nodesep=1]{A1}O)([nodesep=1]{A2}O)
		\uput[-45](0.5;-135){\color{DimGray} $1$}
		\uput[90](O){\color{DimGray} $q > 0$}
		\uput[-60](1;-20){\color{MidnightBlue} $\mathrm{d}\Omega$}
		\uput[90](n){\color{DarkOrange3} $\bm{n}$}
		\uput[-90](E){\color{DimGray} $\bm{E}$}
		\rput(theta){\color{Purple} $\vartheta$}
		\uput[60](A1){\color{DimGray} $\mathrm{d}f$}
		\uput[90](2,-1.3){\color{DimGray} $V$}
		\pcarc[arcangle=-20](1;120)(-1,1.5)
		\uput[160](-1,1.5){\color{DimGray} $S_2$}
		\pcarc[arcangle=20](0,-1.5)(-1.3,-1.3)
		\uput[180](-1.3,-1.3){\color{DimGray} $F = \partial V$}
	\end{pspicture}
	\caption{Projektion der Fläche $\mathrm{d}f$ auf die Einheitssphäre $S_2$ aus der Sicht von Beobachter $q$, für den Fall, dass $q$ im Volumen $V$ ist.}
\end{figure}

$\mathrm{d}\Omega$ ist das Raumwinkelelement unter dem $\mathrm{d}f$ auf der Einheitskugel $S_2$ von $q$ aus gesehen wird.
%
\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-2,-1.5)(6,1.5)
		\pnode(0,0){O}
		\psline[linestyle=dotted,dotsep=1pt](O)(4,0)
		\psdot(O)
		\pscircle[linestyle=dashed](O){1}
		\rput{-60}(4,0){
			\psline[linecolor=MidnightBlue](-0.8,0)(0.8,0)
			\psline[linecolor=DarkOrange3,arrows=->](0,0)(1.3;90)
			\psline[arrows=->](0,0)(2;60)
			\psarcn[linecolor=Purple,arrows=->](0,0){1}{90}{60}
			\pnode(-0.8,0){A1}
			\pnode(0.8,0){A2}
			\pnode(1.3;90){n}
			\pnode(2;60){E}
			\pnode(0.8;75){theta}
		}
		\psline[linestyle=dotted,dotsep=1pt](O)([nodesep=5]{A1}O)
		\psline[linestyle=dotted,dotsep=1pt](O)([nodesep=5]{A2}O)
		\psline[linecolor=MidnightBlue]([nodesep=1]{A1}O)([nodesep=1]{A2}O)
		\uput[90](O){\color{DimGray} $q > 0$}
		\uput[-60](1;-20){\color{MidnightBlue} $\mathrm{d}\Omega$}
		\uput[90](n){\color{DarkOrange3} $\bm{n}$}
		\uput[-90](E){\color{DimGray} $\bm{E}$}
		\rput(theta){\color{Purple} $\vartheta$}
		\uput[60](A1){\color{MidnightBlue} $\mathrm{d}f$}
		\pcarc[arcangle=-20](1;120)(-1,1.2)
		\uput[160](-1,1.2){\color{DimGray} $S_2$}
		\psline([nodesep=4.05]{A1}O)([nodesep=4.05]{A2}O)
		\uput[-90]([nodesep=4.05]{A2}O){\color{DimGray} $\mathrm{d}f \cos\vartheta$}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=2pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=90,
			nodesepA=-2pt,
			nodesepB=5pt
		](0,0)(1,0){\color{DimGray} $1$}
	\end{pspicture}
	\caption{Hilfskonstruktion zur Bestimmung des Raumwinkelelements $\mathrm{d}\Omega$ für den Fall, dass $q$ im Volumen $V$ ist.}
\end{figure}
%
\begin{align*}
	\mathrm{d}\Omega
	&= \frac{\mathrm{d}f \cos\vartheta}{r^2} \quad \text{für } 0 \leq \vartheta \leq \frac{\pi}{2} \\
	&= \frac{\mathrm{d}f (-\cos\vartheta)}{r^2} \quad \text{für } \pi \geq \vartheta \geq \frac{\pi}{2} \\
	\int_{\partial V} \mathrm{d}\bm{f} \cdot \bm{E}
	&= \int_{\partial V} \mathrm{d}f \, E \cdot \cos\vartheta
	= \int_{\partial V} \mathrm{d}f \frac{q}{r^2} \cos\vartheta \\
	&= \int_{S_2} \mathrm{d}\Omega \, q = 4 \pi q
\end{align*}

Der elektrische Fluss ist unabhängig von der Position der Quelle.

Es wird nun der Fall betrachtet, dass sich die Ladung $q$ nicht innerhalb des Volumens $V$ befindet, wie in Abbildung~\ref{fig:2013-04-12-1} illustriert.
%
\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-1.5,-2)(6,1.5)
		\pnode(0,0){O}
		\psline[linestyle=dashed](O)(3.3,0)
		\psline[linecolor=MidnightBlue,arrows=->](3.3,0)(4,0)
		\psline[linecolor=MidnightBlue,arrows=->](4.3,0)(5,0)
		\uput[-90](4,0){\color{MidnightBlue} $\bm{E}$}
		\uput[-90](5,0){\color{MidnightBlue} $\bm{E}$}
		\psdot(O)
		\pscircle[linestyle=dashed](O){1}
		\pnode(4,1){A1}
		\pnode(4,-1.5){A2}
		\psdots(A1)(A2)
		\rput(A1){\psline[arrows=->](0,0)(0.5;110)}
		\rput(A2){\psline[arrows=->](0,0)(0.5;-115)}

		\psccurve(4,1)(4.5,0.5)(4.3,0)(4.8,-0.5)(4.5,-1)(4,-1.5)(3.5,-1)(3.8,-0.5)(3.3,0)(3.5,0.5)

		\psline[linestyle=dotted,dotsep=1pt](O)([nodesep=5]{A1}O)
		\psline[linestyle=dotted,dotsep=1pt](O)([nodesep=5]{A2}O)
		\pcarc[linecolor=Purple,arcangle=15]([nodesep=1]{A1}O)([nodesep=1]{A2}O)
		\uput[90](O){\color{DimGray} $q > 0$}
		\uput[-60](1;-20){\color{Purple} $\mathrm{d}\Omega$}
		\pcarc[arcangle=-20](1.2,0.1)(1.8,1)
		\uput[90](1.8,1){\color{Purple} $\tilde{S}_2$}
		\pcarc[arcangle=-20](1;120)(-1,1.2)
		\uput[160](-1,1.2){\color{DimGray} $S_2$}

		\rput(4.3,0){
			\psline[linecolor=DarkOrange3,arrows=->](0,0)(1;45)
			\uput[-45](1;45){\color{DarkOrange3} $\mathrm{d}f$}
		}
		\rput(3.3,0){
			\psline[linecolor=DarkOrange3,arrows=->](0,0)(1;-135)
			\uput[135](1;-135){\color{DarkOrange3} $\mathrm{d}f$}
		}
		\uput[90](A2){\color{DimGray} $V$}
		\pcarc(3.5,-1)(3,-1.5)
		\pcarc(4.5,-1)(5,-1.5)
		\uput[180](3,-1.5){\color{DimGray} $(\partial V)_1$}
		\uput[0](5,-1.5){\color{DimGray} $(\partial V)_2$}
	\end{pspicture}
	\caption{Hilfskonstruktion zur Bestimmung des Raumwinkelelements $\mathrm{d}\Omega$ für den Fall, dass $q$ nicht im Volumen $V$ ist.}
	\label{fig:2013-04-12-1}
\end{figure}
%
\begin{align*}
	\partial V &= (\partial V)_1 + (\partial V)_2
\end{align*}

\begin{description}
	\item[$(\partial V)_2$:]
		\begin{itemalign}
			\bm{E} \cdot \mathrm{d}\bm{f} &= E \, \mathrm{d}f \underbrace{\cos\vartheta}_{\geq 0} = q \, \mathrm{d}\Omega \\
			\implies&{} \int_{(\partial V)_2} \bm{E} \cdot \mathrm{d}\bm{f} = \int_{\tilde{S}_2} q \, \mathrm{d}\Omega = q \cdot \frac{|\tilde{S}_2|}{|S_2|} |S_2|
		\end{itemalign}

	\item[$(\partial V)_1$:]
		\begin{itemalign}
			\bm{E} \cdot \mathrm{d}\bm{f} &= E \, \mathrm{d}f \underbrace{\cos\vartheta}_{< 0} = - q \, \mathrm{d}\Omega \\
			\implies &{} \int_{(\partial V)_1} \bm{E} \cdot \mathrm{d}\bm{f} = - \int_{\tilde{S}_2} q \, \mathrm{d}\Omega = - q \cdot \frac{|\tilde{S}_2|}{|S_2|} |S_2| \\
			\implies &{}\int_{\partial V} \bm{E} \cdot \mathrm{d}\bm{f} = 0
		\end{itemalign}
\end{description}

\begin{notice}[Vorraussetzung:]
	Geometrie und Coulombgesetz
	%
	\begin{align*}
		\int_{\partial V} \bm{E} \cdot \mathrm{d}\bm{f}
		&=
		\begin{cases}
			4 \pi q & \text{, falls $q$ in $V$}       \\
			0       & \text{, falls $q$ nicht in $V$}
		\end{cases}
	\end{align*}
	%
	Alle Überlegungen leben davon, dass sich $V$ ohne Selbstüberschneidung auf $S_2$ projizieren lässt. Siehe dazu auch Abbildung~\ref{fig:2013-04-12-2}.

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-2,-1.5)(6,1.5)
			\pnode(0,0){O}
			\psline[linestyle=dotted,dotsep=1pt,arrows=->](O)(5,0)
			\psdot(O)
			\pscircle[linestyle=dashed](O){0.5}
			\uput[180](O){\color{DimGray} $q$}
			\psccurve[linecolor=MidnightBlue](-0.6,0)(0,-0.7)(1,-0.6)(2,-1)(4,0)(3,1.5)(2,-0.5)(0,0.6)
			\psline[linestyle=dotted,dotsep=1pt,linecolor=Purple](2,-1)(2,-0.5)
			\begin{psclip}{\pspolygon[linestyle=none](0,0)(5,1)(5,-1)}
				\psccurve[linecolor=DarkOrange3](-0.6,0)(0,-0.7)(1,-0.6)(2,-1)(4,0)(3,1.5)(2,-0.5)(0,0.6)
			\end{psclip}
			\psline(5,1)(0,0)(5,-1)
			\pcarc(0,-0.7)(-0.5,-1)\uput[180](-0.5,-1){\color{MidnightBlue} $\partial V$}
			\pcarc(2,-0.7)(1.5,-1.3)\uput[180](1.5,-1.3){\color{Purple} $F$}
			\pcarc(2,-0.8)(2.5,-1.3)\uput[0](2.5,-1.3){\color{Purple} $-F$}
			\uput[140](1,-0.6){\color{MidnightBlue} $V_1$}
			\uput[-90](3,1.5){\color{MidnightBlue} $V_2$}
			\uput[-45](5,0){\color{DimGray} $\bm{E}$}
			\pcarc(4,0.2)(1.5,1)
			\pcarc(2.4,0.2)(1.5,1)
			\uput{0.1}[150](1.5,1){\color{DimGray} $\circledast$}
		\end{pspicture}
		\caption{$\circledast$: Diese Beträge zu $\int_{\partial V} \bm{E} \cdot \mathrm{d}\bm{f}$ heben sich auf.}
		\label{fig:2013-04-12-2}
	\end{figure}
\end{notice}

\begin{notice}
	\begin{align*}
		\int_{\partial V} \bm{E} \cdot \mathrm{d}\bm{f}
		&=
		\int_{\partial (V_1)} \bm{E} \cdot \mathrm{d}\bm{f}
		+
		\underbrace{
			\int_{\partial (V_2)} \bm{E} \cdot \mathrm{d}\bm{f}
		}_{=0}
		=
		\int_{\partial (V_1)} \bm{E} \cdot \mathrm{d}\bm{f}
	\end{align*}
	%
	da
	%
	\begin{align*}
		\int_{F} \bm{E} \cdot \mathrm{d}\bm{f}
		&=
		- \int_{-F} \bm{E} \cdot \mathrm{d}\bm{f}
	\end{align*}
	%
	denn $(\mathrm{d}\bm{f})_1 = - (\mathrm{d}\bm{f})_2$, da $\mathrm{d}\bm{f}$ stets aus dem Volumen hinausweist.

	\paragraph{Für mehrere Ladungen:} Hier gilt das Superpositionsprinzip.
	%
	\begin{align*}
		\int_{\partial V} \bm{E} \cdot \mathrm{d}\bm{f}
		&=
		4 \pi \sum\limits_{j \in V} q_j
	\end{align*}
	%
	Diese Darstellung ist unabhängig von der Form von $\partial V$. Siehe Abbildung~\ref{fig:2013-04-12-2}.

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-1,-0.8)(4,1.8)
			\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
			\rput(3,1){
				\psline[linecolor=DarkOrange3,arrows=->](0,0)(1;35)
				\uput[-45](1;35){\color{DarkOrange3} $\bm{n}$}
			}
			\rput(2,0){\color{DimGray} $V$}
			\pcarc(1,0)(0,-0.5)
			\uput[180](0,-0.5){\color{MidnightBlue} $\partial V$}
			\psdots(-0.9,-0.5)
				(2.9,-0.6)
				(0.9,0.6)
				(1.7,0.1)
				(2.6,1.5)
				(0.9,-0.4)
				(0.2,0.7)
				(0.6,0.8)
				(2.6,-0.1)
				(-0.4,1.6)
				(2.0,-0.3)
				(2.5,1.0)
				(0.4,-0.7)
				(0.8,1.0)
				(2.2,1.5)
				(1.9,0.4)
				(1.6,1.6)
				(1.1,0.6)
				(-0.8,0.4)
				(3.4,1.6)
		\end{pspicture}
		\caption{Die Punkte symbolisieren Punktladungen $q_j$ innerhalb eines Volumens $V$}
		\label{fig:2013-04-12-2}
	\end{figure}
\end{notice}

Dieses \acct*{Gaußsche Gesetz} \index{Gaußsches Gesetz} des elektrischen Feldes gilt erfahrungsgemäß auch für zeitabhängige Felder, d.h.\ für beliebig bewegte Ladungen im stationären $V$ und $\partial V = F$. Dies stellt bereits eine der Maxwellschen Gleichungen dar.

Es sind auch die Fälle abgedeckt, dass Ladungen hinein- oder herausfließen.

\paragraph{Kontinuierliche Ladungsverteilungen:}
%
\begin{align*}
	\varrho(\bm{r},t) \, \Delta V \hateq \text{Ladung in $\Delta V$}
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,0)(4,2)
		\pstThreeDBox[linecolor=MidnightBlue](-3,1,1)(1,0,0)(0,1,0)(0,0,0.5)
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](0,0,0)(-2,1.5,1.25)
		\pstThreeDDot(0,0,0)
		\pstThreeDNode(-1,0.75,0.625){r}
		\pstThreeDNode(-3,2,1.5){V}
		\uput[130](r){\color{DarkOrange3} $\bm{r}$}
		\uput[-30](V){\color{MidnightBlue} $\Delta V$}
	\end{pspicture}
	\caption{Volumenelement $\Delta V$ mit Ortsvektor $\bm{r}$.}
\end{figure}

Nach Gauß gilt:
%
\begin{align*}
	\int_{\partial V} \bm{E}(\bm{r},t) \, \mathrm{d}\bm{f}
	=
	4 \pi \int_{V} \varrho(\bm{r},t) \, \mathrm{d}V
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,0)(3,2)
		\foreach \x/\y/\z in {-2/1.5/1.25, -2/0/1, -2/2/2, -2/2/1, -2/1/0} {
			\pstThreeDDot[linecolor=MidnightBlue,fillcolor=MidnightBlue](\x,\y,\z)
		}
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](0,0,0)(-2,1.5,1.25)
		\pstThreeDDot(0,0,0)
		\pstThreeDNode(-2,1.5,1.25){q}
		\pstThreeDNode(-1,0.75,0.625){r}
		\pstThreeDNode(-3,2,1.5){V}
		\uput[130](r){\color{DarkOrange3} $\bm{r}_i(t)$}
		\uput[0](q){\color{MidnightBlue} $q_i(t)$}
	\end{pspicture}
	\caption{Ladungsdichte bestehend aus Punktladungen $q_i$.}
\end{figure}

Man kann diskrete Punktladungen als singuläre Dichte beschreiben.
%
\begin{align*}
	\varrho(\bm{r},t)
	&=
	\sum\limits_{i} q_i \, \delta(\bm{r} - \bm{r}_i(t)) \\
	\delta(\bm{r} - \bm{r}_i(t))
	&=
	\delta(x-x_i) \delta(y-y_i) \delta(z-z_i)
\end{align*}

\paragraph{Statische Ladungen und Superposition}
%
\begin{align*}
	\bm{E}(\bm{r}) = \sum\limits_{i \in V} q_i \frac{\bm{r} - \bm{r}_i}{|\bm{r} - \bm{r}_i|^3}
	\quad \xrightarrow{\text{kont.}} \quad
	\int_{V} \varrho(\bm{r}') \frac{\bm{r} - \bm{r}_i}{|\bm{r} - \bm{r}_i|^3} \, \mathrm{d}V
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,-1)(4,1.5)
		\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
		\foreach \x/\y in {
			1.4/1.1, 1.2/1.4, 1.6/0.4, 1.4/0.1, 2.0/0.7, 0.8/1.2, 0.6/0.1, 2.7/0.3, 1.8/0.6, 0.2/0.7, 1.6/0.5, 0.5/0.3, 2.0/-0.3, 1.1/0.1, 2.8/0.8, 2.3/0.5, 0.8/0.4
		} {
			\psdots*[linecolor=MidnightBlue](\x,\y)
		}
		\rput(1.2,0.6){\color{MidnightBlue} $V$}
		\pcline[linecolor=DarkOrange3,arrows=->](3,-1)(2,-0.3)
		\naput{\color{DarkOrange3} $\bm{r}_i$}
		\pcline[arrows=->](3,-1)(4,0)
		\nbput{\color{DimGray} $\bm{r}$}
		\psdot(3,-1)
		\uput[120](2,-0.3){\color{MidnightBlue} $q_i$}
	\end{pspicture}
	\caption{Statische Ladungen in einem Volumen $V$.}
\end{figure}

\begin{description}
	\item[$\varrho(\bm{r})$:] Raumladungsdichte
	\item[$\sigma(\bm{r})$:] Flächenladungsdichte
		\[ \bm{E}(\bm{r}) = \int_{F} \sigma(\bm{r}') \frac{\bm{r} - \bm{r}_i}{|\bm{r} - \bm{r}_i|^3} \, \mathrm{d}^2 r' \]
	\item[$\eta(\bm{r})$:] Linienladungsdichte
\end{description}

In das Gaußsche Gesetz (im statischen Fall) ging ein:
%
\begin{itemize}
	\item $\bm{F} \sim 1/r^2$ und Zentralkraft
	\item lineare Superposition
\end{itemize}
%
Das gilt auch für (statische) Gravitationsfelder ($\varrho \hateq \text{Massendichte}$).

\begin{notice}
	Das Gaußsche Gesetz legt im Allgemeinen das Feld $\bm{E}$ nicht fest (außer in gewissen symmetrischen Fällen)
	%
	\begin{align*}
		\int_{\partial V} \bm{E} \cdot \mathrm{d}\bm{f}
	\end{align*}
	%
	legt nur die >>Quellen<< des Feldes fest, nicht jedoch die >>Wirbel<< ($\rot \bm{E}$)
\end{notice}
