% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 10.04.2013
% TeX: Jan

\renewcommand{\printfile}{2013-04-10}

In diesem Kapitel werden die empirischen Grundlagen der Maxwellschen Gleichungen, sowie mathematische Konzepte der Feldbeschreibung vermittelt.

\section{Lorentzkraft und Superpositionsprinzip}

Zu Beginn werden zwei fundamentale Erfahrungssätze aufgegriffen und interpretiert. Der erste Erfahrungssatz beschreibt die \acct{Lorentzkraft}, der zweite das \acct{Superpositionsprinzip}.

\subsection{Erster Erfahrungssatz}

\begin{figure}
	\centering
	\begin{pspicture}(0,0)(6,2)
		\psline[arrows=->](0,0)(2,2)
		\psccurve[linecolor=MidnightBlue,fillstyle=solid,fillcolor=white](0.5,0.5)(1.5,1)(2,1.3)(1.5,1.5)(1,1.4)(1,1.5)(0.5,1.2)(0,1.1)
		\rput(1.0,1.0){\color{MidnightBlue} $Q$}

		\psarc(3,2){1}{-90}{0}
		\rput(3,2){
			\cnode*(1;-60){2pt}{A}
		}
		\pnode(3,0){O}
		\pnode(5,2){E}
		\uput[120](A){\color{DimGray} $q$}
		\ncline[linecolor=MidnightBlue,arrows=->]{O}{A}
		\ncline[linecolor=DarkOrange3,arrows=->]{A}{E}
		\uput[0]([nodesep=1]{O}A){\color{MidnightBlue} $\bm{r}(t)$}
		\uput[0]([nodesep=1]{A}E){\color{DarkOrange3} $\bm{v}(t) = \dot{\bm{r}}(t)$}
	\end{pspicture}
	\caption{\textbf{Links:} Eine Ladungsverteilung der Gesamtladung $Q$. \textbf{Rechts:} Gleichförmige Bewegung einer Ladung auf der Trajektorie $\bm{r}(t)$.}
\end{figure}

Ladungen in beliebiger Bewegung erzeugen ein elektrisches Feld $\bm{E}$ und ein magnetisches Feld $\bm{B}$. Eine Testladung $q$ spürt diese Felder. $\bm{E}$ und $\bm{B}$ bestimmen die Bewegung der Testladung gemäß der \acct{Lorentzkraft}:
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} \bm{p} = \frac{\mathrm{d}}{\mathrm{d}t} \left(  \frac{m \cdot \bm{v}(t)}{\sqrt{1 - \left( \frac{\bm{v}(t)}{c} \right)^2}} \right)
	= \bm{F}_L \left( \bm{r}(t),t \right) 
	=q \left[ \bm{E}\left( \bm{r}(t),t \right) + \frac{1}{c} \, \bm{v}(t) \times \bm{B}\left( \bm{r}(t),t \right) \right]
\end{align*}
 
\begin{notice} Erinnerung zum \acct{Kreuzprodukt}.
	%
	\begin{align*}
		\bm{a} \times \bm{b} = \bm{c} \qquad 
		\bm{a} \times \bm{b} = -\bm{b} \times \bm{a} \qquad
		\bm{a} \cdot \bm{b} \cdot \sin\varphi = \bm{c} \qquad
		c_{\alpha} = \varepsilon_{\alpha \beta \gamma} \, a_{\beta} b_{\gamma}
	\end{align*}
	%
	Eine kleine Skizze dazu findet sich in Abbildung~\ref{fig:1.1}.
\end{notice}

\begin{figure}
	\centering
	\psset{Alpha=80,Beta=30}
	\begin{pspicture}(-1.2,-1.5)(2.5,2)
		\pstThreeDLine[arrows=->](0,0,0)(2,0,0)
		\pstThreeDLine[arrows=->](0,0,0)(0,2,0)
		\pstThreeDLine[arrows=->](0,0,0)(0,0,2)
		\pstThreeDLine[linestyle=dotted](0,0,0)(2,2,0)
		\pstThreeDLine[linestyle=dotted](2,0,0)(2,2,0)
		\pstThreeDLine[linestyle=dotted](0,2,0)(2,2,0)
		\pstThreeDNode(1.5,0,0){A}
		\pstThreeDNode(0,1.5,0){B}
		\ncarc[linecolor=MidnightBlue,arcangle=30]{B}{A}
		\pstThreeDPut(2,-0.3,0){\color{DimGray} $\bm{a}$}
		\pstThreeDPut(-0.2,2.2,0){\color{DimGray} $\bm{b}$}
		\pstThreeDPut(0,0.3,2){\color{DimGray} $\bm{c}$}
		\pstThreeDPut(0.5,0.5,0){\color{MidnightBlue} $\varphi$}
	\end{pspicture}
	\caption{Skizze zum Kreuzprodukt.}
	\label{fig:1.1}
\end{figure}

