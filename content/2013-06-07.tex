% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 07.06.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-06-07}

\begin{figure}
	\centering
	\begin{pspicture}(-1.5,-1)(1.5,1)
		\psframe[linestyle=none,fillstyle=hlines](-0.2,-1)(0,1)
		\psline(0,-1)(0,1)
		\cnode*(0.5,-0.5){2pt}{r'}
		\cnode*(0.7,0.6){2pt}{r}
		\cnode*(-0.7,-0.3){2pt}{r''}
		\ncline{r}{r'}
		\ncline{r}{r''}
		\uput[0](r){\color{DimGray} $\bm{r}$}
		\uput[0](r'){\color{DimGray} $\bm{r}'$}
		\uput[180](r''){\color{DimGray} $\bm{r}''$}
	\end{pspicture}
	\caption{Führt man $\bm{r}$ und $\bm{r}'$ nahe zusammen, so explodiert der Nenner in $1/|\bm{r}-\bm{r}'|$. Ebenso erhalten wir eine Singularität, wenn wir den Punkt $\bm{r}'$ und den Spiegelpunkt zusammenführen.}
\end{figure}

\section{Elektromagnetische Wellen im Vakuum}

Wir beschäftigen uns im Folgenden mit zeitabhängigen Feldern $\bm{E}(\bm{r},t)$ und $\bm{B}(\bm{r},t)$ außerhalb des Gebietes felderzeugender Quellen und Wirbel, vergleiche auch Abbildung~\ref{fig:2.5.1}.

\begin{figure}
	\centering
	\begin{pspicture}(-1,-0.5)(1.5,1.5)
		\pstThreeDCoor[
			coorType=2,
			linecolor=DimGray,
			xMax=1,yMax=1,zMax=1,
			xMin=0,yMin=0,zMin=0,
			nameX={},nameY={},nameZ={}
		]
		\uput[-90](0,0){\color{DimGray} $0$}
		\pcarc[linecolor=DarkOrange3,arcangle=-20,arrows=->](1.5,0.7)(-0.7,1.3)
		\pcarc[linecolor=MidnightBlue,arcangle=20,arrows=->](-0.7,1.5)(1.4,0.5)
		\uput[-90](-0.7,1.3){\color{DarkOrange3} $\bm{B}$}
		\uput[-90](1.4,0.5){\color{MidnightBlue} $\bm{E}$}
	\end{pspicture}
	\caption{$\bm{E}$- und $\bm{B}$-Feld außerhalb felderzeugender Wirbel und Quellen.}
	\label{fig:2.5.1}
\end{figure}

Die Maxwellschen Gleichungen lauten im Vakuum
%
\begin{align*}
	\div \bm{E} &= 0 \\
	\div \bm{B} &= 0 \\
	\rot \bm{E} &= - \frac{1}{c} \partial_t \bm{B} \\
	\rot \bm{B} &= \frac{1}{c} \partial_t \bm{E}
\end{align*}
%
Auf die dritte dieser Gleichungen wenden wir die Rotation an
%
\begin{align*}
	\rot \rot \bm{E} &= - \frac{1}{c} \partial_t \bm{B} = - \frac{1}{c^2} \partial_t^2 \bm{E}
\end{align*}
%
wir verwenden die Identität
%
\begin{align*}
	[\rot \rot \bm{E}]_{\alpha}
	&= \left[ \nabla \times \left( \nabla \times \bm{E} \right) \right]_\alpha \\
	&= \varepsilon_{\alpha\beta\gamma} \partial_{\beta} \varepsilon_{\gamma\mu\nu} \partial_{\mu} E_{\nu} \\
	&= \varepsilon_{\alpha\beta\gamma} \varepsilon_{\gamma\mu\nu} \partial_{\beta} \partial_{\mu} E_{\nu} \\
	&= \left[ \delta_{\alpha\mu} \delta_{\beta\nu} - \delta_{\alpha\nu} \delta_{\beta\mu} \right] \partial_{\beta} \partial_{\mu} E_{\nu} \\
	&= \partial_{\beta} \partial_{\alpha} E_{\beta} - \partial_{\beta} \partial_{\beta} E_{\alpha} \\
	&= \partial_{\alpha} \div \bm{E} - \nabla^2 E_{\alpha} \\
	&= \bigl[ \underbrace{\grad \div}_{=0} \bm{E} - \nabla^2 \bm{E} \bigr]_{\alpha} = [ - \nabla^2 \bm{E}]_{\alpha}
\end{align*}

Wendet man die Rotation auf die vierte Maxwell'sche Gleichung an, so erhält man
%
\begin{align*}
	\rot \rot \bm{B} = \frac{1}{c} \partial_t \rot \bm{E} = - \frac{1}{c^2} \partial_t^2 \bm{B}
\end{align*}
%
Wir erhalten also die beiden Gleichungen
%
\begin{align*}
	\left( \nabla^2 - \frac{1}{c^2} \partial_t^2 \right) \bm{E} &= 0 \\
	\underbrace{\left( \nabla^2 - \frac{1}{c^2} \partial_t^2 \right)}_{\eqcolon \quabla} \bm{B} &= 0
\end{align*}

Jede kartesische Komponente von $\bm{E}$ und $\bm{B}$ erfüllt die Wellengleichung.

\begin{notice}
	Der Maxwellsche Verschiebungsstrom geht hier wesentlich ein.
\end{notice}

\paragraph{Wellengleichung:} Die skalare Wellengleichung lautet
%
\begin{align*}
	\left( \nabla^2 - \frac{1}{v^2} \partial_t^2 \right) \psi &= 0
\end{align*}
%
hierbei ist $v = c$ und $\psi(\bm{r},t) \in \{ E_x, \dotsc, B_z \}$.

Zunächst betrachten wir die spezielle Lösung, anschließend die allgemeine Lösung durch Superposition. Die Wellengleichung ist eine lineare Differentialgleichung.
%
\begin{align*}
	\psi(\bm{r},t) = \psi_0 \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - \omega t)}
\end{align*}
%
Einsetzen in die Wellengleichung
%
\begin{align*}
	\left( -k^2 + \frac{\omega^2}{c^2} \right) \psi_0 &= 0 \\
	\implies \omega &= \pm c k && \text{\acct{Dispersionsgesetz}}
\end{align*}
%
Wir haben also das Dispersionsgesetz für elektromagnetische Wellen im Vakuum erhalten.

\paragraph{Monochromatische Wellen:}
Wenn $\psi(\bm{r},t)$ eine Lösung der Wellengleichung ist, dann ist auch $\psi^*(\bm{r},t)$ mit
%
\begin{align*}
	\psi^*(\bm{r},t) = \psi_0^* \mathrm{e}^{-\mathrm{i} ( \bm{k} \cdot \bm{r} - \omega t)}
\end{align*}
%
eine Lösung.

Der feste Vektor $\bm{k}$ ist nach Richtung und Betrag beliebig. Im Folgenden betrachten wir eine nach rechts laufenden Welle, also
%
\begin{align*}
	\omega = + c |\bm{k}|
\end{align*}
%
Damit gelten einige Zusammenhänge
%
\begin{align*}
	\bm{k} &= k \cdot \hat{\bm{k}} && \text{Wellenvektor}, k = |\bm{k}| > 0 \\
	k &\eqcolon \frac{2 \pi}{\lambda} && \text{$\lambda$ hat die Rolle einer Wellenlänge} \\
	\omega &\eqcolon \frac{2 \pi}{T} && \text{Kreisfrequenz, $T$: Periode} \\
	\nu &\coloneq \frac{1}{T} = \frac{\omega}{2 \pi} && \text{Frequenz}
\end{align*}

Das Disperionsgesetz lautet nun
%
\begin{align*}
	\lambda = c T \quad \text{bzw.} \quad \frac{\lambda}{T} = c \quad \text{bzw.} \quad \nu \lambda = c
\end{align*}
%
Die Amplitude der Welle ist komplex: $\psi_0 = |\psi_0| \mathrm{e}^{\mathrm{i} \varphi_0}$, $\varphi_0 \in \mathbb{R}$.
%
\begin{gather*}
	\begin{aligned}
		\psi(\bm{r},t) &= |\psi_0| \mathrm{e}^{\mathrm{i} \varphi(\bm{r},t)} \; , \quad \varphi(\bm{r},t) = \bm{k} \cdot \bm{r} - \omega t + \varphi_0 \\
		\varphi(\bm{r},t) &= 2 \pi \left( \frac{\hat{\bm{k}} \cdot \bm{r}}{\lambda} - \frac{t}{T} \right) + \varphi_0
	\end{aligned} \\
	\begin{rcases}
		\psi(\bm{r} + n \lambda \hat{\bm{k}},t) = \psi(\bm{r},t) \\
		\psi(\bm{r},t + n T) = \psi(\bm{r},t) \\
	\end{rcases}
	\text{für} \, n \in \mathbb{Z}
\end{gather*}
%
\begin{figure}
	\centering
	\begin{pspicture}(-0.5,-1)(3.5,2.5)
		\pstThreeDCoor[
			coorType=2,
			linecolor=DimGray,
			xMax=1,yMax=1,zMax=1,
			xMin=0,yMin=0,zMin=0,
			nameX={},nameY={},nameZ={}
		]
		\rput{30}(1.5;30){
			\pnode(0.0,0){O}
			\pnode(0.0,-1){R}
			\pnode(0.0,1){A}
			\pnode(0.5,1){B}
			\pnode(1.0,1){C}
			\psline(0.0,-2)(0.0,1)
			\psline(0.5,-2)(0.5,1)
			\psline(1.0,-2)(1.0,1)
			\psline[linestyle=dotted,dotsep=1pt](0,0)(1,0)
			\psarc(0.0,0){0.2}{90}{180}\rput(0.0,0){\psdot[dotsize=1pt](0.1;135)}
			\psarc(0.5,0){0.2}{90}{180}\rput(0.5,0){\psdot[dotsize=1pt](0.1;135)}
			\psarc(1.0,0){0.2}{90}{180}\rput(1.0,0){\psdot[dotsize=1pt](0.1;135)}
		}
		\ncline[arrows=<->]{A}{B}\naput{\color{DimGray} $\lambda$}
		\ncline[arrows=<->]{B}{C}\naput{\color{DimGray} $\lambda$}
		\psline[linecolor=DarkOrange3,arrows=->](0,0)(O)
		\psline[linecolor=MidnightBlue,arrows=->](0,0)(R)
		\uput[-110](O){\color{DarkOrange3} $\hat{\bm{k}}$}
		\uput[-140](R){\color{MidnightBlue} $\bm{r}$}
	\end{pspicture}
	\caption{Ebenen konstanter, gleicher Phase (periodisch im Raum, Momentaufnahme). Die Konstante der Phase ändert sich von Ebene zu Ebene um $2 \pi$. $\mathrm{e}^{\mathrm{i} \varphi}$ ist für alle Ebenen gleich.}
\end{figure}
%
Wähle nun $t$ fest.
%
\begin{align*}
	\{ \bm{r} \in \mathbb{R}^3 | \varphi(\bm{r},t) = \mathrm{const} \}
	= \{ \bm{r} \in \mathbb{R}^3 | \hat{\bm{k}} \cdot \bm{r} = \mathrm{const} \}
\end{align*}
%
Diese Punktmenge entspricht einer Ebene mit Normalenvektor $\hat{\bm{k}}$.

Nun gilt: $t$ variiert, $\varphi$ ist fest.

\begin{notice}[Frage:]
	Wie muss sich ein Punkt $\bm{r}(t)$ im Raum bewegen, sodass sich die Phase an diesem Punkt nicht ändert?
\end{notice}

\begin{notice}[Antwort:]
	Er muss sich so bewegen, dass die Projektion der Geschwindigkeit auf $\hat{\bm{k}}$ konstant und gleich $c$ ist. Dazu
	%
	\begin{gather*}
		\varphi(t) \coloneq \varphi(\bm{r}(t),t)) = \frac{2 \pi}{\lambda} \left( \hat{\bm{k}} \cdot \bm{r}(t) - c t \right) \\
		\dot{\varphi} = 0 \implies \hat{\bm{k}} \cdot \dot{\bm{r}} = c
	\end{gather*}
	%
	Das heißt, dass $c$ die Phasengeschwindigkeit elektromagnetischer Wellen im Vakuum ist.
\end{notice}

\begin{notice}
	Die komplexe Lösung $\psi(\bm{r},t)$ steht für zwei lineare unabhängige relle Lösungen:
	%
	\begin{align*}
		\psi(\bm{r},t) \to
		\begin{cases}
			\Re \psi(\bm{r},t) = |\psi_0| \cos \varphi(\bm{r},t) \\
			\Im \psi(\bm{r},t) = |\psi_0| \sin \varphi(\bm{r},t)
		\end{cases}
	\end{align*}
	%
\end{notice}

Die Felder $\bm{E}$ und $\bm{B}$ sind naturgemäß reell. Dazu gilt folgende Konvention.
%
\begin{align*}
	\bm{E}(\bm{r},t) &= \Re \left[ \mathring{\bm{E}} \, \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - \omega t)} \right] \\
	\bm{B}(\bm{r},t) &= \Re \left[ \mathring{\bm{B}} \, \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - \omega t)} \right]
\end{align*}

\begin{notice}
	\begin{itemize}
		\item Die orts- und zeitunabhängigen Amplitudenvektoren $\mathring{\bm{E}}$ und $\mathring{\bm{B}}$ sind im Allgmeinen komplex, d.h.\ sie haben komplexe Komponenten.

		\item $\omega = \pm c |\bm{k}|$ $\implies$ $\varphi(\bm{r},t) = |\bm{k}| ( \bm{k} \cdot \bm{r} \mp c t ) + \varphi_0$.
			\begin{description}
				\item[$+$] Die Welle läuft in Richtung $\hat{\bm{k}}$
				\item[$-$] Die Welle läuft in Richtung $-\hat{\bm{k}}$
			\end{description}
	\end{itemize}
\end{notice}

Nicht jede Lösung der Wellengleichung ist auch eine Lösung der Maxwell'schen Gleichungen. Diese liefern zusätzliche Bedingungen.
%
\begin{align*}
	\div \bm{E} = 0 \implies \bm{k} \cdot \bm{E} = 0 \\
	\div \bm{B} = 0 \implies \bm{k} \cdot \bm{B} = 0
\end{align*}
%
weil 
%
\begin{align*}
	\div \bm{E} &= 0 \\
	\mathrm{i} \bm{k} \cdot \mathring{\bm{E}} &= 0 \\
	\bm{k} \cdot \bm{E} &= 0
\end{align*}
%
Daraus folgt insbesondere $\mathring{\bm{E}},\mathring{\bm{B}} \perp \bm{k}$. Es handelt sich also um \acct{transversale Wellen}.

\paragraph{Ferner:}
%
\begin{gather*}
	\begin{aligned}
		\rot \bm{E} &= - \frac{1}{c} \partial_t \bm{B} \\
		\implies \mathrm{i} \bm{k} \times \mathring{\bm{E}} &= \mathrm{i} \frac{\omega}{c} \mathring{\bm{B}} \\
		&= \mathrm{i} |\bm{k}| \mathring{\bm{B}}
	\end{aligned}
	\\
	\boxed{
		\begin{aligned}
			\hat{\bm{k}} \times \mathring{\bm{E}} &= \mathring{\bm{B}} \\
			|\mathring{\bm{E}}| &= |\mathring{\bm{B}}|
		\end{aligned}
	}
	\quad
	\boxed{
		\begin{aligned}
			\hat{\bm{k}} \times \bm{E} &= \bm{B} \\
			|\bm{E}| &= |\bm{B}|
		\end{aligned}
	}
\end{gather*}

\begin{figure}
	\centering
	\begin{pspicture}(-0.5,-0.5)(5,2.5)
		\pstThreeDCoor[
			coorType=2,
			linecolor=DimGray,
			xMax=1,yMax=1,zMax=1,
			xMin=0,yMin=0,zMin=0,
			nameX={},nameY={},nameZ={}
		]
		\rput{-10}(3,1){
			\pstThreeDLine(-0.5,-0.5,0)(2.5,-0.5,0)(2.5,2,0)(-0.5,2,0)(-0.5,-0.5,0)
			\pstThreeDNode(1,0.75,0){O}
			\pstThreeDNode(1,0.75,2){k}
			\pstThreeDNode(1,0.1,0){E}
			\pstThreeDNode(1.7,0.75,0){B}
			\pstThreeDNode(0.3,-0.3,0){L}
			\psline[arrows=->](O)(k)
			\psline[linecolor=MidnightBlue,arrows=->](O)(E)
			\psline[linecolor=DarkOrange3,arrows=->](O)(B)
		}
		\uput[180](E){\color{MidnightBlue} $\mathring{\bm{E}}$}
		\uput[-45](B){\color{DarkOrange3} $\mathring{\bm{B}}$}
		\uput[180](k){\color{DimGray} $\bm{k}$}
		\pcarc[arcangle=20](2,2)(L)
		\uput[180](2,2){\color{DimGray} $\varphi = \mathrm{const}$}
	\end{pspicture}
	\caption{$\grad_{\bm{r}} \varphi = \bm{k}$, d.h.\ dass $\bm{k}$ Normalenvektor zur Ebene $\varphi = \mathrm{const}$ ist.}
\end{figure}

$(\hat{\bm{k}},\mathring{\bm{E}},\mathring{\bm{B}})$ bilden ein Rechtssystem für Wellen, die sich in $\hat{\bm{k}}$-Richtung ausbreiten.

Betrachte Ebenen konstanter Phase, d.h.\ $\perp \bm{k}$.
%
\begin{align*}
	\mathring{\bm{E}} &= \mathring{E}_1 \bm{\varepsilon}_1 + \mathring{E}_2 \bm{\varepsilon}_2 \\
	\bm{\varepsilon}_i \cdot \bm{\varepsilon}_j &= \delta_{ij} \\
	\bm{\varepsilon}_{1,2} \cdot \bm{k} &= 0 \\
	\mathring{E}_{1,2} &= |\mathring{\bm{E}}_{1,2}| \mathrm{e}^{\mathrm{i} \alpha_{1,2}}
\end{align*}

\begin{figure}
	\centering
	\begin{pspicture}(-2.5,-0.5)(2.5,2)
		\psaxes[ticks=none,labels=none,arrows=->](0,0)(-2,-0.5)(2,1.5)[{\color{DimGray} $\bm{\varepsilon}_1$}, 0][{\color{DimGray} $\bm{\varepsilon}_2$}, 0]
		\psline[linecolor=MidnightBlue,arrows=->](0,0)(2;30)
		\psline[linecolor=DarkOrange3,arrows=->](0,0)(2;120)
		\psarc(0,0){0.2}{0}{90}\psarc(0,0){0.3}{0}{90}
		\psarc(0,0){0.5}{30}{120}\psarc(0,0){0.6}{30}{120}
		\uput[180](2;120){\color{DarkOrange3} $\mathring{\bm{B}}$}
		\uput[0](2;30){\color{MidnightBlue} $\mathring{\bm{E}}$}
		\pscircle(-1,0.3){4pt}
		\psdot[dotsize=1pt](-1,0.3)
		\uput[180](-1,0.3){\color{DimGray} $\bm{k}$}
	\end{pspicture}
	\caption{Das Rechtssystem.}
\end{figure}

Damit folgt
%
\begin{align*}
	\bm{E}(\bm{r},t)
	&= \Re \left[ \left( |\mathring{E}_1| \bm{\varepsilon}_1 \mathrm{e}^{\mathrm{i} \alpha_1} + |\mathring{E}_2| \bm{\varepsilon}_2 \mathrm{e}^{\mathrm{i} \alpha_2} \right) \mathrm{e}^{\mathrm{i} ( \bm{k} \cdot \bm{r} - \omega t)} \right] \\
	&= |\mathring{E}_1| \bm{\varepsilon}_1 \cos(\bm{k} \cdot \bm{r} - \omega t + \alpha_1) + |\mathring{E}_2| \bm{\varepsilon}_2 \cos(\bm{k} \cdot \bm{r} - \omega t + \alpha_2)
\end{align*}

\paragraph{Spezialfälle:} Betrachte
%
\begin{align*}
	\alpha_1 = \alpha_2 = \alpha \implies \bm{E}(\bm{r},t) = \left[ \bm{\varepsilon}_1 |\mathring{E}_1| + \bm{\varepsilon}_2 |\mathring{E}_2| \right] \cos(\underbrace{\bm{k} \cdot \bm{r} - \omega t + \alpha}_{\varphi(\bm{r},t)})
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.5,-1.5)(10,1.7)
		\rput(0,0){
			\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.5,-0.5)(1.5,1.5)[{\color{DimGray} $\bm{\varepsilon}_1$}, 0][{\color{DimGray} $\bm{\varepsilon}_2$}, 0]
			\psline[linecolor=MidnightBlue,arrows=->](1.5;30)
			\psarc(0,0){0.7}{0}{30}
			\pcarc[arcangle=20](0.4;15)(1,-0.5)
			\uput[0](1,-0.5){\color{DimGray} $\chi$}
			\uput[30](1.5;30){\color{MidnightBlue} $\bm{E}$}
		}
		\rput(4,0){
			\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.5,-0.5)(1.5,1.5)[{\color{DimGray} $\bm{\varepsilon}_1$}, 0][{\color{DimGray} $\bm{\varepsilon}_2$}, 0]
			\psline[linecolor=MidnightBlue,arrows=->](1;30)
			\psarc(0,0){0.7}{0}{30}
			\pcarc[arcangle=20](0.4;15)(1,-0.5)
			\uput[0](1,-0.5){\color{DimGray} $\chi$}
		}
		\rput(8,0){
			\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.5,-0.5)(1.5,1.5)[{\color{DimGray} $\bm{\varepsilon}_1$}, 0][{\color{DimGray} $\bm{\varepsilon}_2$}, 0]
			\psline[linecolor=MidnightBlue,arrows=->](-0.7;30)
			\psarc(0,0){0.5}{180}{210}
			\pcarc[arcangle=-20](-0.4;15)(1,-0.5)
			\uput[0](1,-0.5){\color{DimGray} $\chi$}
		}
		\psline[linecolor=DarkOrange3,arrows=->](0,-1)(9.5,-1)
		\uput[-90](5,-1){\color{DarkOrange3} $\varphi$}
	\end{pspicture}
	\caption{Veränderung von $\bm{E}$ mit $\varphi$. Dabei gilt $\tan \chi = |\mathring{\bm{E}}_2|/|\mathring{\bm{E}}_1|$.}
\end{figure}

\begin{align*}
	|\bm{E}| = |\mathring{\bm{E}}| |\cos\varphi| \neq \mathrm{const}
\end{align*}
%
Die Richtung von $\bm{E}$ ist konstant.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}[Alpha=70,Beta=30](-1.5,-1.8)(6,1.5)
		\pstThreeDLine[arrows=->](0,-1,0)(0,6,0)
		\pstThreeDNode(0,6,0){k}
		\pstThreeDPut(0,-0.5,0){
			\pstThreeDLine[arrows=->](0,0,0)(0.707,0,0.707)
			\pstThreeDLine[arrows=->](0,0,0)(-0.707,0,0.707)
			\pstThreeDNode(-0.707,0,0.707){eps1}
			\pstThreeDNode(0.707,0,0.707){eps2}
		}
		\pstThreeDNode(1,0,0){B}
		\pstThreeDNode(0,0,1){E}
		\parametricplotThreeD[
			xPlotpoints=200,plotstyle=curve,algebraic,linecolor=DarkOrange3
		](0,7.853981){cos(t) | 0.6*t | 0}
		\parametricplotThreeD[
			xPlotpoints=200,plotstyle=curve,algebraic,linecolor=MidnightBlue
		](0,7.853981){0 | 0.6*t | cos(t)}
		\parametricplotThreeD[
			yPlotpoints=20,arrows=->,algebraic,linecolor=DarkOrange3
		](0,1)(0,7.853981){t*cos(u) | 0.6*u | 0}
		\parametricplotThreeD[
			yPlotpoints=20,arrows=->,algebraic,linecolor=MidnightBlue
		](0,1)(0,7.853981){0 | 0.6*u | t*cos(u)}
		\uput[90](eps1){\color{DimGray} $\bm{\varepsilon}_1$}
		\uput[135](eps2){\color{DimGray} $\bm{\varepsilon}_2$}
		\uput[90](k){\color{DimGray} $\bm{k}$}
		\uput[45](E){\color{MidnightBlue} $\bm{E}$}
		\uput[-90](B){\color{DarkOrange3} $\bm{B}$}
	\end{pspicture}
	\caption{Eine lineare polarisierte Welle. Zunahme von $\varphi(\bm{r},t)$ entweder durch $t$ für festes $\bm{r}$ oder durch $\bm{r} \cdot \bm{k}$ für festes $t$.}
\end{figure}
