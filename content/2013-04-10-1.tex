% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 10.04.2013
% TeX: Jan

\renewcommand{\printfile}{2013-04-10}

\section{Historischer Überblick}

\begin{description}
	\item[11. Jhd. :] Kompass
	\item[1269 P. Peregrinus:] Magnetpole
	\item[1600 W. Gilbert:] ~
	\item[1609 Galilei:] Fallgesetze
	\item[1609-19 Kepler:] Planetengesetze
	\begin{enumerate}
		\item keine magnetische Ladung
		\item Reibungselektrizität
		\item Elektroskop
		\item Erde als Magnet
	\end{enumerate}
	\item[1629 N. Cabeo:] Anziehung und Abstoßung elektrisch geladener Körper
	\item[1665 Grimaldi:] Lichtbeugung
	\item[1666 Newton:] Gravitation
	\item[1667 R\o mer:] Messung von $c$
	\item[1690 Huygens:] Licht als Welle
	\item[1729 S. Gray:] Leiter, Isolatoren
	\item[1734 du Fay:] Zwei Arten von Ladungen (Glas, Harz)
	\item[1747 B. Franklin:]
	\begin{enumerate}
		\item $\pm$ Ladungen
		\item Erhaltungssatz von Ladungen
		\item $F= 0$ im Leiter
		\item Blitzableiter
	\end{enumerate}
	\item[1767 J. Priestly:] $F\propto \frac{1}{r^2}$
	\item[C.A. Coulomb:] Durch Messung: $F\propto \frac{q_1 q_2}{r^2}$
	\item[1791 L. Galvani:] Berührungselektrizität
	\item[1799 A. Volta:] Batterie als Stromquelle
	\item[Ch. Oersted:] Strom erzeugt Magnetfeld
	\item[1821-24 A.M. Ampère:]
	\begin{enumerate}
		\item Kräfte zwischen Strömen
		\item Hypothese: Magnetismus ist elektrischen Ursprungs
	\end{enumerate}
	\item[1826 G.S. Ohm:] Ohmsches Gesetz
	\item[1830-50 M. Faraday:]
	\begin{enumerate}
		\item Induktionsgesetz
		\item $\bm{E}$-,$\bm{B}$- Felder
		\item Dia- und Paramagnetismus 
	\end{enumerate}
	\item[1865 J.C. Maxwell:]
	\begin{enumerate}
		\item Endgültige Form der Feldgleichungen
		\item elektromagnetische Lichttheorie
	\end{enumerate}
	\item[1887 H. Hertz:] Nachweis der elektromagnetischen Wellen
	\item[1895 K.W. Röntgen:] Röntgenstrahlen
	\item[1895 H.A. Lorentz:] Maxwellsche Theorien für kondensierte Materie, Elektronentheorie
	\item[1905 A. Einstein:]
	\begin{enumerate}
		\item Prinzip $c = \mathrm{const}$
		\item spezielle Relativitätstheorie
	\end{enumerate}
	\item[1928:] Quantenelektrodynamik (QED)
\end{description}

% TeX: Marcel

\section{allgemeine Bezüge}

Aus der Mechanik ist uns das zweite Newtonsche Gesetz bekannt als
%
\begin{align*}
	\bm{F} = \bm{\dot{P}} = m \bm{\ddot{r}} 
\end{align*}
%
Dieses liefert jedoch keine Aussage über die Natur der Kräfte.

Das dritte Newtonsche Gesetz kann für die Gravitation wie folgt formuliert werden.
%
\begin{align*}
	\bm{F_2}  = - G \cdot \frac{m_1 \cdot m_2}{r^2} \cdot \frac{1}{\left| r \right|} = -\bm{F_1}
\end{align*}
%
Zur Illustration dieser Gleichung siehe auch Abbildung~\ref{fig:0.1}.

\begin{figure}
	\centering
	\begin{pspicture}(-0.5,-0.5)(5,2.5)
		\cnode*(0,0){2pt}{m1}
		\cnode*(4,2){2pt}{m2}
		\pnode(2,1){mi}
		\uput[-90](m1){\color{DimGray} $m_1$}
		\uput[0](m2){\color{DimGray} $m_2$}
		\ncline[linecolor=MidnightBlue,arrows=->]{m1}{m2}
		\uput[-45](mi){\color{MidnightBlue} $\bm{r}$}
		\psline[linecolor=DarkOrange3,arrows=->](0,0.3)(1.5,1.05)
		\psline[linecolor=DarkOrange3,arrows=->](4,2.3)(2.5,1.55)
		\uput[135](0.75,0.675){\color{DarkOrange3} $\bm{F}_1$}
		\uput[135](3.25,1.925){\color{DarkOrange3} $\bm{F}_2$}
	\end{pspicture}
	\caption{Illustration zum Gravitationsgesetz}
	\label{fig:0.1}
\end{figure}

Gegenstand der Vorlesung ist jedoch die Elektrodynamik. Dies ist die Theorie der elektromagnetischen Wechselwirkung.

Betrachten wir zunächst einmal die Hierachie der (bekannten) fundamentalen Kräfte. Eine Aufstellung findet sich in Tabelle~\ref{tab:0.1}.

\begin{table}[htpb]
	\centering
	\begin{tabularx}{\textwidth}{>{\raggedright\arraybackslash}X>{\raggedright\arraybackslash}Xcc}
		\toprule
		Wechselwirkung & Vorkommen & relative Stärke & Reichweite \\
		\midrule
		starke Wechselwirkung & Kernkräfte, Elementarteilchen & \num{1} -- \num{e-1} & $\SI{1e-15}{\m}$ \\
		Elektromagnetische Wechselwirkung & gewöhnliche Materie & \num{e-2} & $\infty$ \\
		Schwache Wechselwirkung & Elementarteilchen & \num{e-5} & $\ll \SI{1e-15}{\m}$ \\
		Gravitation & Astronomie & \num{e-4} & $\infty$ \\
		\bottomrule
	\end{tabularx}
	\caption{Die vier Grundkräfte.}
	\label{tab:0.1}
\end{table}

Als Vermittler der elektromagnetischen Wechselwirkung fungieren die elektrischen Ladungen. Betrachen wir nun näher das Konzept der Ladungen.

\subsection{ruhende (Quell) - Ladung}

Die Wechselwirkung zwischen zwei Ladungen kann durch das \acct{Coulomb-Gesetz} beschrieben werden.
%
\begin{align}
	\bm{F_2} = f \cdot \frac{q_1 \cdot q_2}{r^2} \cdot \frac{1}{r}	= - \bm{F_1} \quad f > 0
	\label{eq:0.0}
\end{align}
%
Wie man bereits aus der Gleichung herauslesen kann ergeben sich zwei Fälle
%
\begin{itemize}
	\item $q_1 \cdot q_2 > 0$: Abstoßung

	\item $q_1 \cdot q_2 < 0$: Anziehung
\end{itemize}
%
Der Fall $q_1 \cdot q_2 = 0$ kann natürlich auch auftreten ist jedoch etwas trivial.

Eine Illustration zum Coulomb-Gesetz findet sich in Abbildung~\ref{fig:0.2}.

\begin{figure}
	\centering
	\begin{pspicture}(-0.5,-0.5)(5,2.5)
		\cnode*(0,0){2pt}{m1}
		\cnode*(4,2){2pt}{m2}
		\pnode(2,1){mi}
		\uput[-90](m1){\color{DimGray} $q_1$}
		\uput[0](m2){\color{DimGray} $q_2$}
		\ncline[linecolor=MidnightBlue,arrows=->]{m1}{m2}
		\uput[-45](mi){\color{MidnightBlue} $\bm{r}$}
		\psline[linecolor=DarkOrange3,arrows=->](0,0.3)(1.5,1.05)
		\psline[linecolor=DarkOrange3,arrows=->](4,2.3)(2.5,1.55)
		\uput[135](0.75,0.675){\color{DarkOrange3} $\bm{F}_1$}
		\uput[135](3.25,1.925){\color{DarkOrange3} $\bm{F}_2$}
	\end{pspicture}
	\caption{Illustration zur Coulomb-Wechselwirkung.}
	\label{fig:0.2}
\end{figure}

In Gleichung \eqref{eq:0.0} wurde stillschweigend der Vorfaktor $f$ eingeführt. Dieser ist eine Proportionalitätsgröße, die von ihrem Maßsystem abhängig ist. Im folgenden werden wir immer das \acct{CGS-System} verwenden. Daher gilt
%
\begin{align*}
	[F] = \si{\g\cm\s^{-2}}
\intertext{woraus folgt}
	[f \cdot q^2] = \si{\g\cm^{3}\s^{-2}}
\end{align*}
%
Für das CGS-System gilt $f = 1$, und daher $[q] = \si{\g^{1/2}\cm^{3/2}\s^{-1}}$.

\subsection{bewegte Ladung}

Die Kraft, die eine bewegte Ladung auf eine ruhende ausübt kann berechnet werden mit
%
\begin{align}
	\bm{F}_2(t) = q_1 \cdot q_2 \left( \frac{\bm{\hat{r}}}{\kappa(t) \, r^2} + \frac{1}{c \, \kappa(t)} \, \frac{\mathrm{d}}{\mathrm{d}t} \left[ \frac{\bm{\hat{r}} - \frac{1}{c} \, \bm{v}(t)}{\kappa(t) \, r}  \right]\right)_{\text{ret.}}
	\label{eq:0.1}
\end{align}
%
In Abbildung~\ref{fig:0.3} ist eine Skizze der betrachteten Konfiguration zu sehen.

\begin{figure}
	\centering
	\begin{pspicture}(-3,-0.5)(3,2.5)
		% Ursprung als Node
		\cnode*(0,0){2pt}{O}
		\uput[-90](O){\color{DimGray} $0$}
		\psline[arrows=->](O)(2,1.2)
		\uput[0](2,1.2){\color{DimGray} $q_2$}
		\psdot*(2,1.2)
		\psline[arrows=->](O)(-2,1.2)
		\psline[arrows=<->](2,1.2)(-2,1.2)
		\uput[180](-2,1.2){\color{DimGray} $q_1$}
		\uput[180](-0.8,0.3){\color{DimGray} $\bm{r}_1(t)$}
		\uput[-90](0,1.3){\color{DimGray} $\bm{r}(t)$}
		\uput[0](0.8,0.3){\color{DimGray} $\bm{r}_2(t)$}
		\psarc[linecolor=MidnightBlue]{<-}(0,0){2.33}{100}{180}
		\psline[linecolor=DarkOrange3,arrows=->](-2,1.2)(-1.5,2.2)
		\uput[180](-1.6,1.8){\color{DarkOrange3} $\bm{v}(t)$}
		\psdot*(-2,1.2)
	\end{pspicture}
	\caption{Skizze einer bewegten Ladung gegenüber einer anderen, ruhenden Ladung.}
	\label{fig:0.3}
\end{figure}

In Gleichung \eqref{eq:0.1} werden verschiedene Konstanten und Zeichen verwendet:
%
\begin{itemize}
	\item $c$ ist die Lichtgeschwindigkeit im Vakuum
	\item $\kappa = 1 - \frac{1}{c} \bm{\hat{r}} \cdot \bm{v}$
	\item ret.\ bedeutet $t' = t - \frac{r(t')}{c}$. Man kann $t'$ also ausdrücken als $t' = t'(t)$.
\end{itemize}
%
Die Beschreibung der elektromagnetischen Wechselwirkung durch Kräfte ist kompliziert wegen der oben beschriebenen Retardierung. Das bedeutet die Kraftwirkung breitet sich mit endlicher Geschwindigkeit aus.

Ein besserer Ansatz in diesem Fall ist eine Beschreibung durch Felder.

