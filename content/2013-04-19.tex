% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 19.04.2013
% TeX: Michi

\renewcommand{\printfile}{2013-04-19}

\section{Induktionsgesetz}

Im folgenden betrachten wir eine Leiterschleife $\mathcal{C}_1$ die von einem Magnetfeld $\bm{B}$ durchsetzt wird und eine im $\mathbb{R}^3$ glatte Fläche $F_t$ auf der sich eine geschlossene Kurve $\mathcal{C}_t$ ($\mathcal{C}_t = \partial F_t$ und $\partial \partial F_t = 0$) befindet. Sowohl die Fläche $F_t$ als auch $\mathcal{C}_t$ können zeitlich veränderlich sein. Durch die Leiterschleife fließt ein Strom $I_1$.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-2,0)(6,3)
		\psline(0,0)(0,1)
		\pscoil[coilarm=0,coilwidth=0.2](0,0)(0,1)
		\psarc[arrows=->](0,1.5){0.5}{-90}{90}
		\pscurve[linecolor=DarkOrange3,arrows=->](-1,2.0)(0,1.8)(1,2.0)
		\pscurve[linecolor=DarkOrange3,arrows=->](-1,1.75)(0,1.6)(1,1.75)
		\psline[linecolor=DarkOrange3 ,arrows=->](-1,1.5)(4.5,1.5)
		\pscurve[linecolor=DarkOrange3,arrows=->](-1,1.25)(0,1.4)(1,1.25)
		\pscurve[linecolor=DarkOrange3,arrows=->](-1,1.0)(0,1.2)(1,1.0)
		\psarc(0,1.5){0.5}{90}{-90}
		\uput[45](0,2){\color{DimGray} $I_1$}
		\uput[0](1,2){\color{DarkOrange3} $\bm{B}$}
		\rput(0,1.5){
			\pcarc(0.5;-125)(-1,-1)
			\uput[180](-1,-1){\color{DimGray} $\mathcal{C}_1$}
		}

		\rput(3,1.5){
			\psarc[linestyle=dotted,dotsep=1pt](0,0){0.8}{-90}{90}
			\psarc[arrows=->](0,0){0.8}{90}{-90}
			\pscurve(0,0.8)(2,0)(0,-0.8)
			\psellipse[linecolor=MidnightBlue,fillstyle=vlines,hatchcolor=MidnightBlue,hatchsep=2pt](0,-0.4)(0.25,0.125)
			\psline[linecolor=MidnightBlue,arrows=->](0,-0.4)(-0.5,-1)
			\uput[180](-0.5,-1){\color{MidnightBlue} $\bm{n}$}
			\uput[45](2,0){\color{DimGray} $F_t$}
			\pcarc(0.8;120)(0,1.3)
			\uput[0](0,1.3){\color{DimGray} $\mathcal{C}_t = \partial F_t$}
		}
	\end{pspicture}
	\caption{Eine Leiterschleife $\mathcal{C}_1$ erzeugt ein Magnetfeld $\bm{B}$, dessen Feldlinien ins Volumen $F_t$ hineinreichen.}
\end{figure}

Die eingezeichnete (angegebene) Stromrichtung gehört zu einer Abnahme des magnetischen Flusses.

\begin{notice}[Beobachtungen:] In $\mathcal{C}_t$ fließt ein Strom, wenn
	\begin{enumerate}
		\item \label{itm:2013-04-19-1} $\mathcal{C}_1$ ruht und $\mathcal{C}_t$ bewegt wird
		\item \label{itm:2013-04-19-2} $\mathcal{C}_1$ bewegt wird und $\mathcal{C}_t$ in ruhe ist 
		\item \label{itm:2013-04-19-3} $\mathcal{C}_1$ und $\mathcal{C}_t$ in ruhe sind und $I_1(t)$ variiert
	\end{enumerate}
\end{notice}

$I$ wir durch ein elektrisches Feld $\bm{E}^{(e)} \left(= \frac{\text{Kraft}}{\text{Fläche}} \right)$ in Leiter $\mathcal{C}_t$ bewirkt (>>elektromotorische Kraft<<). 

Aus diesen Erfahrungen ergeben sich zusammenfassend (auch als Faradaysches Induktionsgesetz bekannt):
\[
	\mathfrak{E} \equiv \Gamma_{\partial F_t}(\bm{E}) = \boxed{\int_{\partial F_t} \bm{E}^{(e)} \; \mathrm{d}\bm{r} = -\frac{1}{c} \frac{\mathrm{d}}{\mathrm{d}t} \int_{F_t} \bm{B} \cdot \bm{n} \; \mathrm{d}f}
\]
$\Gamma_{\partial F_t}(\bm{E})$ bezeichnet hierbei die Ringspannung $\mathfrak{E}$. Das Wegintegral:
\[
	\int_{\partial F_t} \bm{E}^{(e)} \; \mathrm{d}\bm{r}
\]
bezeichnet man als die elektromotorische Kraft im Leiter. 
\[
	\Phi_{F_t}(\bm{B}) = \int_{\partial F_t} \bm{E}^{(e)} \; \mathrm{d}f
\]
Dies bezeichnet man als den magnetischen Fluss.

Wesentlich für $\mathfrak{E} \neq 0$ ist nur die Änderung der magnetischen Flusses, z.B.\ aufgrund von Relativbewegungen, $\bm{B}(t)$ oder Deformation der Schleife $\mathcal{C}_t$. 

\begin{theorem}[Lenzsche Regel]
Ein induzierter Strom (hervorgerufen durch $\bm{E}^{(e)}$) wirkt der Flussänderung entgegen. 
\end{theorem}

\begin{notice}[Frage:]
Ist das Induktionsgesetz eine neue Aussage oder ist es aus der Lorentzkraft herleitbar?
\end{notice}

\begin{description}
	\item[zur Beobachtung~\ref{itm:2013-04-19-1}:] Wir betrachten einen Bügel, der über eine Leiterschleife gezogen wird wie in Abbildung~\ref{fig:2013-04-19-0} zu sehen ist.

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-0.5,-1.5)(4,1.5)
				\psline(2,1)(0,1)(0,-1)(2,-1)
				\psline(2.5,1)(3,1)
				\psline(2.5,-1)(3,-1)
				\psframe(2,-1.5)(2.5,1.5)
				\psbrace[
					fillcolor=DimGray,
					braceWidth=0.5pt,
					braceWidthInner=4pt,
					braceWidthOuter=2pt,
					ref=lC,
					rot=0,
					nodesepA=-5pt
				](-0,1)(0,-1){\color{DimGray} $b$}
				\psbrace[
					fillcolor=DimGray,
					braceWidth=0.5pt,
					braceWidthInner=4pt,
					braceWidthOuter=2pt,
					ref=lC,
					rot=90,
					nodesepB=5pt
				](0,-1)(2,-1){\color{DimGray} \clap{$\ell(t)$}}
				\rput(0.5,0.5){\color{DarkOrange3} $\otimes$}
				\rput(0.5,-0.5){\color{DarkOrange3} $\otimes$}
				\uput[45](0.5,0.5){\color{DarkOrange3} $\bm{B}$}
				\rput(3,0.5){\color{DarkOrange3} $\otimes$}
				\rput(3,-0.5){\color{DarkOrange3} $\otimes$}
				\rput(1.5,0){\color{DimGray} $\odot$}
				\uput[135](1.5,0){\color{DimGray} $\bm{n}$}
				\psline[linecolor=MidnightBlue,arrows=->](2.25,-0.75)(2.25,0.75)
				\uput[90](2.25,0.75){\color{MidnightBlue} $\bm{K}$}
				\psline[linecolor=Purple,arrows=->](2.5,0)(4,0)
				\uput[90](3.5,0){\color{Purple} $\bm{v}$}
			\end{pspicture}
			\caption{Zur Herleitung der Lorentzkraft: Beweglicher Bügel über einer Leiterschleife.}
			\label{fig:2013-04-19-0}
		\end{figure}

		Auf die beweglichen Ladungen im Bügel wirkt eine Lorentzkraft, die senkrecht zu $\bm{v}$ und $\bm{B}$ steht. Für die Ringspannung (die Orientierung für die Integration ist gegen den Uhrzeigersinn):
		%
		\begin{align}
			\mathfrak{E} &= %\int_{\text{um Schleife}} \frac{1}{q} \bm{K} \; \mathrm{d}\bm{r} \nonumber\\
			\int_{
				\tikz[scale=0.3]{
					\draw[arrow inside={pos=0.7}] (1.5,1) -- (0,1);
					\draw[arrow inside] (0,1) -- (0,0);
					\draw[arrow inside={pos=0.3}] (0,0) -- (1.5,0);
					\draw[arrow inside] (1,-0.5) -- (1,1.5);
				}
			}  \frac{1}{q} \bm{K} \; \mathrm{d}\bm{r} \nonumber \\
			&=  %\frac{1}{c} \int_{\text{Bügel}} \left(\bm{v} \times \bm{B} \right) \; \mathrm{d}\bm{r} \nonumber \\
			\frac{1}{c} \int_{
				\tikz[scale=0.3]{
					\draw (1.5,1) -- (0,1) -- (0,0) -- (1.5,0);
					\draw[arrow inside] (1,-0.5) -- (1,1.5);
				}
			} \left(\bm{v} \times \bm{B} \right) \; \mathrm{d}\bm{r} \nonumber \\
			&= \frac{1}{c} v B b = \frac{1}{c} B b \frac{\mathrm{d}\ell}{\mathrm{d}t} \label{eq: Ind_beo_1}
		\end{align}
		%
		Es zeigt sich, dass nur der Bügel einen Beitrag liefert. Anderseits gilt für den magnetischen Fluss (integriert über die Fläche die von Bügel und Schleife eingeschlossen wird):
		%
		\begin{align}
			\Phi(\bm{B}) &= %\int \bm{B} \cdot\bm{n} \; \mathrm{d}f = -B b \ell(t) \label{eq: int_beo_1_1}
			\int_{
				\tikz[scale=0.3]{
					\path[fill=lightgray] (0,0) rectangle (1,1);
					\draw (1.5,1) -- (0,1) -- (0,0) -- (1.5,0);
					\draw (1,-0.5) -- (1,1.5);
				}
			} \bm{B} \cdot\bm{n} \; \mathrm{d}f = -B b \ell(t) \label{eq: int_beo_1_1}
		\end{align}
		%
		Aus \eqref{eq: Ind_beo_1} und \eqref{eq: int_beo_1_1} folgt somit für die Ringspannung:
		\[
			\implies \mathfrak{E} = -\frac{1}{c} \frac{\mathrm{d}}{\mathrm{d}t} \Phi(\bm{B}),
		\]
		d.h.\ dieser Aspekt des Induktionsgesetzes folgt aus der Lorentzkraft.

	\item[zur Beobachtung~\ref{itm:2013-04-19-2}:] Folgt aus \ref{itm:2013-04-19-1} gemäß dem Relativitätsprinzip. 
		\begin{notice}
			Bei beliebiger Bewegung ist $\mathcal{C}_1$, i.A.\ kein Inertialsystem, Betrachtung momentaner Inertialsysteme.
		\end{notice}

	\item[zur Beobachtung~\ref{itm:2013-04-19-3}:] Es wirkt eine Kraft auf ruhende Ladungen im zeitlich veränderlichen $\bm{B}$-Feld. Anderseits muss sich jede Kraft auf Ladungen stets in der Form
		\[
			F = q\left(\bm{E} + \frac{1}{c} \bm{v} \times \bm{B}\right)
		\]
		schreiben lassen, d.h.\ die allgemeinste Form des \acct{Induktionsgesetz} lautet:
		\[
			\boxed{\mathfrak{E} \coloneq \int_{\partial F_t} \left(\bm{E} + \frac{1}{c} \bm{v} \times \bm{B}\right)  \; \mathrm{d}\bm{r} = -\frac{1}{c} \frac{\mathrm{d}}{\mathrm{d}t} \int_{F_t} \bm{B}\cdot\bm{n} \mathrm{d}f}
		\]
		Ist $\bm{B}$ zeitabhängig und der Leiter in Ruhe ist dennoch ein Strom zu messen. Die Ladung in einem Leiter muss also durch ein $\bm{E}$-Feld angetrieben sein. Die Folgerung ist, dass ein zeitabhängiges $\bm{B}$-Feld mit einem $\bm{E}$-Feld verknüpft ist.
\end{description}

\begin{notice}
\begin{enumerate}
	\item $q$ geht nicht ein $\implies$ Zirkulation von $\bm{E}$ tritt auch auf, wenn \emph{kein} materieller Leiter entlang $\partial F_t$ vorhanden ist.
	\item Der Beschreibung liegt ein Inertialsystem $\Sigma_i$ zugrunde:

		\begin{figure}[htpb]
			\centering
			\begin{pspicture}(-0.5,-0.7)(3.5,2.2)
				\cput(0,1.5){\color{DimGray} $\Sigma_i$}
				\psline[arrows=->](0,0)(1,0)
				\psline[arrows=->](0,0)(0,1)
				\psline[arrows=->](0,0)(-0.5,-0.5)
				\rput(1,1){
					\psccurve[linecolor=MidnightBlue](0,0)(0.5,-0.5)(0.75,-0.6)(1,-1)(1.5,-0.3)(2,0)(2.5,0.5)(2,0.5)(1.5,1)(1,0.8)(0.5,1)
					\psecurve[linecolor=DarkOrange3,arrows=|-|](0,0)(0.5,-0.5)(0.75,-0.6)(1,-1)(1.5,-0.3)
					\pnode(0.75,-0.6){A}
					\pcarc[arcangle=-20](0.5,1)(0,1)
					\uput[180](0,1){\color{MidnightBlue} $\partial F_t$}
				}
				\rput(A){
					\psline[linecolor=Purple,arrows=->](0,0)(0.8;-80)
					\uput[-160](0.8;-80){\color{Purple} $\bm{v}(\bm{r},t)$}
				}
				\pcline[arrows=->](0,0)(A)
				\naput{\color{DimGray} $\bm{r}(t)$}
				\uput[80](A){\color{DarkOrange3} $\mathrm{d}\bm{r}$}
			\end{pspicture}
			\caption{Das Interialsystem $\Sigma_i$}
		\end{figure}

		\begin{itemize}
			\item 	Entlang $\partial F_t$, bzw.\ auf $F_t$:
				\[ \bm{B}(\bm{r}(t),t), \quad \bm{E}(\bm{r}(t),t) \]
			\item Geschwindigkeit des Leiterelements $\mathrm{d}\bm{r}$:
				\[ \bm{v}(\bm{r}(t),t) \]
		\end{itemize} 
	\end{enumerate}
\end{notice}

\begin{notice}[Fazit:]
	Mit den bisher  angegebenen Gesetzen des elektromagnetische Feldes sind fast schon die Maxwellschen Gleichungen in integraler Form vorhanden.
\end{notice}

\section{Maxwellsche Gleichungen}
%
Wir haben in den vorhergehenden Abschnitten die folgenden Feldgleichungen in ihrer integralen Form gewonnen:
%
\begin{align}
	\int_{\partial V}\,\bm{E}\cdot\mathrm{d}\bm{f} &\stackrel{\text{Gauß}}{=} 4\pi\int_{V}\,\varrho\,\mathrm{d}V \label{eq:2013-04-19-max_1} \\
	\int_{\partial V}\,\bm{B}\cdot\mathrm{d}\bm{f} &\stackrel{\text{Gilbert}}{=} 0 \label{eq:2013-04-19-max_2} \\
	\int_{\partial F_t}\,\left[\bm{E} + \frac{1}{c}\bm{v}\times\bm{B}\right]\,\mathrm{d}\bm{r} &\stackrel{\text{Faraday}}{=} -\frac{1}{c}\frac{\mathrm{d}}{\mathrm{d}t}\int_{F_t}\,\bm{B}\cdot\mathrm{d}\bm{f} \label{eq:2013-04-19-max_3}
\end{align}
%
Dabei ergibt sich $\bm{v}$ aus der Beschreibung der Berandung $\partial F_t$ 
%
\begin{align}
	\underbrace{\int_{\partial F_t}\,\bm{B}\cdot\mathrm{d}\bm{r} = \frac{4\pi}{c}\int_{F_t}\,\bm{j}\cdot\mathrm{d}\bm{f}}_{\text{Oersted, Ampère}} + \underbrace{\frac{1}{c}\int_{F_t}\,\partial_t\bm{E}\cdot\mathrm{d}\bm{f}}_{\text{Maxwells Ergänzung}} \label{eq:2013-04-19-max_4}
\end{align}
%
Hierzu wurde lediglich die Lorentzkraft verwendet, welche die mechanische Wirkung elektromagnetischer Felder beschreibt und wie folgt lautet:
%
\begin{align*}
	\bm{K} &= q\left(\bm{E} + \frac{1}{c}\bm{v}\times\bm{B}\right)
\intertext{bzw.\ die dazugehörige Kraftdichte, auch \acct{Lorentzkraftdichte}:} 
	\bm{k} &= \varrho\bm{E} + \frac{1}{c}\bm{j}\times\bm{B}
\end{align*}
%
Die Gleichungen \eqref{eq:2013-04-19-max_1}, \eqref{eq:2013-04-19-max_2}, \eqref{eq:2013-04-19-max_3} und \eqref{eq:2013-04-19-max_4} stellen dabei die \acct*{Maxwellschen Gleichungen in ihrer integralen Form}\index{Maxwellsche Gleichungen!integrale Form} dar. Mit der Maxwellschen Ergänzung (auch Verschiebungsstromdichte genannt) 
\[
	\frac{1}{c}\partial_t \bm{E}
\]
gelten sie für beliebige, räumlich und zeitlich variierende Ladungen und Stromdichten. Zusammen mit dem zweiten Newtonschen Axiom $\bm{F} = \dot{\bm{p}}$ und dem Gravitationsgesetz bilden sie die Grundlage der klassischen Physik. (Es fehlt lediglich noch der Begriff der Entropie).

\begin{notice}
	Da die Maxwellgleichungen linear sind gehorchen sie dem Superpositionsprinzip.
\end{notice}

Die Raumladungsdichte $\varrho$ und die Stromdichte $\bm{j}$, die durch die Kontinuitätsgleichung miteinander gekoppelt sind erzeugen $\bm{E}$ und $\bm{B}$; über die Lorentzkraft wirken $\bm{E}$ und $\bm{B}$ zurück auf die Bewegungen der Ladungen. Dieses Prinzip bezeichnet schließlich die \acct*{elektromagnetische Wechselwirkung}. Darüber hinaus stellen die Maxwellschen Gleichungen eine einheitliche Feldtheorie der Materie dar. 
%
\paragraph{Motivation und Interpretation der Maxwellschen Ergänzung:}
\index{Maxwellsche Ergänzung}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-1,-1.5)(2,2.5)
		\psellipse(0,0)(1,0.5)
		\rput(-0.5,0){\color{DimGray} $-$}
		\rput(0.5,0){\color{DimGray} $-$}
		\psellipse[fillstyle=solid,fillcolor=white](0,0.5)(1,0.5)
		\rput(-0.5,0.5){\color{DimGray} $+$}
		\rput(0.5,0.5){\color{DimGray} $+$}
		\psdot(0,0.5)
		\psline(0,0.5)(0,1.5)
		\rput(0,1.5){
			\rput(-1,0.5){\color{DarkOrange3} $F$}
			\pnode(! 1 -30 cos mul 0.5 -30 sin mul){A}
			\psellipse[linestyle=dashed,linecolor=DarkOrange3,fillstyle=hlines,hatchcolor=DarkOrange3](0,0)(1,0.5)
			\psline[linecolor=DarkOrange3,arrows=->](-0.5,0.1)(-0.5,1)
			\psline[linecolor=MidnightBlue](0,0)(A)
			\rput(A){
				\psline[linecolor=Purple,arrows=->](0,0)(1;50)
				\uput[-90](1;50){\color{Purple} $\bm{B}$}
			}
			\psdot*[linecolor=MidnightBlue](A)
		}
		\psline[ArrowInside=->,ArrowInsidePos=0.75](0,1.5)(0,2.5)
		\psline[ArrowInside=->](0,2.5)(2,2.5)
		\psline[ArrowInside=->](2,2.5)(2,0.5)
		\rput(2,0.5){
			\psline(0,0)(0.5;-120)
			\psarc[arrows=->](0,0){0.6}{-140}{-100}
		}
		\psdots(2,0.5)(2,0)
		\psline[ArrowInside=->](2,0)(2,-1.5)
		\psline[ArrowInside=->](2,-1.5)(0,-1.5)
		\psline[ArrowInside=->](0,-1.5)(0,-0.5)
		\uput[90](1,-1.5){\color{DimGray} $\bm{I}(t)$}
	\end{pspicture}
	\hspace{0.2\textwidth}
	\begin{pspicture}(-1.5,-2)(1.5,2)
		\psline(-0.2,1)(-0.2,0.5)(-1,0.5)(-1,0.2)(1,0.2)(1,0.5)(0.2,0.5)(0.2,1)
		\psline(-0.2,-1)(-0.2,-0.5)(-1,-0.5)(-1,-0.2)(1,-0.2)(1,-0.5)(0.2,-0.5)(0.2,-1)
		\foreach \x in {-0.8, -0.4, 0, 0.4, 0.8} {
			\rput(\x,0.35){\color{DimGray} $+$}
			\rput(\x,-0.35){\color{DimGray} $-$}
			\psline[linecolor=MidnightBlue,arrows=->](\x,0.2)(\x,-0.2)
		}
		\uput[0](1,-0.35){\color{MidnightBlue} $\bm{E}(t)$}
		\psline[linecolor=DarkOrange3,linestyle=dashed](-1.5,0)(1.5,0)
		\uput[-90](-1.5,0){\color{DarkOrange3} $F''$}
		\psline[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](1.5,0)(1,0.8)(-1,0.8)(-1.5,0)
		\uput[90](-1,0.8){\color{DarkOrange3} $F'$}
		\psline[arrows=->](1.2,0)(1.2,0.4)
		\uput[0](1.2,0.4){\color{DimGray} $\bm{n}''$}
		\psline[arrows=->](0.8,0.8)(0.8,1.2)
		\uput[0](0.8,1.2){\color{DimGray} $\bm{n}'$}
		\rput(-1,1.5){\color{Purple} $\odot$}
		\rput(-1,-1.5){\color{Purple} $\odot$}
		\rput(1,1.5){\color{Purple} $\otimes$}
		\rput(1,-1.5){\color{Purple} $\otimes$}
		\uput[0](-1,-1.5){\color{Purple} $\bm{B}$}
		\psline[arrows=->](0,0.7)(0,1.3)
		\uput[90](0,1.3){\color{DimGray} $\bm{I}(t)$}
	\end{pspicture}
	\caption{Plattenkonsensator. Links eingebaut im Stromkreis, rechts im Querschnitt mit charakteristischen Größen.}
\end{figure}

Das Schließen des Schalters führt zu einem Entladungsstrom $I(t)$, der wiederum ein Magnetfeld induziert. Nach Oersted und Ampère wird dies wie folgt beschrieben:
%
\begin{align*}
	\underbrace{\int_{\partial F}\,\bm{B}\cdot\mathrm{d}\bm{r}}_{= 2\pi B r} &= \frac{4\pi}{c}\int_{F}\,\bm{j}\cdot\mathrm{d}\bm{f} = \frac{4\pi}{c}I
\end{align*}
%
Betrachten wir den Querschnitt eines Plattenkondensators, so lassen sich wie in der Abbildung gezeigt zwei Flächen definieren (die Fläche $F'$ und die Fläche $F''$). Die Beschreibung hier erfolgt analog zu oben:
%
\begin{align}
	\int_{\partial F'}\,\bm{B}\cdot\mathrm{d}\bm{r} &= \frac{4\pi}{c}\int_{F'}\,\bm{j}\cdot\mathrm{d}\bm{f} = \frac{4\pi}{c}I \notag
	\intertext{Bei Betrachtung der Fläche $F''$ liegt das gleiche Magnetfeld vor, dennoch ergibt sich:}
	\int_{\partial F''}\,\bm{B}\cdot\mathrm{d}\bm{r} &= \underbrace{\frac{4\pi}{c}\int_{F''}\,\bm{j}\cdot\mathrm{d}\bm{f} = 0}_{\mathclap{\text{ohne Maxwellsche Ergänzung}}} \tag{*} \label{eq:2013-04-19-stern1}
\end{align}
%
Das heißt, dass Gleichung \eqref{eq:2013-04-19-stern1} nicht berücksichtigt, was sich zwischen den Kondensatorplatten abspielt; nämlich ein zeitlich veränderliches $\bm{E}$-Feld während der Entladung. Denn während der Entladung werden Oberflächenladungen abgebaut, was dazu führt, dass $|\bm{E}(t)|$ minimal wird.

\begin{theorem}[Maxwells Hypothese]
$\frac{1}{4\pi}\partial_t\bm{E}$ erzeugt wie $\bm{j}$ eine magnetische Zirkulation.
\end{theorem}
